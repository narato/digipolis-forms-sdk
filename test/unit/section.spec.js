import chai from 'chai';
import { Field, Prerequisites, Section, SectionState } from '../../src/index.js';

const expect = chai.expect;
var section;

/** @test {Section} */
describe('Creating a Section instance', function () {
  /** @test {Section#id} */
  it('throws an exception without an id', () => {
    expect(() => { const section = new Section(null); }).to.throw(/Section must have an id/);
  });

  /** @test {Section#id} */
  it('throws an exception when specified value is not a string', () => {
    expect(() => { const section = new Section(5); }).to.throw(TypeError);
  });
});

/** @test {Section} */
describe('A Section instance', function () {
  beforeEach(function () {
    section = new Section('section0');
  });

  describe('with an id', function () {
    it('should use the id from the constructor', () => {
      expect(section.id).to.equal('section0');
    });

    /** @test {Section#toJSON} */
    it('should include the id in the JSON representation', () => {
      let output = section.toJSON();
      expect(output.id).to.equal('section0');
    });
  });

  describe('with a title', function () {
    beforeEach(function () {
      section.title = 'section title';
    });

    /** @test {Section#title} */
    it('should have the specified title value', () => {
      expect(section.title).to.equal('section title');
    });

    /** @test {Section#toJSON} */
    it('should include the title in the JSON representation', () => {
      let output = section.toJSON();
      expect(output.title).to.equal('section title');
    });
  });

  describe('with a subtitle', function () {
    beforeEach(function () {
      section.subtitle = 'section subtitle';
    });

    /** @test {Section#subtitle} */
    it('should have the specified subtitle value', () => {
      expect(section.subtitle).to.equal('section subtitle');
    });

    /** @test {Section#toJSON} */
    it('should include the subtitle in the JSON representation', () => {
      let output = section.toJSON();
      expect(output.subtitle).to.equal('section subtitle');
    });
  });

  describe('with a state', function () {
    var s;
    beforeEach(function () {
      s = new SectionState();
      section.state = s;
    });

    /** @test {Section#state} */
    it('should have the specified state type value', () => {
      expect(section.state).to.equal(s);
    });

    /** @test {Section#state} */
    it('throws an exception when the specified value is not a SectionState object', () => {
      expect(() => { section.state = 5; }).to.throw(TypeError);
    });

    /** @test {Section#toJSON} */
    it('should include the state when exporting', () => {
      const output = section.toJSON();
      expect(output.state).to.exist;
    });
  });

  describe('without a state', function () {
    beforeEach(function () {
      section.state = null;
    });

    /** @test {Section#state} */
    it('should be allowed', () => {
      expect(section.state).to.be.null;
    });

    /** @test {Section#toJSON} */
    it('should not include a state when exporting', () => {
      const output = section.toJSON();
      expect(output.state).to.not.exist;
    });
  });

  describe('with prerequisites', function () {
    var p;
    beforeEach(function () {
      p = new Prerequisites();
      section.prerequisites = p;
    });

    /** @test {Section#prerequisites} */
    it('should have the specified prerequisites', () => {
      expect(section.prerequisites).to.equal(p);
    });

    /** @test {Section#prerequisites} */
    it('throws an exception when the specified value is not a Prerequisites object', () => {
      expect(() => { section.prerequisites = 5; }).to.throw(TypeError);
    });

    /** @test {Section#toJSON} */
    it('should include the prerequisites when exporting', () => {
      const output = section.toJSON();
      expect(output.prerequisites).to.exist;
    });
  });

  describe('without prerequisites', function () {
    beforeEach(function () {
      section.prerequisites = null;
    });

    /** @test {Section#prerequisites} */
    it('should be allowed', () => {
      expect(section.prerequisites).to.be.null;
    });

    /** @test {Section#toJSON} */
    it('should not include a prerequisites when exporting', () => {
      const output = section.toJSON();
      expect(output.prerequisites).to.not.exist;
    });
  });

  describe('with fields', function () {
    var fields;
    beforeEach(function () {
      fields = [new Field('field0'), new Field('field1')];
      section.fields = fields;
    });

    /** @test {Section#fields} */
    it('should have the specified fieds', () => {
      expect(section.fields).to.equal(fields);
    });

    /** @test {Section#fields} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { section.fields = 5; }).to.throw(TypeError);
    });

    /** @test {Section#fields} */
    it('throws an exception when the specified value is not an Array of Fields', () => {
      expect(() => { section.fields = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Section#toJSON} */
    it('should include the fields when exporting', () => {
      const output = section.toJSON();
      expect(output.fields).to.exist;
    });
  });

  describe('with an empty fields array', function () {
    beforeEach(function () {
      section.fields = [];
    });

    /** @test {Section#fields} */
    it('should be allowed', () => {
      expect(section.fields).to.be.empty;
    });

    /** @test {Section#toJSON} */
    it('should not include fields when exporting', () => {
      const output = section.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('without fields', function () {
    beforeEach(function () {
      section.fields = null;
    });

    /** @test {Section#fields} */
    it('should be allowed', () => {
      expect(section.fields).to.be.null;
    });

    /** @test {Section#toJSON} */
    it('should not include fields when exporting', () => {
      const output = section.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('when adding a Field', function () {
    /** @test {Section#addField} */
    it('should have added the field to the section', () => {
      let field = new Field('field0');
      section.addField(field);
      let anotherField = new Field('field1');
      section.addField(anotherField);

      expect(section.fields[0]).to.equal(field);
      expect(section.fields[1]).to.equal(anotherField);
    });

    /** @test {Section#addField} */
    it('should not allow a duplicate field', () => {
      let field = new Field('field0');
      section.addField(field);
      let addDuplicateField = () => section.addField(field);

      expect(addDuplicateField).to.throw(/Field with name/);
    });

    /** @test {Section#addField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { section.addField(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a field', function () {
    /** @test {Section#getField} */
    it('should return the field with the specified name', () => {
      let field0 = new Field('field0');
      section.addField(field0);
      section.addField(new Field('field1'));
      section.addField(new Field('field2'));

      expect(section.getField('field0')).to.equal(field0);
    });

    /** @test {Section#getField} */
    it('should return undefined if a field with the specified name does not exist', () => {
      section.addField(new Field('field0'));
      section.addField(new Field('field1'));
      section.addField(new Field('field2'));

      expect(section.getField('field4')).to.be.undefined;
    });
  });

  describe('when updating a field', function () {
    /** @test {Section#updateField} */
    it('should have updated the field of the section', () => {
      section.addField(new Field('field0'));
      section.addField(new Field('field1'));
      section.addField(new Field('field2'));

      let updatedField = new Field('field1');
      section.updateField(updatedField);

      expect(section.fields.length).to.equal(3);
      expect(section.fields[1]).to.equal(updatedField);
    });

    /** @test {Section#updateField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let updateNonExistingField = () => section.updateField(field);

      expect(updateNonExistingField).to.throw(Error);
    });

    /** @test {Section#updateField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { section.updateField(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a field', function () {
    /** @test {Section#removeField} */
    it('should have removed the field of the section', () => {
      section.addField(new Field('field0'));
      let fieldToRemove = new Field('field1');
      section.addField(fieldToRemove);
      section.addField(new Field('field2'));
      expect(section.fields.length).to.equal(3);

      section.removeField(fieldToRemove);

      expect(section.fields.length).to.equal(2);
      expect(section.fields).not.to.include(fieldToRemove);
    });

    /** @test {Section#removeField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let removeNonExistingField = () => section.removeField(field);

      expect(removeNonExistingField).to.throw(Error);
    });

    /** @test {Section#removeField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { section.removeField(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a field', function () {
    /** @test {Section#moveFieldToIndex} */
    it('should have moved the field to the specified index', () => {
      let firstField = new Field('field0');
      let secondField = new Field('field1');
      let thirdField = new Field('field2');

      section.addField(firstField);
      section.addField(secondField);
      section.addField(thirdField);
      expect(section.fields[0]).to.equal(firstField);

      section.moveFieldToIndex(thirdField, 0);

      expect(section.fields[0]).to.equal(thirdField);
    });

    /** @test {Section#moveFieldToIndex} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let moveNonExistingField = () => section.moveFieldToIndex(field, 0);

      expect(moveNonExistingField).to.throw(Error);
    });

    /** @test {Section#moveFieldToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let field = new Field('field0');
      section.addField(field);
      let moveOUBindex = () => section.moveFieldToIndex(field, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Section#moveFieldToIndex} */
    it('throws an error if no index was specified', () => {
      let field = new Field('field0');
      section.addField(field);
      let moveOUBindex = () => section.moveFieldToIndex(field);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Section#moveFieldToIndex} */
    it('throws an exception when the specified field is not a Field', () => {
      expect(() => { section.moveFieldToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Section#moveFieldToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { section.moveFieldToIndex(new Field('field0'), 'test'); }).to.throw(TypeError);
    });
  });

  describe('created from a JSON string', function () {
    /** @test {Section.fromJSON} */
    it('should return a Section instance', () => {
      let jsonData = '{"id":"section0","title":"section title"}';
      let section = Section.fromJSON(jsonData);
      expect(section.id).to.equal('section0');
    });

    /** @test {Section.fromJSON} */
    it('should contain a nested SectionState object', () => {
      const jsonData = `{
        "id": "section0",
        "state": { "editMode": true }
      }`;
      const section = Section.fromJSON(jsonData);
      expect(section.state).to.be.an.instanceof(SectionState);
    });

    /** @test {Section.fromJSON} */
    it('should contain a nested Prerequisites object', () => {
      const jsonData = `{
        "id": "section0",
        "prerequisites": { "sectionsCompleted": { "logical": "and" } }
      }`;
      const section = Section.fromJSON(jsonData);
      expect(section.prerequisites).to.be.an.instanceof(Prerequisites);
    });

    /** @test {Section.fromJSON} */
    it('should include fields as Field objects', () => {
      const obj = `{
        "id": "section0",
        "fields": [ { "name": "field0" } ]
      }`;
      const section = Section.fromJSON(obj);
      expect(section.fields[0]).to.be.an.instanceof(Field);
    });

    /** @test {Section.fromJSON} */
    it('should return null if null was given', () => {
      expect(Section.fromJSON(null)).to.be.null;
    });
  });
});
