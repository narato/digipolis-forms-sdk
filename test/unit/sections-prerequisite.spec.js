'use strict';

import chai from 'chai';
import {Section, SectionsPrerequisite} from '../../src/index.js';

const expect = chai.expect;

var prerequisites;

/** @test {SectionsPrerequisite} */
describe('A SectionsPrerequisite instance', function () {
  beforeEach(function () {
    prerequisites = new SectionsPrerequisite();
  });

  describe('with a logical', function () {
    beforeEach(function () {
      const l = 'and';
      prerequisites.logical = l;
    });

    /** @test {SectionsPrerequisite#logical} */
    it('should have the specified logical', () => {
      expect(prerequisites.logical).to.equal('and');
    });

    /** @test {SectionsPrerequisite#logical} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.logical = 5; }).to.throw(TypeError);
    });

    /** @test {SectionsPrerequisite#toJSON} */
    it('should include the logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.equal('and');
    });
  });

  describe('without a logical', function () {
    beforeEach(function () {
      prerequisites.logical = null;
    });

    /** @test {SectionsPrerequisite#logical} */
    it('should be allowed', () => {
      expect(prerequisites.logical).to.be.null;
    });

    /** @test {SectionsPrerequisite#toJSON} */
    it('should not include a logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.not.exist;
    });
  });

  describe('with sections IDs', function () {
    var sections;
    beforeEach(function () {
      sections = ['section0', 'section1'];
      prerequisites.sections = sections;
    });

    /** @test {Prerequisites#sections} */
    it('should have the specified sections', () => {
      expect(prerequisites.sections).to.equal(sections);
    });

    /** @test {Prerequisites#sections} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { prerequisites.sections = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#sections} */
    it('throws an exception when the specified value is not an Array of strings', () => {
      expect(() => { prerequisites.sections = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the sections when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.sections).to.exist;
    });
  });

  describe('with an empty sections array', function () {
    beforeEach(function () {
      prerequisites.sections = [];
    });

    /** @test {Prerequisites#sections} */
    it('should be allowed', () => {
      expect(prerequisites.sections).to.be.empty;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include sections when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('without sections', function () {
    beforeEach(function () {
      prerequisites.sections = null;
    });

    /** @test {Prerequisites#sections} */
    it('should be allowed', () => {
      expect(prerequisites.sections).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include sections when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('when adding a section', function () {
    /** @test {SectionsPrerequisite#addSection} */
    it('should have added the section to the prerequisites', () => {
      let section = new Section('section0');
      prerequisites.addSection(section);
      let anotherSection = new Section('section1');
      prerequisites.addSection(anotherSection);

      expect(prerequisites.sections[0]).to.equal('section0');
      expect(prerequisites.sections[1]).to.equal('section1');
    });

    /** @test {SectionsPrerequisite#addSectionId} */
    it('should have added the section id to the prerequisites', () => {
      prerequisites.addSectionId('section0');
      prerequisites.addSectionId('section1');

      expect(prerequisites.sections[0]).to.equal('section0');
      expect(prerequisites.sections[1]).to.equal('section1');
    });

    /** @test {SectionsPrerequisite#addSectionId} */
    it('should not allow a duplicate section', () => {
      prerequisites.addSectionId('section0');
      let addDuplicateSection = () => prerequisites.addSectionId('section0');

      expect(addDuplicateSection).to.throw(/Section ID/);
    });

    /** @test {SectionsPrerequisite#addSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { prerequisites.addSection(5); }).to.throw(TypeError);
    });

    /** @test {SectionsPrerequisite#addSectionId} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.addSectionId(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a section', function () {
    /** @test {SectionsPrerequisite#removeSectionId} */
    it('should have removed the section id of the prerequisites', () => {
      prerequisites.addSectionId('section0');
      prerequisites.addSectionId('section1');
      prerequisites.addSectionId('section2');
      expect(prerequisites.sections.length).to.equal(3);

      prerequisites.removeSectionId('section1');

      expect(prerequisites.sections.length).to.equal(2);
      expect(prerequisites.sections).not.to.include('section1');
    });

    /** @test {SectionsPrerequisite#removeSection} */
    it('should have removed the section of the prerequisites', () => {
      prerequisites.addSectionId('section0');
      prerequisites.addSectionId('section1');
      prerequisites.addSectionId('section2');
      expect(prerequisites.sections.length).to.equal(3);

      prerequisites.removeSection(new Section('section1'));

      expect(prerequisites.sections.length).to.equal(2);
      expect(prerequisites.sections).not.to.include('section1');
    });

    /** @test {SectionsPrerequisite#removeSectionId} */
    it('throws an error if specified section does not exist', () => {
      let removeNonExistingSection = () => prerequisites.removeSectionId('section0');

      expect(removeNonExistingSection).to.throw(Error);
    });

    /** @test {SectionsPrerequisite#removeSectionId} */
    it('throws an exception when the specified value is not a String', () => {
      expect(() => { prerequisites.removeSectionId(5); }).to.throw(TypeError);
    });

    /** @test {SectionsPrerequisite#removeSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { prerequisites.removeSection(5); }).to.throw(TypeError);
    });
  });

  /** @test {SectionsPrerequisite#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    prerequisites.randomAttribute = 5;
    const output = prerequisites.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {SectionsPrerequisite.fromJSON} */
    it('should return a SectionsPrerequisite instance', () => {
      const jsonData = '{ "logical": "and" }';
      const prerequisites = SectionsPrerequisite.fromJSON(jsonData);
      expect(prerequisites).to.be.an.instanceof(SectionsPrerequisite);
    });
  });
});
