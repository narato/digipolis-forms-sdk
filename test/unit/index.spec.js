import chai from 'chai';
import {Form} from '../../src/index.js';
import ComposerSdk from '../../src/index.js';

const expect = chai.expect;

/** @test {ComposerSdk} */
describe('Given an instance of my library', function () {

  describe('when I create a new form instance', function () {
    /** @test {ComposerSdk.createForm} */
    it('should return a new Form instance', () => {
      expect(ComposerSdk.createForm()).to.not.equal(null);
    });
  });

  describe('when I create a new step', function () {
    /** @test {ComposerSdk.createStep} */
    it('should have the specified id', () => {
      const step = ComposerSdk.createStep('step0');
      expect(step.id).to.equal('step0');
    });
  });

  describe('when I create a new section', function () {
    /** @test {ComposerSdk.createSection} */
    it('should have the specified id', () => {
      const section = ComposerSdk.createSection('section0');
      expect(section.id).to.equal('section0');
    });
  });

  describe('when I create a new Field', function () {
    /** @test {ComposerSdk.createField} */
    it('should have the specified name', () => {
      const field = ComposerSdk.createField('field0');
      expect(field.name).to.equal('field0');
    });
  });

  describe('when loading a JSON template', function () {
    /** @test {ComposerSdk.loadFromJSON} */
    it('should load the form as a Form object', () => {
      const obj = {
        info: { title: 'a test title' }
      };
      const form = ComposerSdk.loadFromJSON(obj);
      expect(form).to.be.an.instanceof(Form);
    });
  });
});
