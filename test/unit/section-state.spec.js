import chai from 'chai';
import {SectionState} from '../../src/index.js';

const expect = chai.expect;

var state;

describe('A SectionState instance', function () {
  beforeEach(function () {
    state = new SectionState();
  });

  describe('with an editable flag', function () {
    beforeEach(function () {
      state.editable = true;
    });

    /** @test {SectionState#editable} */
    it('should have the specified editable value', () => {
      expect(state.editable).to.be.true;
    });

    /** @test {SectionState#editable} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editable = 5; }).to.throw(TypeError);
    });

    /** @test {SectionState#toJSON} */
    it('should include the editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.be.true;
    });
  });

  describe('without a editable flag', function () {
    beforeEach(function () {
      state.editable = null;
    });

    /** @test {SectionState#editable} */
    it('should be allowed', () => {
      expect(state.editable).to.be.null;
    });

    /** @test {SectionState#toJSON} */
    it('should not include a editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.not.exist;
    });
  });

  describe('with an editMode flag', function () {
    beforeEach(function () {
      state.editMode = true;
    });

    /** @test {SectionState#editMode} */
    it('should have the specified editMode value', () => {
      expect(state.editMode).to.be.true;
    });

    /** @test {SectionState#editMode} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editMode = 5; }).to.throw(TypeError);
    });

    /** @test {SectionState#toJSON} */
    it('should include the editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.be.true;
    });
  });

  describe('without a editMode flag', function () {
    beforeEach(function () {
      state.editMode = null;
    });

    /** @test {SectionState#editMode} */
    it('should be allowed', () => {
      expect(state.editMode).to.be.null;
    });

    /** @test {SectionState#toJSON} */
    it('should not include a editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.not.exist;
    });
  });

  describe('with an external URL', function () {
    beforeEach(function () {
      state.externalUrl = 'http://www.example.com';
    });

    /** @test {SectionState#externalUrl} */
    it('should have the specified external URL value', () => {
      expect(state.externalUrl).to.equal('http://www.example.com');
    });

    /** @test {SectionState#externalUrl} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { state.externalUrl = 5; }).to.throw(TypeError);
    });

    /** @test {SectionState#toJSON} */
    it('should include the external URL when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.equal('http://www.example.com');
    });
  });

  describe('without an external URL', function () {
    beforeEach(function () {
      state.externalUrl = null;
    });

    /** @test {SectionState#externalUrl} */
    it('should be allowed', () => {
      expect(state.externalUrl).to.be.null;
    });

    /** @test {SectionState#toJSON} */
    it('should not include a externalUrl when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.not.exist;
    });
  });

  describe('with a collapsible flag', function () {
    beforeEach(function () {
      state.collapsible = true;
    });

    /** @test {SectionState#collapsible} */
    it('should have the specified collapsible value', () => {
      expect(state.collapsible).to.be.true;
    });

    /** @test {SectionState#collapsible} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.collapsible = 5; }).to.throw(TypeError);
    });

    /** @test {SectionState#toJSON} */
    it('should include the editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.collapsible).to.be.true;
    });
  });

  describe('without a collapsible flag', function () {
    beforeEach(function () {
      state.collapsible = null;
    });

    /** @test {SectionState#collapsible} */
    it('should be allowed', () => {
      expect(state.collapsible).to.be.null;
    });

    /** @test {SectionState#toJSON} */
    it('should not include a collapsible flag when exporting', () => {
      const output = state.toJSON();
      expect(output.collapsible).to.not.exist;
    });
  });

  describe('with a collapsed flag', function () {
    beforeEach(function () {
      state.collapsed = true;
    });

    /** @test {SectionState#collapsed} */
    it('should have the specified collapsed value', () => {
      expect(state.collapsed).to.be.true;
    });

    /** @test {SectionState#collapsed} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.collapsed = 5; }).to.throw(TypeError);
    });

    /** @test {SectionState#toJSON} */
    it('should include the collapsed flag when exporting', () => {
      const output = state.toJSON();
      expect(output.collapsed).to.be.true;
    });
  });

  describe('without a collapsed flag', function () {
    beforeEach(function () {
      state.collapsed = null;
    });

    /** @test {SectionState#collapsed} */
    it('should be allowed', () => {
      expect(state.collapsed).to.be.null;
    });

    /** @test {SectionState#toJSON} */
    it('should not include a collapsed flag when exporting', () => {
      const output = state.toJSON();
      expect(output.collapsed).to.not.exist;
    });
  });

  /** @test {SectionState#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    state.randomAttribute = 5;
    const output = state.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });
});
