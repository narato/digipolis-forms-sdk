import chai from 'chai';
import {ValidatorFactory, Validator, AdvancedValidator} from '../../src/index.js';

const expect = chai.expect;

/** @test {ValidatorFactory} */
describe('ValidatorFactory', function () {
  describe('createValidatorFromJSON', function () {
    /** @test {ValidatorFactory.createValidatorFromJSON} */
    it('should return a normal validator if no options were present on the input object', () => {
      const obj = `{
        "name": "validator0",
        "type": "required"
      }`;
      const validator = ValidatorFactory.createValidatorFromJSON(obj);
      expect(validator).to.be.an.instanceof(Validator);
      expect(validator).to.not.be.an.instanceof(AdvancedValidator);
    });

    /** @test {ValidatorFactory.createValidatorFromJSON} */
    it('should return an advanced validator if options were present on the input object', () => {
      const obj = `{
        "name": "validator0",
        "type": "required",
        "options": {}
      }`;
      const validator = ValidatorFactory.createValidatorFromJSON(obj);
      expect(validator).to.be.an.instanceof(Validator);
      expect(validator).to.be.an.instanceof(AdvancedValidator);
    });

    /** @test {ValidatorFactory.createValidatorFromJSON} */
    it('should return a normal validator if options were present on the input object, but were null', () => {
      const obj = `{
        "name": "validator0",
        "type": "required",
        "options": null
      }`;
      const validator = ValidatorFactory.createValidatorFromJSON(obj);
      expect(validator).to.be.an.instanceof(Validator);
      expect(validator).to.not.be.an.instanceof(AdvancedValidator);
    });

    /** @test {ValidatorFactory.createValidatorFromJSON} */
    it('should return null if null was given', () => {
      expect(ValidatorFactory.createValidatorFromJSON(null)).to.be.null;
    });
  });
});
