'use strict';

import chai from 'chai';
import {Field, FieldsPrerequisite} from '../../src/index.js';

const expect = chai.expect;

var prerequisites;

/** @test {FieldsPrerequisite} */
describe('A FieldsPrerequisite instance', function () {
  beforeEach(function () {
    prerequisites = new FieldsPrerequisite();
  });

  describe('with a logical', function () {
    beforeEach(function () {
      const l = 'and';
      prerequisites.logical = l;
    });

    /** @test {FieldsPrerequisite#logical} */
    it('should have the specified logical', () => {
      expect(prerequisites.logical).to.equal('and');
    });

    /** @test {FieldsPrerequisite#logical} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.logical = 5; }).to.throw(TypeError);
    });

    /** @test {FieldsPrerequisite#toJSON} */
    it('should include the logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.equal('and');
    });
  });

  describe('without a logical', function () {
    beforeEach(function () {
      prerequisites.logical = null;
    });

    /** @test {FieldsPrerequisite#logical} */
    it('should be allowed', () => {
      expect(prerequisites.logical).to.be.null;
    });

    /** @test {FieldsPrerequisite#toJSON} */
    it('should not include a logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.not.exist;
    });
  });

  describe('with fields names', function () {
    var fields;
    beforeEach(function () {
      fields = ['field0', 'field1'];
      prerequisites.fields = fields;
    });

    /** @test {Prerequisites#fields} */
    it('should have the specified fields', () => {
      expect(prerequisites.fields).to.equal(fields);
    });

    /** @test {Prerequisites#fields} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { prerequisites.fields = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#fields} */
    it('throws an exception when the specified value is not an Array of strings', () => {
      expect(() => { prerequisites.fields = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the fields when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fields).to.exist;
    });
  });

  describe('with an empty fields array', function () {
    beforeEach(function () {
      prerequisites.fields = [];
    });

    /** @test {Prerequisites#fields} */
    it('should be allowed', () => {
      expect(prerequisites.fields).to.be.empty;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include fields when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('without fields', function () {
    beforeEach(function () {
      prerequisites.fields = null;
    });

    /** @test {Prerequisites#fields} */
    it('should be allowed', () => {
      expect(prerequisites.fields).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include fields when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('when adding a field', function () {
    /** @test {FieldsPrerequisite#addField} */
    it('should have added the field to the prerequisites', () => {
      let field = new Field('field0');
      prerequisites.addField(field);
      let anotherField = new Field('field1');
      prerequisites.addField(anotherField);

      expect(prerequisites.fields[0]).to.equal('field0');
      expect(prerequisites.fields[1]).to.equal('field1');
    });

    /** @test {FieldsPrerequisite#addFieldId} */
    it('should have added the field name to the prerequisites', () => {
      prerequisites.addFieldId('field0');
      prerequisites.addFieldId('field1');

      expect(prerequisites.fields[0]).to.equal('field0');
      expect(prerequisites.fields[1]).to.equal('field1');
    });

    /** @test {FieldsPrerequisite#addFieldId} */
    it('should not allow a duplicate field', () => {
      prerequisites.addFieldId('field0');
      let addDuplicateField = () => prerequisites.addFieldId('field0');

      expect(addDuplicateField).to.throw(/Field name/);
    });

    /** @test {FieldsPrerequisite#addField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { prerequisites.addField(5); }).to.throw(TypeError);
    });

    /** @test {FieldsPrerequisite#addFieldId} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.addFieldId(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a field', function () {
    /** @test {FieldsPrerequisite#removeFieldId} */
    it('should have removed the field name of the prerequisites', () => {
      prerequisites.addFieldId('field0');
      prerequisites.addFieldId('field1');
      prerequisites.addFieldId('field2');
      expect(prerequisites.fields.length).to.equal(3);

      prerequisites.removeFieldId('field1');

      expect(prerequisites.fields.length).to.equal(2);
      expect(prerequisites.fields).not.to.include('field1');
    });

    /** @test {FieldsPrerequisite#removeField} */
    it('should have removed the field of the prerequisites', () => {
      prerequisites.addFieldId('field0');
      prerequisites.addFieldId('field1');
      prerequisites.addFieldId('field2');
      expect(prerequisites.fields.length).to.equal(3);

      prerequisites.removeField(new Field('field1'));

      expect(prerequisites.fields.length).to.equal(2);
      expect(prerequisites.fields).not.to.include('field1');
    });

    /** @test {FieldsPrerequisite#removeFieldId} */
    it('throws an error if specified field does not exist', () => {
      let removeNonExistingField = () => prerequisites.removeFieldId('field0');

      expect(removeNonExistingField).to.throw(Error);
    });

    /** @test {FieldsPrerequisite#removeFieldId} */
    it('throws an exception when the specified value is not a String', () => {
      expect(() => { prerequisites.removeFieldId(5); }).to.throw(TypeError);
    });

    /** @test {FieldsPrerequisite#removeField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { prerequisites.removeField(5); }).to.throw(TypeError);
    });
  });

  /** @test {FieldsPrerequisite#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    prerequisites.randomAttribute = 5;
    const output = prerequisites.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldsPrerequisite.fromJSON} */
    it('should return a FieldsPrerequisite instance', () => {
      const jsonData = '{ "logical": "and" }';
      const prerequisites = FieldsPrerequisite.fromJSON(jsonData);
      expect(prerequisites).to.be.an.instanceof(FieldsPrerequisite);
    });
  });
});
