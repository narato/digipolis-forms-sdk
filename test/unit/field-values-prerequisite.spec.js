'use strict';

import chai from 'chai';
import {FieldValueOperand, FieldValuesPrerequisite} from '../../src/index.js';

const expect = chai.expect;

var prerequisites;

/** @test {FieldValuesPrerequisite} */
describe('A FieldValuesPrerequisite instance', function () {
  beforeEach(function () {
    prerequisites = new FieldValuesPrerequisite();
  });

  describe('with a logical', function () {
    beforeEach(function () {
      const l = 'and';
      prerequisites.logical = l;
    });

    /** @test {FieldValuesPrerequisite#logical} */
    it('should have the specified logical', () => {
      expect(prerequisites.logical).to.equal('and');
    });

    /** @test {FieldValuesPrerequisite#logical} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.logical = 5; }).to.throw(TypeError);
    });

    /** @test {FieldValuesPrerequisite#toJSON} */
    it('should include the logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.equal('and');
    });
  });

  describe('without a logical', function () {
    beforeEach(function () {
      prerequisites.logical = null;
    });

    /** @test {FieldValuesPrerequisite#logical} */
    it('should be allowed', () => {
      expect(prerequisites.logical).to.be.null;
    });

    /** @test {FieldValuesPrerequisite#toJSON} */
    it('should not include a logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.not.exist;
    });
  });

  describe('with operands', function () {
    var operands;
    beforeEach(function () {
      operands = [new FieldValueOperand(), new FieldValueOperand()];
      prerequisites.operands = operands;
    });

    /** @test {Prerequisites#operands} */
    it('should have the specified operands', () => {
      expect(prerequisites.operands).to.equal(operands);
    });

    /** @test {Prerequisites#operands} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { prerequisites.operands = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#operands} */
    it('throws an exception when the specified value is not an Array of FieldValueOperands', () => {
      expect(() => { prerequisites.operands = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the operands when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.operands).to.exist;
    });
  });

  describe('with an empty operands array', function () {
    beforeEach(function () {
      prerequisites.operands = [];
    });

    /** @test {Prerequisites#operands} */
    it('should be allowed', () => {
      expect(prerequisites.operands).to.be.empty;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include operands when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.operands).to.not.exist;
    });
  });

  describe('without operands', function () {
    beforeEach(function () {
      prerequisites.operands = null;
    });

    /** @test {Prerequisites#operands} */
    it('should be allowed', () => {
      expect(prerequisites.operands).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include operands when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.operands).to.not.exist;
    });
  });

  describe('when adding an operand', function () {
    /** @test {FieldValuesPrerequisite#addOperand} */
    it('should have added the operand to the prerequisites', () => {
      let operand = new FieldValueOperand();
      prerequisites.addOperand(operand);
      let anotherOperand = new FieldValueOperand();
      prerequisites.addOperand(anotherOperand);

      expect(prerequisites.operands[0]).to.equal(operand);
      expect(prerequisites.operands[1]).to.equal(anotherOperand);
    });

    /** @test {FieldValuesPrerequisite#addOperand} */
    it('throws an exception when the specified value is not a FieldValueOperand', () => {
      expect(() => { prerequisites.addOperand(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a operand', function () {
    /** @test {FieldValuesPrerequisite#removeOperand} */
    it('should have removed the operand id of the prerequisites', () => {
      let operand1 = new FieldValueOperand();
      operand1.name = 'field1';
      prerequisites.addOperand(operand1);
      let operand2 = new FieldValueOperand();
      operand2.name = 'field2';
      prerequisites.addOperand(operand2);
      let operand3 = new FieldValueOperand();
      operand3.name = 'field3';
      prerequisites.addOperand(operand3);
      expect(prerequisites.operands.length).to.equal(3);

      prerequisites.removeOperand(operand2);

      expect(prerequisites.operands.length).to.equal(2);
      expect(prerequisites.operands).not.to.include(operand2);
    });

    /** @test {FieldValuesPrerequisite#removeOperand} */
    it('throws an error if specified operand does not exist', () => {
      let removeNonExistingOperand = () => prerequisites.removeOperand(new FieldValueOperand());

      expect(removeNonExistingOperand).to.throw(Error);
    });

    /** @test {FieldValuesPrerequisite#removeOperand} */
    it('throws an exception when the specified value is not a Operand', () => {
      expect(() => { prerequisites.removeOperand(5); }).to.throw(TypeError);
    });
  });

  /** @test {FieldValuesPrerequisite#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    prerequisites.randomAttribute = 5;
    const output = prerequisites.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldValuesPrerequisite.fromJSON} */
    it('should return a FieldValuesPrerequisite instance', () => {
      const jsonData = '{ "logical": "and" }';
      const prerequisites = FieldValuesPrerequisite.fromJSON(jsonData);
      expect(prerequisites).to.be.an.instanceof(FieldValuesPrerequisite);
    });

    /** @test {Prerequisites.fromJSON} */
    it('should include operands as FieldValueOperand objects', () => {
      const jsonData = `{ "logical": "and",
                          "operands": [
                              { "name": "field0", "value": "bar", "operator": "eq" }
                           ]
                        }`;
      const prerequisites = FieldValuesPrerequisite.fromJSON(jsonData);
      expect(prerequisites.operands[0]).to.be.an.instanceof(FieldValueOperand);
    });
  });
});
