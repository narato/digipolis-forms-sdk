import chai from 'chai';
import {FieldInputFilter, Validator, AdvancedValidator, ValidatorOptions} from '../../src/index.js';

const expect = chai.expect;

var inputFilter;

/** @test {FieldInputFilter} */
describe('A FieldInputFilter instance', function () {
  beforeEach(function () {
    inputFilter = new FieldInputFilter();
  });

  describe('with a required flag', function () {
    beforeEach(function () {
      inputFilter.required = true;
    });

    /** @test {FieldFieldInputFilter#required} */
    it('should have the specified required value', () => {
      expect(inputFilter.required).to.be.true;
    });

    /** @test {FieldFieldInputFilter#required} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { inputFilter.required = 5; }).to.throw(TypeError);
    });

    /** @test {FieldFieldInputFilter#toJSON} */
    it('should include the required flag when exporting', () => {
      const output = inputFilter.toJSON();
      expect(output.required).to.be.true;
    });
  });

  describe('without a required flag', function () {
    beforeEach(function () {
      inputFilter.required = null;
    });

    /** @test {FieldFieldInputFilter#required} */
    it('should be allowed', () => {
      expect(inputFilter.required).to.be.null;
    });

    /** @test {FieldFieldInputFilter#toJSON} */
    it('should not include the required flag when exporting', () => {
      const output = inputFilter.toJSON();
      expect(output.required).to.not.exist;
    });
  });

  describe('with validators', function () {
    var validators;
    beforeEach(function () {
      validators = [new Validator('validator0', 'required'), new Validator('validator1', 'required')];
      inputFilter.validators = validators;
    });

    /** @test {FieldInputFilter#validators} */
    it('should have the specified fieds', () => {
      expect(inputFilter.validators).to.equal(validators);
    });

    /** @test {FieldInputFilter#validators} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { inputFilter.validators = 5; }).to.throw(TypeError);
    });

    /** @test {FieldInputFilter#validators} */
    it('throws an exception when the specified value is not an Array of validators', () => {
      expect(() => { inputFilter.validators = [1, 2, 3]; }).to.throw(TypeError);
    });

    it('should allow both validators and advancedValidators', () => {
      let validator = new Validator('validator3', 'required');
      let advancedValidator = new AdvancedValidator('validator4', 'regexp', new ValidatorOptions());
      inputFilter.addValidator(validator);
      expect(() => inputFilter.addValidator(advancedValidator)).to.not.throw();
    });

    /** @test {FieldInputFilter#toJSON} */
    it('should include the validators when exporting', () => {
      const output = inputFilter.toJSON();
      expect(output.validators).to.exist;
    });
  });

  describe('with an empty validators array', function () {
    beforeEach(function () {
      inputFilter.validators = [];
    });

    /** @test {FieldInputFilter#validators} */
    it('should be allowed', () => {
      expect(inputFilter.validators).to.be.empty;
    });

    /** @test {FieldInputFilter#toJSON} */
    it('should not include validators when exporting', () => {
      const output = inputFilter.toJSON();
      expect(output.validators).to.not.exist;
    });
  });

  describe('without validators', function () {
    beforeEach(function () {
      inputFilter.validators = null;
    });

    /** @test {FieldInputFilter#validators} */
    it('should be allowed', () => {
      expect(inputFilter.validators).to.be.null;
    });

    /** @test {FieldInputFilter#toJSON} */
    it('should not include validators when exporting', () => {
      const output = inputFilter.toJSON();
      expect(output.validators).to.not.exist;
    });
  });

  describe('when adding a Validator', function () {
    /** @test {FieldInputFilter#addValidator} */
    it('should have added the validator to the section', () => {
      let validator = new Validator('validator0', 'required');
      inputFilter.addValidator(validator);
      let anotherValidator = new Validator('validator1', 'required');
      inputFilter.addValidator(anotherValidator);

      expect(inputFilter.validators[0]).to.equal(validator);
      expect(inputFilter.validators[1]).to.equal(anotherValidator);
    });

    /** @test {FieldInputFilter#addValidator} */
    it('should not allow a duplicate validator', () => {
      let validator = new Validator('validator0', 'required');
      inputFilter.addValidator(validator);
      let addDuplicateValidator = () => inputFilter.addValidator(validator);

      expect(addDuplicateValidator).to.throw(/Validator with name/);
    });

    /** @test {FieldInputFilter#addValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { inputFilter.addValidator(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a validator', function () {
    /** @test {FieldInputFilter#getValidator} */
    it('should return the validator with the specified name', () => {
      let validator0 = new Validator('validator0', 'required');
      inputFilter.addValidator(validator0);
      inputFilter.addValidator(new Validator('validator1', 'required'));
      inputFilter.addValidator(new Validator('validator2', 'required'));

      expect(inputFilter.getValidator('validator0')).to.equal(validator0);
    });

    /** @test {FieldInputFilter#getValidator} */
    it('should return undefined if a validator with the specified name does not exist', () => {
      inputFilter.addValidator(new Validator('validator0', 'required'));
      inputFilter.addValidator(new Validator('validator1', 'required'));
      inputFilter.addValidator(new Validator('validator2', 'required'));

      expect(inputFilter.getValidator('validator4')).to.be.undefined;
    });
  });

  describe('when updating a validator', function () {
    /** @test {FieldInputFilter#updateValidator} */
    it('should have updated the validator of the section', () => {
      inputFilter.addValidator(new Validator('validator0', 'required'));
      inputFilter.addValidator(new Validator('validator1', 'required'));
      inputFilter.addValidator(new Validator('validator2', 'required'));

      let updatedValidator = new Validator('validator1', 'required');
      inputFilter.updateValidator(updatedValidator);

      expect(inputFilter.validators.length).to.equal(3);
      expect(inputFilter.validators[1]).to.equal(updatedValidator);
    });

    /** @test {FieldInputFilter#updateValidator} */
    it('throws an error if specified validator does not exist', () => {
      let validator = new Validator('validator0', 'required');
      let updateNonExistingValidator = () => inputFilter.updateValidator(validator);

      expect(updateNonExistingValidator).to.throw(Error);
    });

    /** @test {FieldInputFilter#updateValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { inputFilter.updateValidator(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a validator', function () {
    /** @test {FieldInputFilter#removeValidator} */
    it('should have removed the validator of the section', () => {
      inputFilter.addValidator(new Validator('validator0', 'required'));
      let validatorToRemove = new Validator('validator1', 'required');
      inputFilter.addValidator(validatorToRemove);
      inputFilter.addValidator(new Validator('validator2', 'required'));
      expect(inputFilter.validators.length).to.equal(3);

      inputFilter.removeValidator(validatorToRemove);

      expect(inputFilter.validators.length).to.equal(2);
      expect(inputFilter.validators).not.to.include(validatorToRemove);
    });

    /** @test {FieldInputFilter#removeValidator} */
    it('throws an error if specified validator does not exist', () => {
      let validator = new Validator('validator0', 'required');
      let removeNonExistingValidator = () => inputFilter.removeValidator(validator);

      expect(removeNonExistingValidator).to.throw(Error);
    });

    /** @test {FieldInputFilter#removeValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { inputFilter.removeValidator(5); }).to.throw(TypeError);
    });
  });

  /** @test {FieldInputFilter#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    inputFilter.randomAttribute = 5;
    const output = inputFilter.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldInputFilter.fromJSON} */
    it('should return a FieldInputFilter instance', () => {
      let jsonData = '{"required": true}';
      let inputFilter = FieldInputFilter.fromJSON(jsonData);
      expect(inputFilter.required).to.be.true;
    });

    /** @test {FieldInputFilter.fromJSON} */
    it('should include validators as Validator objects', () => {
      const obj = `{
        "required": true,
        "validators": [ { "name": "validator0", "type": "required" }, { "name": "validator1", "type": "required" } ]
      }`;
      const inputFilter = FieldInputFilter.fromJSON(obj);
      expect(inputFilter.validators[0]).to.be.an.instanceof(Validator);
      expect(inputFilter.validators[0].name).to.equal("validator0");
      expect(inputFilter.validators[1]).to.be.an.instanceof(Validator);
      expect(inputFilter.validators[1].name).to.equal("validator1");
    });

    /** @test {FieldInputFilter.fromJSON} */
    it('should include advancedValidators as AdvancedValidator objects', () => {
      const obj = `{
        "required": true,
        "validators": [ { "name": "validator0", "type": "required" }, { "name": "validator1", "type": "required", "options": {} } ]
      }`;
      const inputFilter = FieldInputFilter.fromJSON(obj);
      expect(inputFilter.validators[0]).to.be.an.instanceof(Validator);
      expect(inputFilter.validators[0]).to.not.be.an.instanceof(AdvancedValidator);
      expect(inputFilter.validators[1]).to.be.an.instanceof(Validator);
      expect(inputFilter.validators[1]).to.be.an.instanceof(AdvancedValidator);
    });
  });
});
