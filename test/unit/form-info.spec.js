'use strict';

import chai from 'chai';
import {FormInfo} from '../../src/index.js';

const expect = chai.expect;

var info;

/** @test {FormInfo} */
describe('A FormInfo instance', function () {
  beforeEach(function () {
    info = new FormInfo();
  });

  describe('with a title', function () {
    beforeEach(function () {
      const l = 'example title';
      info.title = l;
    });

    /** @test {FormInfo#title} */
    it('should have the specified title', () => {
      expect(info.title).to.equal('example title');
    });

    /** @test {FormInfo#title} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { info.title = 5; }).to.throw(TypeError);
    });

    /** @test {FormInfo#toJSON} */
    it('should include the title when exporting', () => {
      const output = info.toJSON();
      expect(output.title).to.equal('example title');
    });
  });

  describe('without a title', function () {
    beforeEach(function () {
      info.title = null;
    });

    /** @test {FormInfo#title} */
    it('should be allowed', () => {
      expect(info.title).to.be.null;
    });

    /** @test {FormInfo#toJSON} */
    it('should not include a title when exporting', () => {
      const output = info.toJSON();
      expect(output.title).to.not.exist;
    });
  });

  describe('with a body', function () {
    beforeEach(function () {
      const l = 'example body';
      info.body = l;
    });

    /** @test {FormInfo#body} */
    it('should have the specified body', () => {
      expect(info.body).to.equal('example body');
    });

    /** @test {FormInfo#body} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { info.body = 5; }).to.throw(TypeError);
    });

    /** @test {FormInfo#toJSON} */
    it('should include the body when exporting', () => {
      const output = info.toJSON();
      expect(output.body).to.equal('example body');
    });
  });

  describe('without a body', function () {
    beforeEach(function () {
      info.body = null;
    });

    /** @test {FormInfo#body} */
    it('should be allowed', () => {
      expect(info.body).to.be.null;
    });

    /** @test {FormInfo#toJSON} */
    it('should not include a body when exporting', () => {
      const output = info.toJSON();
      expect(output.body).to.not.exist;
    });
  });

  /** @test {FormInfo#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    info.randomAttribute = 5;
    const output = info.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FormInfo.fromJSON} */
    it('should return a FormInfo instance', () => {
      const jsonData = '{ "title": "a test title" }';
      const info = FormInfo.fromJSON(jsonData);
      expect(info).to.be.an.instanceof(FormInfo);
      expect(info.title).to.equal('a test title');
    });
  });

});
