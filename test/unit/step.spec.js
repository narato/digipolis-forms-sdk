import chai from 'chai';
import {Field, NavigationTexts, Prerequisites, Section, Step, StepState} from '../../src/index.js';

const expect = chai.expect;

var step;

/** @test {Step} */
describe('Creating a Step instance', function () {
  /** @test {Step#id} */
  it('throws an exception without an id', () => {
    expect(() => { const step = new Step(null); }).to.throw(/Step must have an id/);
  });

  /** @test {Step#id} */
  it('throws an exception when specified value is not a string', () => {
    expect(() => { const step = new Step(5); }).to.throw(TypeError);
  });
});

/** @test {Step} */
describe('A Step instance', function () {
  beforeEach(function () {
    step = new Step('step0');
  });

  describe('with an id', function () {
    /** @test {Step#id} */
    it('should use id from constructor', () => {
      expect(step.id).to.equal('step0');
    });

    /** @test {Step#toJSON} */
    it('should include the id when exporting', () => {
      const output = step.toJSON();
      expect(output.id).to.equal('step0');
    });
  });

  describe('with a title', function () {
    beforeEach(function () {
      step.title = 'title0';
    });

    /** @test {Step#title} */
    it('should have the specified title value', () => {
      expect(step.title).to.equal('title0');
    });

    /** @test {Step#title} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { step.title = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the title when exporting', () => {
      const output = step.toJSON();
      expect(output.title).to.equal('title0');
    });

  });

  describe('without a title', function () {
    beforeEach(function () {
      step.title = null;
    });

    /** @test {Step#title} */
    it('should be allowed', () => {
      expect(step.title).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include the title when exporting', () => {
      const output = step.toJSON();
      expect(output.title).to.not.exist;
    });
  });

  describe('with a subtitle', function () {
    beforeEach(function () {
      step.subtitle = 'subtitle0';
    });

    /** @test {Step#subtitle} */
    it('should have the specified subtitle value', () => {
      expect(step.subtitle).to.equal('subtitle0');
    });

    /** @test {Step#subtitle} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { step.subtitle = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the subtitle when exporting', () => {
      const output = step.toJSON();
      expect(output.subtitle).to.equal('subtitle0');
    });
  });

  describe('without a subtitle', function () {
    beforeEach(function () {
      step.subtitle = null;
    });

    /** @test {Step#subtitle} */
    it('should be allowed', () => {
      expect(step.subtitle).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include the subtitle when exporting', () => {
      const output = step.toJSON();
      expect(output.subtitle).to.not.exist;
    });
  });

  describe('with a body', function () {
    beforeEach(function () {
      step.body = '<i>some text</i>';
    });

    /** @test {Step#body} */
    it('should have the specified body value', () => {
      expect(step.body).to.equal('<i>some text</i>');
    });

    /** @test {Step#body} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { step.body = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the body when exporting', () => {
      const output = step.toJSON();
      expect(output.body).to.equal('<i>some text</i>');
    });
  });

  describe('without a body', function () {
    beforeEach(function () {
      step.body = null;
    });

    /** @test {Step#body} */
    it('should be allowed', () => {
      expect(step.body).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include the body when exporting', () => {
      const output = step.toJSON();
      expect(output.body).to.not.exist;
    });
  });

  describe('with a type', function () {
    beforeEach(function () {
      step.type = 'intro';
    });

    /** @test {Step#type} */
    it('should have the specified type value', () => {
      expect(step.type).to.equal('intro');
    });

    /** @test {Step#type} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { step.type = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the type when exporting', () => {
      const output = step.toJSON();
      expect(output.type).to.equal('intro');
    });
  });

  describe('without a type', function () {
    beforeEach(function () {
      step.type = null;
    });

    /** @test {Step#type} */
    it('should be allowed', () => {
      expect(step.type).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include the type when exporting', () => {
      const output = step.toJSON();
      expect(output.type).to.not.exist;
    });
  });

  describe('with a state', function () {
    var s;
    beforeEach(function () {
      s = new StepState();
      s.editMode = true;
      step.state = s;
    });

    /** @test {Step#state} */
    it('should have the specified state value', () => {
      expect(step.state).to.equal(s);
    });

    /** @test {Step#state} */
    it('throws an exception when the specified value is not a StepState object', () => {
      expect(() => { step.state = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the state when exporting', () => {
      const output = step.toJSON();
      expect(output.state).to.exist;
    });
  });

  describe('without a state', function () {
    beforeEach(function () {
      step.state = null;
    });

    /** @test {Step#state} */
    it('should be allowed', () => {
      expect(step.state).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include a state when exporting', () => {
      const output = step.toJSON();
      expect(output.state).to.not.exist;
    });
  });

  describe('with prerequisites', function () {
    var p;
    beforeEach(function () {
      p = new Prerequisites();
      step.prerequisites = p;
    });

    /** @test {Step#prerequisites} */
    it('should have the specified prerequisites', () => {
      expect(step.prerequisites).to.equal(p);
    });

    /** @test {Step#prerequisites} */
    it('throws an exception when the specified value is not a Prerequisites object', () => {
      expect(() => { step.prerequisites = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the prerequisites when exporting', () => {
      const output = step.toJSON();
      expect(output.prerequisites).to.exist;
    });
  });

  describe('without prerequisites', function () {
    beforeEach(function () {
      step.prerequisites = null;
    });

    /** @test {Step#prerequisites} */
    it('should be allowed', () => {
      expect(step.prerequisites).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include a prerequisites when exporting', () => {
      const output = step.toJSON();
      expect(output.prerequisites).to.not.exist;
    });
  });

  describe('with navigation texts', function () {
    var n;
    beforeEach(function () {
      n = new NavigationTexts();
      step.navigationTexts = n;
    });

    /** @test {Step#navigationTexts} */
    it('should have the specified navigation texts', () => {
      expect(step.navigationTexts).to.equal(n);
    });

    /** @test {Step#navigationTexts} */
    it('throws an exception when the specified value is not a NavigationTexts object', () => {
      expect(() => { step.navigationTexts = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the navigation texts when exporting', () => {
      const output = step.toJSON();
      expect(output.navigationTexts).to.exist;
    });
  });

  describe('without navigation texts', function () {
    beforeEach(function () {
      step.navigationTexts = null;
    });

    /** @test {Step#navigationTexts} */
    it('should be allowed', () => {
      expect(step.navigationTexts).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include navigation texts when exporting', () => {
      const output = step.toJSON();
      expect(output.navigationTexts).to.not.exist;
    });
  });

  describe('with sections', function () {
    var sections;
    beforeEach(function () {
      sections = [new Section('section0'), new Section('section1')];
      step.sections = sections;
    });

    /** @test {Step#sections} */
    it('should have the specified sections', () => {
      expect(step.sections).to.equal(sections);
    });

    /** @test {Step#sections} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { step.sections = 5; }).to.throw(TypeError);
    });

    /** @test {Step#sections} */
    it('throws an exception when the specified value is not an Array of Sections', () => {
      expect(() => { step.sections = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the navigation texts when exporting', () => {
      const output = step.toJSON();
      expect(output.navigationTexts).to.exist;
    });
  });

  describe('with an empty sections array', function () {
    beforeEach(function () {
      step.sections = [];
    });

    /** @test {Step#sections} */
    it('should be allowed', () => {
      expect(step.sections).to.be.empty;
    });

    /** @test {Step#toJSON} */
    it('should not include sections when exporting', () => {
      const output = step.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('without sections', function () {
    beforeEach(function () {
      step.sections = null;
    });

    /** @test {Step#sections} */
    it('should be allowed', () => {
      expect(step.sections).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include sections when exporting', () => {
      const output = step.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('with fields', function () {
    var fields;
    beforeEach(function () {
      fields = [new Field('field0'), new Field('field1')];
      step.fields = fields;
    });

    /** @test {Step#fields} */
    it('should have the specified fieds', () => {
      expect(step.fields).to.equal(fields);
    });

    /** @test {Step#fields} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { step.fields = 5; }).to.throw(TypeError);
    });

    /** @test {Step#fields} */
    it('throws an exception when the specified value is not an Array of Fields', () => {
      expect(() => { step.fields = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the fields when exporting', () => {
      const output = step.toJSON();
      expect(output.fields).to.exist;
    });
  });

  describe('with an empty fields array', function () {
    beforeEach(function () {
      step.fields = [];
    });

    /** @test {Step#fields} */
    it('should be allowed', () => {
      expect(step.fields).to.be.empty;
    });

    /** @test {Step#toJSON} */
    it('should not include fields when exporting', () => {
      const output = step.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('without fields', function () {
    beforeEach(function () {
      step.fields = null;
    });

    /** @test {Step#fields} */
    it('should be allowed', () => {
      expect(step.fields).to.be.null;
    });

    /** @test {Step#toJSON} */
    it('should not include fields when exporting', () => {
      const output = step.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('when adding a section', function () {
    /** @test {Step#addSection} */
    it('should have added the section to the step', () => {
      let section = new Section('section0');
      step.addSection(section);
      let anotherSection = new Section('section1');
      step.addSection(anotherSection);

      expect(step.sections[0]).to.equal(section);
      expect(step.sections[1]).to.equal(anotherSection);
    });

    /** @test {Step#addSection} */
    it('should not allow a duplicate section', () => {
      let section = new Section('section0');
      step.addSection(section);
      let addDuplicateSection = () => step.addSection(section);

      expect(addDuplicateSection).to.throw(/Section with id/);
    });

    /** @test {Step#addSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { step.addSection(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a section', function () {
    /** @test {Step#getSection} */
    it('should return the section with the specified name', () => {
      let section0 = new Section('section0');
      step.addSection(section0);
      step.addSection(new Section('section1'));
      step.addSection(new Section('section2'));

      expect(step.getSection('section0')).to.equal(section0);
    });

    /** @test {Step#getSection} */
    it('should return undefined if a section with the specified name does not exist', () => {
      step.addSection(new Section('section0'));
      step.addSection(new Section('section1'));
      step.addSection(new Section('section2'));

      expect(step.getSection('section4')).to.be.undefined;
    });
  });

  describe('when updating a section', function () {
    /** @test {Step#updateSection} */
    it('should have updated the section of the step', () => {
      step.addSection(new Section('section0'));
      step.addSection(new Section('section1'));
      step.addSection(new Section('section2'));

      let updatedSection = new Section('section1');
      step.updateSection(updatedSection);

      expect(step.sections.length).to.equal(3);
      expect(step.sections[1]).to.equal(updatedSection);
    });

    /** @test {Step#updateSection} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let updateNonExistingSection = () => step.updateSection(section);

      expect(updateNonExistingSection).to.throw(Error);
    });

    /** @test {Step#updateSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { step.updateSection(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a section', function () {
    /** @test {Step#removeSection} */
    it('should have removed the section of the step', () => {
      step.addSection(new Section('section0'));
      let sectionToRemove = new Section('section1');
      step.addSection(sectionToRemove);
      step.addSection(new Section('section2'));
      expect(step.sections.length).to.equal(3);

      step.removeSection(sectionToRemove);

      expect(step.sections.length).to.equal(2);
      expect(step.sections).not.to.include(sectionToRemove);
    });

    /** @test {Step#removeSection} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let removeNonExistingSection = () => step.removeSection(section);

      expect(removeNonExistingSection).to.throw(Error);
    });

    /** @test {Step#removeSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { step.removeSection(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a section', function () {
    /** @test {Step#moveSectionToIndex} */
    it('should have moved the section to the specified index', () => {
      let firstSection = new Section('section0');
      let secondSection = new Section('section1');
      let thirdSection = new Section('section2');

      step.addSection(firstSection);
      step.addSection(secondSection);
      step.addSection(thirdSection);
      expect(step.sections[0]).to.equal(firstSection);

      step.moveSectionToIndex(thirdSection, 0);

      expect(step.sections[0]).to.equal(thirdSection);
    });

    /** @test {Step#moveSectionToIndex} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let moveNonExistingSection = () => step.moveSectionToIndex(section, 0);

      expect(moveNonExistingSection).to.throw(Error);
    });

    /** @test {Step#moveSectionToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let section = new Section('section0');
      step.addSection(section);
      let moveOUBindex = () => step.moveSectionToIndex(section, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Step#moveSectionToIndex} */
    it('throws an error if no index was specified', () => {
      let section = new Section('section0');
      step.addSection(section);
      let moveOUBindex = () => step.moveSectionToIndex(section);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Step#moveSectionToIndex} */
    it('throws an exception when the specified section is not a Section', () => {
      expect(() => { step.moveSectionToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Step#moveSectionToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { step.moveSectionToIndex(new Section('section0'), 'test'); }).to.throw(TypeError);
    });
  });

  describe('when adding a field', function () {
    /** @test {Step#addField} */
    it('should have added the field to the step', () => {
      let field = new Field('field0');
      step.addField(field);
      let anotherField = new Field('field1');
      step.addField(anotherField);

      expect(step.fields[0]).to.equal(field);
      expect(step.fields[1]).to.equal(anotherField);
    });

    /** @test {Step#addField} */
    it('should not allow a duplicate field', () => {
      let field = new Field('field0');
      step.addField(field);
      let addDuplicateField = () => step.addField(field);

      expect(addDuplicateField).to.throw(/Field with name/);
    });

    /** @test {Step#addField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { step.addField(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a field', function () {
    /** @test {Step#getField} */
    it('should return the field with the specified name', () => {
      let field0 = new Field('field0');
      step.addField(field0);
      step.addField(new Field('field1'));
      step.addField(new Field('field2'));

      expect(step.getField('field0')).to.equal(field0);
    });

    /** @test {Step#getField} */
    it('should return undefined if a field with the specified name does not exist', () => {
      step.addField(new Field('field0'));
      step.addField(new Field('field1'));
      step.addField(new Field('field2'));

      expect(step.getField('field4')).to.be.undefined;
    });
  });

  describe('when updating a field', function () {
    /** @test {Step#updateField} */
    it('should have updated the field of the step', () => {
      step.addField(new Field('field0'));
      step.addField(new Field('field1'));
      step.addField(new Field('field2'));

      let updatedField = new Field('field1');
      step.updateField(updatedField);

      expect(step.fields.length).to.equal(3);
      expect(step.fields[1]).to.equal(updatedField);
    });

    /** @test {Step#updateField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let updateNonExistingField = () => step.updateField(field);

      expect(updateNonExistingField).to.throw(Error);
    });

    /** @test {Step#updateField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { step.updateField(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a field', function () {
    /** @test {Step#removeField} */
    it('should have removed the field of the step', () => {
      step.addField(new Field('field0'));
      let fieldToRemove = new Field('field1');
      step.addField(fieldToRemove);
      step.addField(new Field('field2'));
      expect(step.fields.length).to.equal(3);

      step.removeField(fieldToRemove);

      expect(step.fields.length).to.equal(2);
      expect(step.fields).not.to.include(fieldToRemove);
    });

    /** @test {Step#removeField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let removeNonExistingField = () => step.removeField(field);

      expect(removeNonExistingField).to.throw(Error);
    });

    /** @test {Step#removeField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { step.removeField(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a field', function () {
    /** @test {Step#moveFieldToIndex} */
    it('should have moved the field to the specified index', () => {
      let firstField = new Field('field0');
      let secondField = new Field('field1');
      let thirdField = new Field('field2');

      step.addField(firstField);
      step.addField(secondField);
      step.addField(thirdField);
      expect(step.fields[0]).to.equal(firstField);

      step.moveFieldToIndex(thirdField, 0);

      expect(step.fields[0]).to.equal(thirdField);
    });

    /** @test {Step#moveFieldToIndex} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let moveNonExistingField = () => step.moveFieldToIndex(field, 0);

      expect(moveNonExistingField).to.throw(Error);
    });

    /** @test {Step#moveFieldToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let field = new Field('field0');
      step.addField(field);
      let moveOUBindex = () => step.moveFieldToIndex(field, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Step#moveFieldToIndex} */
    it('throws an error if no index was specified', () => {
      let field = new Field('field0');
      step.addField(field);
      let moveOUBindex = () => step.moveFieldToIndex(field);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Step#moveFieldToIndex} */
    it('throws an exception when the specified field is not a Field', () => {
      expect(() => { step.moveFieldToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Step#moveFieldToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { step.moveFieldToIndex(new Field('field0'), 'test'); }).to.throw(TypeError);
    });
  });

  /** @test {Step#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    step.randomAttribute = 5;
    const output = step.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {Step.fromJSON} */
    it('should return a Step instance', () => {
      const jsonData = '{"id":"step0","title":"step title"}';
      const step = Step.fromJSON(jsonData);
      expect(step).to.be.an.instanceof(Step);
      expect(step.id).to.equal('step0');
    });

    /** @test {Step.fromJSON} */
    it('should contain a nested StepState object', () => {
      const jsonData = `{
        "id": "step0",
        "title": "step title",
        "state": { "editMode": true }
      }`;
      const step = Step.fromJSON(jsonData);
      expect(step.state).to.be.an.instanceof(StepState);
    });

    /** @test {Step.fromJSON} */
    it('should contain a nested Prerequisites object', () => {
      const jsonData = `{
        "id": "step0",
        "prerequisites": { "sectionsCompleted": { "logical": "and" } }
      }`;
      const step = Step.fromJSON(jsonData);
      expect(step.prerequisites).to.be.an.instanceof(Prerequisites);
    });

    /** @test {Form.fromJSON} */
    it('should include sections as Section objects', () => {
      const obj = `{
        "id": "step0",
        "title": "step title",
        "sections": [ { "id": "section0" } ]
      }`;
      const step = Step.fromJSON(obj);
      expect(step.sections[0]).to.be.an.instanceof(Section);
    });

    /** @test {Form.fromJSON} */
    it('should include fields as Field objects', () => {
      const obj = `{
        "id": "step0",
        "title": "step title",
        "fields": [ { "name": "field0" } ]
      }`;
      const step = Step.fromJSON(obj);
      expect(step.fields[0]).to.be.an.instanceof(Field);
    });

    /** @test {Step.fromJSON} */
    it('should contain a nested NavigationTexts object', () => {
      const jsonData = `{
        "id": "step0",
        "title": "step title",
        "navigationTexts": { "next": "next test label" }
      }`;
      const step = Step.fromJSON(jsonData);
      expect(step.navigationTexts).to.be.an.instanceof(NavigationTexts);
    });

    /** @test {Step.fromJSON} */
    it('should return null if null was given', () => {
      expect(Step.fromJSON(null)).to.be.null;
    });

  });
});
