'use strict';

import chai from 'chai';
import {FieldsPrerequisite, FieldValuesPrerequisite, Prerequisites,
        SectionsPrerequisite, StepsPrerequisite} from '../../src/index.js';

const expect = chai.expect;

var prerequisites;

/** @test {Prerequisites} */
describe('A Prerequisites instance', function () {
  beforeEach(function () {
    prerequisites = new Prerequisites();
  });

  describe('with sections completed', function () {
    var p;
    beforeEach(function () {
      p = new SectionsPrerequisite();
      p.logical = 'and';
      prerequisites.sectionsCompleted = p;
    });

    /** @test {Prerequisites#sectionsCompleted} */
    it('should have the specified sections completed', () => {
      expect(prerequisites.sectionsCompleted).to.equal(p);
    });

    /** @test {Prerequisites#sectionsCompleted} */
    it('throws an exception when specified value is not a SectionsPrerequisite object', () => {
      expect(() => { prerequisites.sectionsCompleted = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the sections completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.sectionsCompleted).to.exist;
    });
  });

  describe('without sections completed', function () {
    beforeEach(function () {
      prerequisites.sectionsCompleted = null;
    });

    /** @test {Prerequisites#sectionsCompleted} */
    it('should be allowed', () => {
      expect(prerequisites.sectionsCompleted).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include sections completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.sectionsCompleted).to.not.exist;
    });
  });

  describe('with steps completed', function () {
    var p;
    beforeEach(function () {
      p = new StepsPrerequisite();
      p.logical = 'and';
      prerequisites.stepsCompleted = p;
    });

    /** @test {Prerequisites#stepsCompleted} */
    it('should have the specified steps completed', () => {
      expect(prerequisites.stepsCompleted).to.equal(p);
    });

    /** @test {Prerequisites#stepsCompleted} */
    it('throws an exception when specified value is not a StepsPrerequisite object', () => {
      expect(() => { prerequisites.stepsCompleted = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the steps completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.stepsCompleted).to.exist;
    });
  });

  describe('without steps completed', function () {
    beforeEach(function () {
      prerequisites.stepsCompleted = null;
    });

    /** @test {Prerequisites#stepsCompleted} */
    it('should be allowed', () => {
      expect(prerequisites.stepsCompleted).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include steps completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.stepsCompleted).to.not.exist;
    });
  });

  describe('with fields completed', function () {
    var p;
    beforeEach(function () {
      p = new FieldsPrerequisite();
      p.logical = 'and';
      prerequisites.fieldsCompleted = p;
    });

    /** @test {Prerequisites#fieldsCompleted} */
    it('should have the specified fields completed', () => {
      expect(prerequisites.fieldsCompleted).to.equal(p);
    });

    /** @test {Prerequisites#fieldsCompleted} */
    it('throws an exception when specified value is not a FieldsPrerequisite object', () => {
      expect(() => { prerequisites.fieldsCompleted = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the fields completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fieldsCompleted).to.exist;
    });
  });

  describe('without fields completed', function () {
    beforeEach(function () {
      prerequisites.fieldsCompleted = null;
    });

    /** @test {Prerequisites#fieldsCompleted} */
    it('should be allowed', () => {
      expect(prerequisites.fieldsCompleted).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include fields completed when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fieldsCompleted).to.not.exist;
    });
  });

  describe('with field values', function () {
    var p;
    beforeEach(function () {
      p = new FieldValuesPrerequisite();
      p.logical = 'and';
      prerequisites.fieldValues = p;
    });

    /** @test {Prerequisites#fieldValues} */
    it('should have the specified fieldValues ', () => {
      expect(prerequisites.fieldValues).to.equal(p);
    });

    /** @test {Prerequisites#fieldValues} */
    it('throws an exception when specified value is not a FieldValuesPrerequisite object', () => {
      expect(() => { prerequisites.fieldValues = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the fieldValues  when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fieldValues).to.exist;
    });
  });

  describe('without fieldValues ', function () {
    beforeEach(function () {
      prerequisites.fieldValues = null;
    });

    /** @test {Prerequisites#fieldValues} */
    it('should be allowed', () => {
      expect(prerequisites.fieldValues).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include fieldValues when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.fieldValues).to.not.exist;
    });
  });

  /** @test {Prerequisites#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    prerequisites.randomAttribute = 5;
    const output = prerequisites.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {Prerequisites.fromJSON} */
    it('should return a Prerequisites instance', () => {
      const jsonData = '{ "sectionsCompleted": { "logical": "and", "sections": ["section0"] } }';
      const prerequisites = Prerequisites.fromJSON(jsonData);
      expect(prerequisites).to.be.an.instanceof(Prerequisites);
    });

    /** @test {Prerequisites.fromJSON} */
    it('should include a SectionsPrerequisite instance', () => {
      const jsonData = '{ "sectionsCompleted": { "logical": "and", "sections": ["section0"] } }';
      const prerequisites = Prerequisites.fromJSON(jsonData);
      expect(prerequisites.sectionsCompleted).to.be.an.instanceof(SectionsPrerequisite);
    });

    /** @test {Prerequisites.fromJSON} */
    it('should include a StepsPrerequisite instance', () => {
      const jsonData = '{ "stepsCompleted": { "logical": "and", "steps": ["step0"] } }';
      const prerequisites = Prerequisites.fromJSON(jsonData);
      expect(prerequisites.stepsCompleted).to.be.an.instanceof(StepsPrerequisite);
    });

    /** @test {Prerequisites.fromJSON} */
    it('should include a FieldsPrerequisite instance', () => {
      const jsonData = '{ "fieldsCompleted": { "logical": "and", "fields": ["field0"] } }';
      const prerequisites = Prerequisites.fromJSON(jsonData);
      expect(prerequisites.fieldsCompleted).to.be.an.instanceof(FieldsPrerequisite);
    });

    /** @test {Prerequisites.fromJSON} */
    it('should include a FieldValuesPrerequisite instance', () => {
      const jsonData = '{ "fieldValues": { "logical": "and", "operands": [] } }';
      const prerequisites = Prerequisites.fromJSON(jsonData);
      expect(prerequisites.fieldValues).to.be.an.instanceof(FieldValuesPrerequisite);
    });
  });
});
