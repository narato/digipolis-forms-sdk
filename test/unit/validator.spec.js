import chai from 'chai';
import {Validator, ValidatorOptions} from '../../src/index.js';

const expect = chai.expect;

var validator;

/** @test {Validator} */
describe('Creating a Validator instance', function () {
  /** @test {Validator#name} */
  it('throws an exception without a name', () => {
    expect(() => { const validator = new Validator(null); }).to.throw(/Validator must have a name/);
  });

  /** @test {Validator#name} */
  it('throws an exception when specified name is not a string', () => {
    expect(() => { const validator = new Validator(5, 5); }).to.throw(TypeError);
  });
  /** @test {Validator#name} */
  it('throws an exception when specified type is not a string', () => {
    expect(() => { const validator = new Validator('test', 5); }).to.throw(TypeError);
  });
});

/** @test {Validator} */
describe('A Validator instance', function () {
  beforeEach(function () {
    validator = new Validator('name0', 'required');
  });

  describe('with a name', function () {
    /** @test {Validator#name} */
    it('should use name from constructor', () => {
      expect(validator.name).to.equal('name0');
    });

    /** @test {Validator#toJSON} */
    it('should include the name when exporting', () => {
      const output = validator.toJSON();
      expect(output.name).to.equal('name0');
    });
  });

  describe('with a type', function () {

    /** @test {Validator#type} */
    it('should use type from constructor', () => {
      expect(validator.type).to.equal('required');
    });

    /** @test {Validator#toJSON} */
    it('should include the type when exporting', () => {
      const output = validator.toJSON();
      expect(output.type).to.equal('required');
    });
  });

  describe('with an errorMessage', function () {
    beforeEach(function () {
      validator.errorMessage = 'errorMessage0';
    });

    /** @test {Validator#errorMessage} */
    it('should have the errorMessage value', () => {
      expect(validator.errorMessage).to.equal('errorMessage0');
    });

    /** @test {Validator#errorMessage} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { validator.errorMessage = 5; }).to.throw(TypeError);
    });

    /** @test {Validator#toJSON} */
    it('should include the errorMessage when exporting', () => {
      const output = validator.toJSON();
      expect(output.errorMessage).to.equal('errorMessage0');
    });
  });

  describe('without an errorMessage', function () {
    beforeEach(function () {
      validator.errorMessage = null;
    });

    /** @test {Validator#errorMessage} */
    it('should be allowed', () => {
      expect(validator.errorMessage).to.be.null;
    });

    /** @test {Validator#toJSON} */
    it('should not include a errorMessage when exporting', () => {
      const output = validator.toJSON();
      expect(output.errorMessage).to.not.exist;
    });
  });

  /** @test {Validator#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    validator.randomAttribute = 5;
    const output = validator.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {Validator.fromJSON} */
    it('should return a Validator instance', () => {
      const jsonData = '{ "name": "Validator name", "type": "required" }';
      const validator = Validator.fromJSON(jsonData);
      expect(validator).to.be.an.instanceof(Validator);
      expect(validator.name).to.equal('Validator name');
      expect(validator.type).to.equal('required');
    });

    /** @test {Validator.fromJSON} */
    it('should return null if null was given', () => {
      expect(Validator.fromJSON(null)).to.be.null;
    });
  });
});
