import chai from 'chai';
import {Field, Form, FormInfo, NavigationTexts, Section, Step, Validator, AdvancedValidator, ValidatorOptions} from '../../src/index.js';

const expect = chai.expect;

var form;

/** @test {Form} */
describe('A Form instance', function () {
  beforeEach(function () {
    form = new Form();
  });

  describe('setTitle', function () {
    /** @test {Form#setTitle} */
    it('should set the title in the info object', () => {
      const t = 'test-title';
      form.setTitle(t);
      expect(form.info.title).to.equal(t);
    });
  });

  describe('setBody', function () {
    /** @test {Form#setBody} */
    it('should set the body in the info object', () => {
      const b = 'an example body to test with';
      form.setBody(b);
      expect(form.info.body).to.equal(b);
    });
  });

  describe('with a name', function () {
    beforeEach(function () {
      let l = 'example name';
      form.name = l;
    });

    /** @test {Form#name} */
    it('should have the specified name', () => {
      expect(form.name).to.equal('example name');
    });

    /** @test {Form#name} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { form.name = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the name when exporting', () => {
      const output = form.toJSON();
      expect(output.name).to.equal('example name');
    });
  });

  describe('without a name', function () {
    beforeEach(function () {
      form.name = null;
    });

    /** @test {Form#name} */
    it('should be allowed', () => {
      expect(form.name).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a name when exporting', () => {
      const output = form.toJSON();
      expect(output.name).to.not.exist;
    });
  });

  describe('with a formId', function () {
    beforeEach(function () {
      let l = 'example formId';
      form.formId = l;
    });

    /** @test {Form#formId} */
    it('should have the specified formId', () => {
      expect(form.formId).to.equal('example formId');
    });

    /** @test {Form#formId} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { form.formId = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the formId when exporting', () => {
      const output = form.toJSON();
      expect(output.formId).to.equal('example formId');
    });
  });

  describe('without a formId', function () {
    beforeEach(function () {
      form.formId = null;
    });

    /** @test {Form#formId} */
    it('should be allowed', () => {
      expect(form.formId).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a formId when exporting', () => {
      const output = form.toJSON();
      expect(output.formId).to.not.exist;
    });
  });

  describe('with a rendererVersion', function () {
    beforeEach(function () {
      let l = 'example rendererVersion';
      form.rendererVersion = l;
    });

    /** @test {Form#rendererVersion} */
    it('should have the specified rendererVersion', () => {
      expect(form.rendererVersion).to.equal('example rendererVersion');
    });

    /** @test {Form#rendererVersion} */
    it('throws an exception when specified value is not a string', () => {
      expect(() => { form.rendererVersion = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the rendererVersion when exporting', () => {
      const output = form.toJSON();
      expect(output.rendererVersion).to.equal('example rendererVersion');
    });
  });

  describe('without a rendererVersion', function () {
    beforeEach(function () {
      form.rendererVersion = null;
    });

    /** @test {Form#rendererVersion} */
    it('should be allowed', () => {
      expect(form.rendererVersion).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a rendererVersion when exporting', () => {
      const output = form.toJSON();
      expect(output.rendererVersion).to.not.exist;
    });
  });

  describe('with info', function () {
    var l;
    beforeEach(function () {
      l = new FormInfo();
      form.info = l;
    });

    /** @test {Form#info} */
    it('should have the specified info', () => {
      expect(form.info).to.equal(l);
    });

    /** @test {Form#info} */
    it('throws an exception when specified value is not a FormInfo object', () => {
      expect(() => { form.info = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the info when exporting', () => {
      const output = form.toJSON();
      expect(output.info).to.exist;
    });
  });

  describe('without info', function () {
    beforeEach(function () {
      form.info = null;
    });

    /** @test {Form#info} */
    it('should be allowed', () => {
      expect(form.info).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a info when exporting', () => {
      const output = form.toJSON();
      expect(output.info).to.not.exist;
    });
  });

  describe('with navigation texts', function () {
    var n;
    beforeEach(function () {
      n = new NavigationTexts();
      form.navigationTexts = n;
    });

    /** @test {Form#navigationTexts} */
    it('should have the specified navigation texts', () => {
      expect(form.navigationTexts).to.equal(n);
    });

    /** @test {Form#navigationTexts} */
    it('throws an exception when specified value is not a NavigationTexts object', () => {
      expect(() => { form.navigationTexts = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the navigation texts when exporting', () => {
      const output = form.toJSON();
      expect(output.navigationTexts).to.exist;
    });
  });

  describe('without navigation texts', function () {
    beforeEach(function () {
      form.navigationTexts = null;
    });

    /** @test {Form#navigationTexts} */
    it('should be allowed', () => {
      expect(form.navigationTexts).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include navigation texts when exporting', () => {
      const output = form.toJSON();
      expect(output.navigationTexts).to.not.exist;
    });
  });

  describe('with a canSaveDraft flag', function () {
    beforeEach(function () {
      form.canSaveDraft = true;
    });

    /** @test {Form#canSaveDraft} */
    it('should have the specified canSaveDraft flag', () => {
      expect(form.canSaveDraft).to.be.true;
    });

    /** @test {Form#canSaveDraft} */
    it('throws an exception when specified value is not a boolean', () => {
      expect(() => { form.canSaveDraft = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the canSaveDraft flag when exporting', () => {
      const output = form.toJSON();
      expect(output.canSaveDraft).to.be.true;
    });
  });

  describe('without a canSaveDraft flag', function () {
    beforeEach(function () {
      form.canSaveDraft = null;
    });

    /** @test {Form#canSaveDraft} */
    it('should be allowed', () => {
      expect(form.canSaveDraft).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a canSaveDraft flag when exporting', () => {
      const output = form.toJSON();
      expect(output.canSaveDraft).to.not.exist;
    });
  });

  describe('with a saveOnNavigate flag', function () {
    beforeEach(function () {
      form.saveOnNavigate = true;
    });

    /** @test {Form#saveOnNavigate} */
    it('should have the specified saveOnNavigate flag', () => {
      expect(form.saveOnNavigate).to.be.true;
    });

    /** @test {Form#saveOnNavigate} */
    it('throws an exception when specified value is not a boolean', () => {
      expect(() => { form.saveOnNavigate = 5; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the saveOnNavigate flag when exporting', () => {
      const output = form.toJSON();
      expect(output.saveOnNavigate).to.be.true;
    });
  });

  describe('without a saveOnNavigate flag', function () {
    beforeEach(function () {
      form.saveOnNavigate = null;
    });

    /** @test {Form#saveOnNavigate} */
    it('should be allowed', () => {
      expect(form.saveOnNavigate).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include a saveOnNavigate flag when exporting', () => {
      const output = form.toJSON();
      expect(output.saveOnNavigate).to.not.exist;
    });
  });

  describe('with steps', function () {
    var steps;
    beforeEach(function () {
      steps = [new Step('step0'), new Step('step1')];
      form.steps = steps;
    });

    /** @test {Form#steps} */
    it('should have the specified steps', () => {
      expect(form.steps).to.equal(steps);
    });

    /** @test {Form#steps} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { form.steps = 5; }).to.throw(TypeError);
    });

    /** @test {Form#steps} */
    it('throws an exception when the specified value is not an Array of Steps', () => {
      expect(() => { form.steps = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the steps when exporting', () => {
      const output = form.toJSON();
      expect(output.navigationTexts).to.exist;
    });
  });

  describe('with an empty steps array', function () {
    beforeEach(function () {
      form.steps = [];
    });

    /** @test {Form#steps} */
    it('should be allowed', () => {
      expect(form.steps).to.be.empty;
    });

    /** @test {Form#toJSON} */
    it('should not include steps when exporting', () => {
      const output = form.toJSON();
      expect(output.steps).to.not.exist;
    });
  });

  describe('without steps', function () {
    beforeEach(function () {
      form.steps = null;
    });

    /** @test {Form#steps} */
    it('should be allowed', () => {
      expect(form.steps).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include steps when exporting', () => {
      const output = form.toJSON();
      expect(output.steps).to.not.exist;
    });
  });

  describe('with sections', function () {
    var sections;
    beforeEach(function () {
      sections = [new Section('section0'), new Section('section1')];
      form.sections = sections;
    });

    /** @test {Form#sections} */
    it('should have the specified sections', () => {
      expect(form.sections).to.equal(sections);
    });

    /** @test {Form#sections} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { form.sections = 5; }).to.throw(TypeError);
    });

    /** @test {Form#sections} */
    it('throws an exception when the specified value is not an Array of Sections', () => {
      expect(() => { form.sections = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the sections when exporting', () => {
      const output = form.toJSON();
      expect(output.sections).to.exist;
    });
  });

  describe('with an empty sections array', function () {
    beforeEach(function () {
      form.sections = [];
    });

    /** @test {Form#sections} */
    it('should be allowed', () => {
      expect(form.sections).to.be.empty;
    });

    /** @test {Form#toJSON} */
    it('should not include sections when exporting', () => {
      const output = form.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('without sections', function () {
    beforeEach(function () {
      form.sections = null;
    });

    /** @test {Form#sections} */
    it('should be allowed', () => {
      expect(form.sections).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include sections when exporting', () => {
      const output = form.toJSON();
      expect(output.sections).to.not.exist;
    });
  });

  describe('with fields', function () {
    var fields;
    beforeEach(function () {
      fields = [new Field('field0'), new Field('field1')];
      form.fields = fields;
    });

    /** @test {Form#fields} */
    it('should have the specified fields', () => {
      expect(form.fields).to.equal(fields);
    });

    /** @test {Form#fields} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { form.fields = 5; }).to.throw(TypeError);
    });

    /** @test {Form#fields} */
    it('throws an exception when the specified value is not an Array of Fields', () => {
      expect(() => { form.fields = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the fields when exporting', () => {
      const output = form.toJSON();
      expect(output.fields).to.exist;
    });
  });

  describe('with an empty fields array', function () {
    beforeEach(function () {
      form.fields = [];
    });

    /** @test {Form#fields} */
    it('should be allowed', () => {
      expect(form.fields).to.be.empty;
    });

    /** @test {Form#toJSON} */
    it('should not include fields when exporting', () => {
      const output = form.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('without fields', function () {
    beforeEach(function () {
      form.fields = null;
    });

    /** @test {Form#fields} */
    it('should be allowed', () => {
      expect(form.fields).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include fields when exporting', () => {
      const output = form.toJSON();
      expect(output.fields).to.not.exist;
    });
  });

  describe('when adding a step', function () {
    /** @test {Form#addStep} */
    it('should have added the step to the form', () => {
      let step = new Step('step0');
      form.addStep(step);
      let anotherStep = new Step('step1');
      form.addStep(anotherStep);

      expect(form.steps[0]).to.equal(step);
      expect(form.steps[1]).to.equal(anotherStep);
    });

    /** @test {Form#addStep} */
    it('should not allow a duplicate step', () => {
      let step = new Step('step0');
      form.addStep(step);
      let addDuplicateStep = () => form.addStep(step);

      expect(addDuplicateStep).to.throw(/Step with id/);
    });

    /** @test {Form#addStep} */
    it('throws an exception when the specified value is not a Step', () => {
      expect(() => { form.addStep(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a step', function () {
    /** @test {Form#getStep} */
    it('should return the step with the specified ID', () => {
      let step0 = new Step('step0');
      form.addStep(step0);
      form.addStep(new Step('step1'));
      form.addStep(new Step('step2'));

      expect(form.getStep('step0')).to.equal(step0);
    });

    /** @test {Form#getStep} */
    it('should return undefined if a step with the specified ID does not exist', () => {
      form.addStep(new Step('step0'));
      form.addStep(new Step('step1'));
      form.addStep(new Step('step2'));

      expect(form.getStep('step4')).to.be.undefined;
    });
  });

  describe('when updating a step', function () {
    /** @test {Form#updateStep} */
    it('should have updated the step of the form', () => {
      form.addStep(new Step('step0'));
      form.addStep(new Step('step1'));
      form.addStep(new Step('step2'));

      let updatedStep = new Step('step1');
      form.updateStep(updatedStep);

      expect(form.steps.length).to.equal(3);
      expect(form.steps[1]).to.equal(updatedStep);
    });

    /** @test {Form#updateStep} */
    it('throws an error if specified step does not exist', () => {
      let step = new Step('step0');
      let updateNonExistingStep = () => form.updateStep(step);

      expect(updateNonExistingStep).to.throw(Error);
    });

    /** @test {Form#updateStep} */
    it('throws an exception when the specified value is not a Step', () => {
      expect(() => { form.updateStep(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a step', function () {
    /** @test {Form#removeStep} */
    it('should have removed the step of the form', () => {
      form.addStep(new Step('step0'));
      let stepToRemove = new Step('step1');
      form.addStep(stepToRemove);
      form.addStep(new Step('step2'));
      expect(form.steps.length).to.equal(3);

      form.removeStep(stepToRemove);

      expect(form.steps.length).to.equal(2);
      expect(form.steps).not.to.include(stepToRemove);
    });

    /** @test {Form#removeStep} */
    it('throws an error if specified step does not exist', () => {
      let step = new Step('step0');
      let removeNonExistingStep = () => form.removeStep(step);

      expect(removeNonExistingStep).to.throw(Error);
    });

    /** @test {Form#removeStep} */
    it('throws an exception when the specified value is not a Step', () => {
      expect(() => { form.removeStep(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a step', function () {
    /** @test {Form#moveStepToIndex} */
    it('should have moved the step to the specified index', () => {
      let firstStep = new Step('step0');
      let secondStep = new Step('step1');
      let thirdStep = new Step('step2');

      form.addStep(firstStep);
      form.addStep(secondStep);
      form.addStep(thirdStep);
      expect(form.steps[0]).to.equal(firstStep);

      form.moveStepToIndex(thirdStep, 0);

      expect(form.steps[0]).to.equal(thirdStep);
    });

    /** @test {Form#moveStepToIndex} */
    it('throws an error if specified step does not exist', () => {
      let step = new Step('step0');
      let moveNonExistingStep = () => form.moveStepToIndex(step, 0);

      expect(moveNonExistingStep).to.throw(Error);
    });

    /** @test {Form#moveStepToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let step = new Step('step0');
      form.addStep(step);
      let moveOUBindex = () => form.moveStepToIndex(step, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveStepToIndex} */
    it('throws an error if no index was specified', () => {
      let step = new Step('step0');
      form.addStep(step);
      let moveOUBindex = () => form.moveStepToIndex(step);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveStepToIndex} */
    it('throws an exception when the specified step is not a Step', () => {
      expect(() => { form.moveStepToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Form#moveStepToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { form.moveStepToIndex(new Step('step0'), 'test'); }).to.throw(TypeError);
    });
  });

  describe('when adding a section', function () {
    /** @test {Form#addSection} */
    it('should have added the section to the form', () => {
      let section = new Section('section0');
      form.addSection(section);
      let anotherSection = new Section('section1');
      form.addSection(anotherSection);

      expect(form.sections[0]).to.equal(section);
      expect(form.sections[1]).to.equal(anotherSection);
    });

    /** @test {Form#addSection} */
    it('should not allow a duplicate section', () => {
      let section = new Section('section0');
      form.addSection(section);
      let addDuplicateSection = () => form.addSection(section);

      expect(addDuplicateSection).to.throw(/Section with id/);
    });

    /** @test {Form#addSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { form.addSection(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a section', function () {
    /** @test {Form#getSection} */
    it('should return the section with the specified ID', () => {
      let section0 = new Section('section0');
      form.addSection(section0);
      form.addSection(new Section('section1'));
      form.addSection(new Section('section2'));

      expect(form.getSection('section0')).to.equal(section0);
    });

    /** @test {Form#getSection} */
    it('should return undefined if a section with the specified ID does not exist', () => {
      form.addSection(new Section('section0'));
      form.addSection(new Section('section1'));
      form.addSection(new Section('section2'));

      expect(form.getSection('section4')).to.be.undefined;
    });
  });

  describe('when updating a section', function () {
    /** @test {Form#updateSection} */
    it('should have updated the section of the form', () => {
      form.addSection(new Section('section0'));
      form.addSection(new Section('section1'));
      form.addSection(new Section('section2'));

      let updatedSection = new Section('section1');
      form.updateSection(updatedSection);

      expect(form.sections.length).to.equal(3);
      expect(form.sections[1]).to.equal(updatedSection);
    });

    /** @test {Form#updateSection} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let updateNonExistingSection = () => form.updateSection(section);

      expect(updateNonExistingSection).to.throw(Error);
    });

    /** @test {Form#updateSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { form.updateSection(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a section', function () {
    /** @test {Form#removeSection} */
    it('should have removed the section of the form', () => {
      form.addSection(new Section('section0'));
      let sectionToRemove = new Section('section1');
      form.addSection(sectionToRemove);
      form.addSection(new Section('section2'));
      expect(form.sections.length).to.equal(3);

      form.removeSection(sectionToRemove);

      expect(form.sections.length).to.equal(2);
      expect(form.sections).not.to.include(sectionToRemove);
    });

    /** @test {Form#removeSection} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let removeNonExistingSection = () => form.removeSection(section);

      expect(removeNonExistingSection).to.throw(Error);
    });

    /** @test {Form#removeSection} */
    it('throws an exception when the specified value is not a Section', () => {
      expect(() => { form.removeSection(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a section', function () {
    /** @test {Form#moveSectionToIndex} */
    it('should have moved the section to the specified index', () => {
      let firstSection = new Section('section0');
      let secondSection = new Section('section1');
      let thirdSection = new Section('section2');

      form.addSection(firstSection);
      form.addSection(secondSection);
      form.addSection(thirdSection);
      expect(form.sections[0]).to.equal(firstSection);

      form.moveSectionToIndex(thirdSection, 0);

      expect(form.sections[0]).to.equal(thirdSection);
    });

    /** @test {Form#moveSectionToIndex} */
    it('throws an error if specified section does not exist', () => {
      let section = new Section('section0');
      let moveNonExistingSection = () => form.moveSectionToIndex(section, 0);

      expect(moveNonExistingSection).to.throw(Error);
    });

    /** @test {Form#moveSectionToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let section = new Section('section0');
      form.addSection(section);
      let moveOUBindex = () => form.moveSectionToIndex(section, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveSectionToIndex} */
    it('throws an error if no index was specified', () => {
      let section = new Section('section0');
      form.addSection(section);
      let moveOUBindex = () => form.moveSectionToIndex(section);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveSectionToIndex} */
    it('throws an exception when the specified section is not a Section', () => {
      expect(() => { form.moveSectionToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Form#moveSectionToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { form.moveSectionToIndex(new Section('section0'), 'test'); }).to.throw(TypeError);
    });
  });

  describe('when adding a field', function () {
    /** @test {Form#addField} */
    it('should have added the field to the form', () => {
      let field = new Field('field0');
      form.addField(field);
      let anotherField = new Field('field1');
      form.addField(anotherField);

      expect(form.fields[0]).to.equal(field);
      expect(form.fields[1]).to.equal(anotherField);
    });

    /** @test {Form#addField} */
    it('should not allow a duplicate field', () => {
      let field = new Field('field0');
      form.addField(field);
      let addDuplicateField = () => form.addField(field);

      expect(addDuplicateField).to.throw(/Field with name/);
    });

    /** @test {Form#addField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { form.addField(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a field', function () {
    /** @test {Form#getField} */
    it('should return the field with the specified name', () => {
      let field0 = new Field('field0');
      form.addField(field0);
      form.addField(new Field('field1'));
      form.addField(new Field('field2'));

      expect(form.getField('field0')).to.equal(field0);
    });

    /** @test {Form#getField} */
    it('should return undefined if a field with the specified name does not exist', () => {
      form.addField(new Field('field0'));
      form.addField(new Field('field1'));
      form.addField(new Field('field2'));

      expect(form.getField('field4')).to.be.undefined;
    });
  });

  describe('when updating a field', function () {
    /** @test {Form#updateField} */
    it('should have updated the field of the form', () => {
      form.addField(new Field('field0'));
      form.addField(new Field('field1'));
      form.addField(new Field('field2'));

      let updatedField = new Field('field1');
      form.updateField(updatedField);

      expect(form.fields.length).to.equal(3);
      expect(form.fields[1]).to.equal(updatedField);
    });

    /** @test {Form#updateField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let updateNonExistingField = () => form.updateField(field);

      expect(updateNonExistingField).to.throw(Error);
    });

    /** @test {Form#updateField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { form.updateField(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a field', function () {
    /** @test {Form#removeField} */
    it('should have removed the field of the form', () => {
      form.addField(new Field('field0'));
      let fieldToRemove = new Field('field1');
      form.addField(fieldToRemove);
      form.addField(new Field('field2'));
      expect(form.fields.length).to.equal(3);

      form.removeField(fieldToRemove);

      expect(form.fields.length).to.equal(2);
      expect(form.fields).not.to.include(fieldToRemove);
    });

    /** @test {Form#removeField} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let removeNonExistingField = () => form.removeField(field);

      expect(removeNonExistingField).to.throw(Error);
    });

    /** @test {Form#removeField} */
    it('throws an exception when the specified value is not a Field', () => {
      expect(() => { form.removeField(5); }).to.throw(TypeError);
    });
  });

  describe('when moving a field', function () {
    /** @test {Form#moveFieldToIndex} */
    it('should have moved the field to the specified index', () => {
      let firstField = new Field('field0');
      let secondField = new Field('field1');
      let thirdField = new Field('field2');

      form.addField(firstField);
      form.addField(secondField);
      form.addField(thirdField);
      expect(form.fields[0]).to.equal(firstField);

      form.moveFieldToIndex(thirdField, 0);

      expect(form.fields[0]).to.equal(thirdField);
    });

    /** @test {Form#moveFieldToIndex} */
    it('throws an error if specified field does not exist', () => {
      let field = new Field('field0');
      let moveNonExistingField = () => form.moveFieldToIndex(field, 0);

      expect(moveNonExistingField).to.throw(Error);
    });

    /** @test {Form#moveFieldToIndex} */
    it('throws an error if specified index falls outside the boundaries', () => {
      let field = new Field('field0');
      form.addField(field);
      let moveOUBindex = () => form.moveFieldToIndex(field, -1);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveFieldToIndex} */
    it('throws an error if no index was specified', () => {
      let field = new Field('field0');
      form.addField(field);
      let moveOUBindex = () => form.moveFieldToIndex(field);

      expect(moveOUBindex).to.throw(Error);
    });

    /** @test {Form#moveFieldToIndex} */
    it('throws an exception when the specified field is not a Field', () => {
      expect(() => { form.moveFieldToIndex(5, -1); }).to.throw(TypeError);
    });

    /** @test {Form#moveFieldToIndex} */
    it('throws an exception when the specified index is not a Number', () => {
      expect(() => { form.moveFieldToIndex(new Field('field0'), 'test'); }).to.throw(TypeError);
    });
  });

  describe('with validators', function () {
    var validators;
    beforeEach(function () {
      validators = [new Validator('validator0', 'required'), new AdvancedValidator('validator1', 'required', new ValidatorOptions())];
      form.validators = validators;
    });

    /** @test {Form#validators} */
    it('should have the specified fieds', () => {
      expect(form.validators).to.equal(validators);
    });

    /** @test {Form#validators} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { form.validators = 5; }).to.throw(TypeError);
    });

    /** @test {Form#validators} */
    it('throws an exception when the specified value is not an Array of validators', () => {
      expect(() => { form.validators = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Form#toJSON} */
    it('should include the validators when exporting', () => {
      const output = form.toJSON();
      expect(output.validators).to.exist;
    });
  });

  describe('with an empty validators array', function () {
    beforeEach(function () {
      form.validators = [];
    });

    /** @test {Form#validators} */
    it('should be allowed', () => {
      expect(form.validators).to.be.empty;
    });

    /** @test {Form#toJSON} */
    it('should not include validators when exporting', () => {
      const output = form.toJSON();
      expect(output.validators).to.not.exist;
    });
  });

  describe('without validators', function () {
    beforeEach(function () {
      form.validators = null;
    });

    /** @test {Form#validators} */
    it('should be allowed', () => {
      expect(form.validators).to.be.null;
    });

    /** @test {Form#toJSON} */
    it('should not include validators when exporting', () => {
      const output = form.toJSON();
      expect(output.validators).to.not.exist;
    });
  });

  describe('when adding a Validator', function () {
    /** @test {Form#addValidator} */
    it('should have added the validator to the section', () => {
      let validator = new Validator('validator0', 'required');
      form.addValidator(validator);
      let anotherValidator = new Validator('validator1', 'required');
      form.addValidator(anotherValidator);

      expect(form.validators[0]).to.equal(validator);
      expect(form.validators[1]).to.equal(anotherValidator);
    });

    /** @test {Form#addValidator} */
    it('should not allow a duplicate validator', () => {
      let validator = new Validator('validator0', 'required');
      form.addValidator(validator);

      expect(() => form.addValidator(validator)).to.throw(/Validator with name/);
    });

    it('should allow both validators and advancedValidators', () => {
      let validator = new Validator('validator0', 'required');
      let advancedValidator = new AdvancedValidator('validator1', 'regexp', new ValidatorOptions());
      form.addValidator(validator);
      expect(() => form.addValidator(advancedValidator)).to.not.throw();
    });

    /** @test {Form#addValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { form.addValidator(5); }).to.throw(TypeError);
    });

  });

  describe('when retrieving a validator', function () {
    /** @test {Form#getValidator} */
    it('should return the validator with the specified name', () => {
      let validator0 = new Validator('validator0', 'required');
      form.addValidator(validator0);
      form.addValidator(new Validator('validator1', 'required'));
      form.addValidator(new Validator('validator2', 'required'));

      expect(form.getValidator('validator0')).to.equal(validator0);
    });

    /** @test {Form#getValidator} */
    it('should return undefined if a validator with the specified name does not exist', () => {
      form.addValidator(new Validator('validator0', 'required'));
      form.addValidator(new Validator('validator1', 'required'));
      form.addValidator(new Validator('validator2', 'required'));

      expect(form.getValidator('validator4')).to.be.undefined;
    });
  });

  describe('when updating a validator', function () {
    /** @test {Form#updateValidator} */
    it('should have updated the validator of the section', () => {
      form.addValidator(new Validator('validator0', 'required'));
      form.addValidator(new Validator('validator1', 'required'));
      form.addValidator(new Validator('validator2', 'required'));

      let updatedValidator = new Validator('validator1', 'required');
      form.updateValidator(updatedValidator);

      expect(form.validators.length).to.equal(3);
      expect(form.validators[1]).to.equal(updatedValidator);
    });

    /** @test {Form#updateValidator} */
    it('throws an error if specified validator does not exist', () => {
      let validator = new Validator('validator0', 'required');
      let updateNonExistingValidator = () => form.updateValidator(validator);

      expect(updateNonExistingValidator).to.throw(Error);
    });

    /** @test {Form#updateValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { form.updateValidator(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a validator', function () {
    /** @test {Form#removeValidator} */
    it('should have removed the validator of the section', () => {
      form.addValidator(new Validator('validator0', 'required'));
      let validatorToRemove = new Validator('validator1', 'required');
      form.addValidator(validatorToRemove);
      form.addValidator(new Validator('validator2', 'required'));
      expect(form.validators.length).to.equal(3);

      form.removeValidator(validatorToRemove);

      expect(form.validators.length).to.equal(2);
      expect(form.validators).not.to.include(validatorToRemove);
    });

    /** @test {Form#removeValidator} */
    it('throws an error if specified validator does not exist', () => {
      let validator = new Validator('validator0', 'required');
      let removeNonExistingValidator = () => form.removeValidator(validator);

      expect(removeNonExistingValidator).to.throw(Error);
    });

    /** @test {Form#removeValidator} */
    it('throws an exception when the specified value is not a Validator', () => {
      expect(() => { form.removeValidator(5); }).to.throw(TypeError);
    });
  });

  describe('created from a JSON object', function () {
    /** @test {Form.fromJSON} */
    it('should return a Form instance', () => {
      const obj = {
        info: { title: 'a test title' }
      };
      const form = Form.fromJSON(obj);
      expect(form).to.be.an.instanceof(Form);
    });

    /** @test {Form.fromJSON} */
    it('should include steps as Step objects', () => {
      const obj = {
        steps: [ { id: 'step0', title: 'test step' } ]
      };
      const form = Form.fromJSON(obj);
      expect(form.steps[0]).to.be.an.instanceof(Step);
    });

    /** @test {Form.fromJSON} */
    it('should include sections as Section objects', () => {
      const obj = {
        sections: [ { id: 'section0', title: 'test section' } ]
      };
      const form = Form.fromJSON(obj);
      expect(form.sections[0]).to.be.an.instanceof(Section);
    });

    /** @test {Form.fromJSON} */
    it('should include fields as Field objects', () => {
      const obj = {
        fields: [ { name: 'field0' } ]
      };
      const form = Form.fromJSON(obj);
      expect(form.fields[0]).to.be.an.instanceof(Field);
    });

    /** @test {Form.fromJSON} */
    it('should include validators as Validator objects', () => {
      const obj = {
        validators: [ { name: 'validator0', type: 'required' } ]
      };
      const form = Form.fromJSON(obj);
      expect(form.validators[0]).to.be.an.instanceof(Validator);
    });

    /** @test {Form.fromJSON} */
    it('should include navigation texts as NavigationTexts object', () => {
      const obj = {
        navigationTexts: [ { next: 'next test label' } ]
      };
      const form = Form.fromJSON(obj);
      expect(form.navigationTexts).to.be.an.instanceof(NavigationTexts);
    });

    /** @test {Form.fromJSON} */
    it('should work if passed a json string', () => {
      const obj = '{ "navigationTexts": [ { "next": "next test label" } ]}';
      const form = Form.fromJSON(obj);
      expect(form.navigationTexts).to.be.an.instanceof(NavigationTexts);
    });

    /** @test {Form.fromJSON} */
    it('should return an empty Form object if null was given', () => {
      const form = Form.fromJSON(null);
      expect(form).to.be.an.instanceof(Form);
    });


  });

});
