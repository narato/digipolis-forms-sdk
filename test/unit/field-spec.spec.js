'use strict';

import chai from 'chai';
import {FieldSpec, FieldSpecAttributes, FieldSpecOptions} from '../../src/index.js';

const expect = chai.expect;

var spec;
var attributes;
var options;

/** @test {FieldSpec} */
describe('A FieldSpec instance', function () {
  beforeEach(function () {
    spec = new FieldSpec();
  });

  describe('with attributes', function () {
    beforeEach(function () {
      attributes = new FieldSpecAttributes();
      spec.attributes = attributes;
    });

    /** @test {FieldSpec#attributes} */
    it('should have the specified attributes', () => {
      expect(spec.attributes).to.equal(attributes);
    });

    /** @test {FieldSpec#attributes} */
    it('throws an exception when the specified value is not a FieldSpecAttributes', () => {
      expect(() => { spec.attributes = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpec#toJSON} */
    it('should include the attributes when exporting', () => {
      const output = spec.toJSON();
      expect(output.attributes).to.exist;
    });
  });

  describe('without attributes', function () {
    beforeEach(function () {
      spec.attributes = null;
    });

    /** @test {FieldSpec#attributes} */
    it('should be allowed', () => {
      expect(spec.attributes).to.be.null;
    });

    /** @test {FieldSpec#toJSON} */
    it('should not include attributes when exporting', () => {
      const output = spec.toJSON();
      expect(output.attributes).to.not.exist;
    });
  });

  describe('with options', function () {
    beforeEach(function () {
      options = new FieldSpecOptions();
      spec.options = options;
    });

    /** @test {FieldSpec#options} */
    it('should have the specified options', () => {
      expect(spec.options).to.equal(options);
    });

    /** @test {FieldSpec#options} */
    it('throws an exception when the specified value is not a FieldSpecOptions', () => {
      expect(() => { spec.options = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpec#toJSON} */
    it('should include the options when exporting', () => {
      const output = spec.toJSON();
      expect(output.options).to.exist;
    });
  });

  describe('without options', function () {
    beforeEach(function () {
      spec.options = null;
    });

    /** @test {FieldSpec#options} */
    it('should be allowed', () => {
      expect(spec.options).to.be.null;
    });

    /** @test {FieldSpec#toJSON} */
    it('should not include options when exporting', () => {
      const output = spec.toJSON();
      expect(output.options).to.not.exist;
    });
  });

  describe('when directly setting the field type attribute', function () {
    beforeEach(function () {
      const t = 'textbox';
      spec.setTypeAttribute(t);
    });

    /** @test {FieldSpec#setTypeAttribute} */
    it('should have the specified attribute type', () => {
      expect(spec.attributes.type).to.equal('textbox');
    });
  });

  describe('when directly setting the field value attribute', function () {
    beforeEach(function () {
      const v = 'a test value';
      spec.setValueAttribute(v);
    });

    /** @test {FieldSpec#setValueAttribute} */
    it('should have the specified attribute value', () => {
      expect(spec.attributes.value).to.equal('a test value');
    });
  });

  /** @test {FieldSpec#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    spec.randomAttribute = 5;
    const output = spec.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON object', function () {
    /** @test {FieldSpec.fromJSON} */
    it('should return a FieldSpec instance', () => {
      const jsonData = {
        attributes: { type: 'textbox', value: 'a test value' },
        options: { layout: { fieldLayout: 'full' } }
      };
      const spec = FieldSpec.fromJSON(jsonData);
      expect(spec).to.be.an.instanceof(FieldSpec);
    });

    /** @test {Form.fromJSON} */
    it('should include attributes as FieldSpecAttributes object', () => {
      const obj = {
        attributes: { type: 'textbox', value: 'a test value' },
      };
      const spec = FieldSpec.fromJSON(obj);
      expect(spec.attributes).to.be.an.instanceof(FieldSpecAttributes);
    });

    /** @test {Form.fromJSON} */
    it('should include options as FieldSpecOptions object', () => {
      const obj = {
        options: { layout: { fieldLayout: 'full' } }
      };
      const spec = FieldSpec.fromJSON(obj);
      expect(spec.options).to.be.an.instanceof(FieldSpecOptions);
    });

    /** @test {Form.fromJSON} */
    it('should accept a json string', () => {
      const obj = '{ "options": { "layout": { "fieldLayout": "full" } } }';
      const spec = FieldSpec.fromJSON(obj);
      expect(spec.options).to.be.an.instanceof(FieldSpecOptions);
    });
  });
});
