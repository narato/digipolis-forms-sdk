import chai from 'chai';
import {AdvancedValidator, ValidatorOptions} from '../../src/index.js';

const expect = chai.expect;

var validator;

/** @test {AdvancedValidator} */
describe('Creating a AdvancedValidator instance', function () {
  /** @test {AdvancedValidator#options} */
  it('throws an exception without an options object', () => {
    expect(() => { const validator = new AdvancedValidator('test', 'test2'); }).to.throw(/AdvancedValidator must have an options object/);
  });

  /** @test {AdvancedValidator#name} */
  it('throws an exception when specified value is not a ValidatorOptions object', () => {
    expect(() => { const validator = new AdvancedValidator('test', 'test2', 5); }).to.throw(TypeError);
  });
});

/** @test {AdvancedValidator} */
describe('A AdvancedValidator instance', function () {
  var options;
  beforeEach(function () {
    options = new ValidatorOptions();
    validator = new AdvancedValidator('name0', 'required', options);
  });

  describe('with an options object', function () {

    /** @test {AdvancedValidator#options} */
    it('should have the specified options', () => {
      expect(validator.options).to.equal(options);
    });

    /** @test {AdvancedValidator#options} */
    it('should be able to overwrite options', () => {
      let newOptions = new ValidatorOptions();
      validator.options = newOptions;
      expect(validator.options).to.equal(newOptions);
      expect(validator.options).to.not.equal(options);
    });

    /** @test {AdvancedValidator#options} */
    it('throws an exception when the specified value is not a ValidatorOptions object', () => {
      expect(() => { validator.options = 5; }).to.throw(TypeError);
    });

    it('should not be able to be set back to null', () => {
      expect(() => { validator.options = null; }).to.throw(Error);
    })

    /** @test {AdvancedValidator#options} */
    it('should include the options when exporting', () => {
      const output = validator.toJSON();
      expect(output.options).to.exist;
    });
  });

  /** @test {AdvancedValidator#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    validator.randomAttribute = 5;
    const output = validator.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {AdvancedValidator.fromJSON} */
    it('should return a AdvancedValidator instance', () => {
      const jsonData = '{ "name": "AdvancedValidator name", "type": "regexp", "options": { "pattern": "/^test$/" } }';
      const validator = AdvancedValidator.fromJSON(jsonData);
      expect(validator).to.be.an.instanceof(AdvancedValidator);
      expect(validator.options).to.be.an.instanceof(ValidatorOptions);
      expect(validator.options.pattern).to.equal('/^test$/');
    });

    /** @test {AdvancedValidator.fromJSON} */
    it('should return null if null was given', () => {
      expect(AdvancedValidator.fromJSON(null)).to.be.null;
    });
  });
});
