import chai from 'chai';
import {TypeCheck} from '../../src/index.js';

const expect = chai.expect;

/** @test {TypeCheck} */
describe('TypeCheck', function () {
  describe('isString', function () {
    /** @test {TypeCheck.isString} */
    it('should return true for a string literal', () => {
      expect(TypeCheck.isString('test')).to.equal(true);
    });

    /** @test {TypeCheck.isString} */
    it('should return true for a String object', () => {
      expect(TypeCheck.isString(new String('test'))).to.equal(true);
    });

    /** @test {TypeCheck.isString} */
    it('should return false for a number literal', () => {
      expect(TypeCheck.isString(1)).to.equal(false);
    });

    /** @test {TypeCheck.isString} */
    it('should return false for an Object instance', () => {
      expect(TypeCheck.isString(new Object())).to.equal(false);
    });

    /** @test {TypeCheck.isString} */
    it('should return false for an object literal', () => {
      expect(TypeCheck.isString({ test: 'ok' })).to.equal(false);
    });
  });

  describe('isBoolean', function () {
    /** @test {TypeCheck.isBoolean} */
    it('should return true for a boolean literal', () => {
      expect(TypeCheck.isBoolean(false)).to.equal(true);
    });

    /** @test {TypeCheck.isBoolean} */
    it('should return true for a Boolean object', () => {
      expect(TypeCheck.isBoolean(new Boolean(false))).to.equal(true);
    });

    /** @test {TypeCheck.isBoolean} */
    it('should return false for a number literal', () => {
      expect(TypeCheck.isBoolean(5)).to.equal(false);
    });

    /** @test {TypeCheck.isBoolean} */
    it('should return false for an Object instance', () => {
      expect(TypeCheck.isBoolean(new Object())).to.equal(false);
    });

    /** @test {TypeCheck.isBoolean} */
    it('should return false for an object literal', () => {
      expect(TypeCheck.isBoolean({ test: 'ok'})).to.equal(false);
    });
  });

  describe('isNumber', function () {
    /** @test {TypeCheck.isNumber} */
    it('should return true for a number literal', () => {
      expect(TypeCheck.isNumber(5)).to.equal(true);
    });

    /** @test {TypeCheck.isNumber} */
    it('should return true for a Number object', () => {
      expect(TypeCheck.isNumber(new Number(5))).to.equal(true);
    });

    /** @test {TypeCheck.isNumber} */
    it('should return false for a string literal', () => {
      expect(TypeCheck.isNumber('test')).to.equal(false);
    });

    /** @test {TypeCheck.isNumber} */
    it('should return false for an Object instance', () => {
      expect(TypeCheck.isNumber(new Object())).to.equal(false);
    });

    /** @test {TypeCheck.isNumber} */
    it('should return false for an object literal', () => {
      expect(TypeCheck.isNumber({ test: 'ok' })).to.equal(false);
    });
  });

  describe('isArray', function () {
    /** @test {TypeCheck.isArray} */
    it('should return true for a Array literal', () => {
      expect(TypeCheck.isArray([1, 2, 3])).to.equal(true);
    });

    /** @test {TypeCheck.isArray} */
    it('should return true for a Array object', () => {
      expect(TypeCheck.isArray(new Array())).to.equal(true);
    });

    /** @test {TypeCheck.isArray} */
    it('should return false for a number literal', () => {
      expect(TypeCheck.isArray(5)).to.equal(false);
    });

    /** @test {TypeCheck.isArray} */
    it('should return false for an Object instance', () => {
      expect(TypeCheck.isArray(new Object())).to.equal(false);
    });

    /** @test {TypeCheck.isArray} */
    it('should return false for an object literal', () => {
      expect(TypeCheck.isArray({ test: 'ok'})).to.equal(false);
    });
  });

  describe('isArrayOf', function () {
    describe('validating for an array of Strings', function () {
      /** @test {TypeCheck.isArrayOf} */
      it('should return true for an Array of Strings', () => {
        expect(TypeCheck.isArrayOf(['foo', 'bar', 'baz'],
                                   (i) => { return TypeCheck.isString(i); })).to.equal(true);
      });

      /** @test {TypeCheck.isArrayOf} */
      it('should return false for an Array of numbers', () => {
        expect(TypeCheck.isArrayOf([1, 2, 3],
                                   (i) => { return TypeCheck.isString(i); })).to.equal(false);
      });
    });
  });
});
