'use strict';

import chai from 'chai';
import {FieldSpecOptionsLayout} from '../../src/index.js';

const expect = chai.expect;

var layout;

/** @test {FieldSpecOptionsLayout} */
describe('A FieldSpecOptionsLayout instance', function () {
  beforeEach(function () {
    layout = new FieldSpecOptionsLayout();
  });

  describe('with a layout', function () {
    beforeEach(function () {
      const l = 'full';
      layout.fieldLayout = l;
    });

    /** @test {FieldSpecOptionsLayout#fieldLayout} */
    it('should have the specified layout', () => {
      expect(layout.fieldLayout).to.equal('full');
    });

    /** @test {FieldSpecOptionsLayout#fieldLayout} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { layout.fieldLayout = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecOptionsLayout#toJSON} */
    it('should include the field layout when exporting', () => {
      const output = layout.toJSON();
      expect(output.fieldLayout).to.equal('full');
    });
  });

  describe('without a layout', function () {
    beforeEach(function () {
      layout.fieldLayout = null;
    });

    /** @test {FieldSpecOptionsLayout#fieldLayout} */
    it('should be allowed', () => {
      expect(layout.fieldLayout).to.be.null;
    });

    /** @test {FieldSpecOptionsLayout#toJSON} */
    it('should not include a layout when exporting', () => {
      const output = layout.toJSON();
      expect(output.fieldLayout).to.not.exist;
    });
  });

  describe('with a field layout class', function () {
    beforeEach(function () {
      const c = 'my-test-class';
      layout.fieldClass = c;
    });

    /** @test {FieldSpecOptionsLayout#fieldClass} */
    it('should have the specified fieldClass', () => {
      expect(layout.fieldClass).to.equal('my-test-class');
    });

    /** @test {FieldSpecOptionsLayout#fieldClass} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { layout.fieldClass = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecOptionsLayout#toJSON} */
    it('should include the fieldClass when exporting', () => {
      const output = layout.toJSON();
      expect(output.fieldClass).to.equal('my-test-class');
    });
  });

  describe('without a fieldClass', function () {
    beforeEach(function () {
      layout.fieldClass = null;
    });

    /** @test {FieldSpecOptionsLayout#fieldClass} */
    it('should be allowed', () => {
      expect(layout.fieldClass).to.be.null;
    });

    /** @test {FieldSpecOptionsLayout#toJSON} */
    it('should not include a fieldClass when exporting', () => {
      const output = layout.toJSON();
      expect(output.fieldClass).to.not.exist;
    });
  });

  /** @test {FieldSpecOptionsLayout#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    layout.randomAttribute = 5;
    const output = layout.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldSpecOptionsLayout.fromJSON} */
    it('should return a FieldSpecOptionsLayout instance', () => {
      const jsonData = '{ "fieldLayout": "full" }';
      const layout = FieldSpecOptionsLayout.fromJSON(jsonData);
      expect(layout).to.be.an.instanceof(FieldSpecOptionsLayout);
      expect(layout.fieldLayout).to.equal('full');
    });
  });
});
