'use strict';

import chai from 'chai';
import {FieldSpecOptions, FieldSpecOptionsLayout, FieldSpecOptionsValue} from '../../src/index.js';

const expect = chai.expect;

var options;
var layout;

/** @test {FieldSpecOptions} */
describe('A FieldSpecOptions instance', function () {
  beforeEach(function () {
    options = new FieldSpecOptions();
  });

  describe('with a layout', function () {
    beforeEach(function () {
      layout = new FieldSpecOptionsLayout();
      options.layout = layout;
    });

    /** @test {FieldSpecOptions#layout} */
    it('should have the specified layout', () => {
      expect(options.layout).to.equal(layout);
    });

    /** @test {FieldSpecOptions#layout} */
    it('throws an exception when the specified value is not a FieldSpecOptionsLayout', () => {
      expect(() => { options.layout = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecOptions#toJSON} */
    it('should include the layout when exporting', () => {
      const output = options.toJSON();
      expect(output.layout).to.exist;
    });
  });

  describe('without a layout', function () {
    beforeEach(function () {
      options.layout = null;
    });

    /** @test {FieldSpecOptions#layout} */
    it('should be allowed', () => {
      expect(options.layout).to.be.null;
    });

    /** @test {FieldSpecOptions#toJSON} */
    it('should not include a layout when exporting', () => {
      const output = options.toJSON();
      expect(output.layout).to.not.exist;
    });
  });

  describe('when directly setting the field layout', function () {
    beforeEach(function () {
      const l = 'full';
      options.setFieldLayout(l);
    });

    /** @test {FieldSpecOptions#setFieldLayout} */
    it('should have the specified field layout', () => {
      expect(options.layout.fieldLayout).to.equal('full');
    });
  });

  describe('when directly setting the field class', function () {
    beforeEach(function () {
      const l = 'my-test-class';
      options.setFieldClass(l);
    });

    /** @test {FieldSpecOptions#setFieldClass} */
    it('should have the specified field class', () => {
      expect(options.layout.fieldClass).to.equal('my-test-class');
    });
  });

  describe('when adding a value option', function () {
    /** @test {FieldSpecOptions#addValueOption} */
    it('should have added item in valueOptions', () => {
      let val1 = new FieldSpecOptionsValue();
      val1.key = 'key1';
      val1.value = 'value1';
      options.addValueOption(val1);
      let val2 = new FieldSpecOptionsValue();
      options.addValueOption(val2);

      expect(options.valueOptions[0]).to.equal(val1);
    });

    /** @test {FieldSpecOptions#addValueOption} */
    it('should not allow a duplicate value option instance', () => {
      let val = new FieldSpecOptionsValue();
      options.addValueOption(val);
      let addDuplicate = () => options.addValueOption(val);

      expect(addDuplicate).to.throw(Error);
    });

    /** @test {FieldSpecOptions#addValueOption} */
    it('should not allow wrong types', () => {
      let addWrongType = () => options.addValueOption({});
      expect(addWrongType).to.throw(TypeError);
    });
  });

  describe('when getting a value option', function () {
    var val;

    beforeEach(function () {
      val = new FieldSpecOptionsValue();
      val.key = 'key1';
      val.value = 'value1';
      options.addValueOption(val);
    });

    /** @test {FieldSpecOptions#getValueOption} */
    it('should return the value options with specified key', () => {
      expect(options.getValueOption('key1')).to.equal(val);
    });

    /** @test {FieldSpecOptions#getValueOption} */
    it('should return undefined when value option does not exist', () => {
      expect(options.getValueOption('key2')).to.be.undefined;
    });
  });

  describe('when removing a value option', function () {
    beforeEach(function () {
      let val1 = new FieldSpecOptionsValue();
      val1.key = 'key1';
      val1.value = 'value1';
      let val2 = new FieldSpecOptionsValue();
      val2.key = 'key2';
      val2.value = 'value2';
      let val3 = new FieldSpecOptionsValue();
      val3.key = 'key3';
      val3.value = 'value3';
      options.addValueOption(val1);
      options.addValueOption(val2);
      options.addValueOption(val3);
    });

    /** @test {FieldSpecOptions#removeValueOption} */
    it('should remove the specified value option', () => {
      options.removeValueOption('key2');
      expect(options.valueOptions.length).to.equal(2);
    });

    /** @test {FieldSpecOptions#removeValueOption} */
    it('should throw an error if no value option exists with the specified key', () => {
      let removeNonExisting = () => options.removeValueOption('key4');
      expect(removeNonExisting).to.throw(Error);
    });
  });

  /** @test {FieldSpecOptions#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    options.randomAttribute = 5;
    const output = options.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldSpecOptions.fromJSON} */
    it('should return a FieldSpecOptions instance', () => {
      const jsonData = '{ "layout": { "fieldLayout": "full" }, "valueOptions": [ { "key": "key1", "value": "value1" } ] }';
      const options = FieldSpecOptions.fromJSON(jsonData);
      expect(options).to.be.an.instanceof(FieldSpecOptions);
    });

    /** @test {FieldSpecOptions.fromJSON} */
    it('should include layout as a FieldSpecOptionsLayout instance', () => {
      const jsonData = '{ "layout": { "fieldLayout": "full" }, "valueOptions": [ { "key": "key1", "value": "value1" } ] }';
      const options = FieldSpecOptions.fromJSON(jsonData);
      expect(options.layout).to.be.an.instanceof(FieldSpecOptionsLayout);
      expect(options.valueOptions[0]).to.be.an.instanceof(FieldSpecOptionsValue);
    });
  });
});
