'use strict';

import chai from 'chai';
import {FieldSpecAttributes} from '../../src/index.js';

const expect = chai.expect;

var attributes;

/** @test {FieldSpecAttributes} */
describe('A FieldSpecAttributes instance', function () {
  beforeEach(function () {
    attributes = new FieldSpecAttributes();
  });

  describe('with a type', function () {
    beforeEach(function () {
      const l = 'textbox';
      attributes.type = l;
    });

    /** @test {FieldSpecAttributes#type} */
    it('should have the specified type', () => {
      expect(attributes.type).to.equal('textbox');
    });

    /** @test {FieldSpecAttributes#type} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { attributes.type = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecAttributes#toJSON} */
    it('should include the type when exporting', () => {
      const output = attributes.toJSON();
      expect(output.type).to.equal('textbox');
    });
  });

  describe('without a type', function () {
    beforeEach(function () {
      attributes.type = null;
    });

    /** @test {FieldSpecAttributes#type} */
    it('should be allowed', () => {
      expect(attributes.type).to.be.null;
    });

    /** @test {FieldSpecAttributes#toJSON} */
    it('should not include a type when exporting', () => {
      const output = attributes.toJSON();
      expect(output.type).to.not.exist;
    });
  });

  describe('with a value', function () {
    beforeEach(function () {
      const l = 'example value';
      attributes.value = l;
    });

    /** @test {FieldSpecAttributes#value} */
    it('should have the specified value', () => {
      expect(attributes.value).to.equal('example value');
    });

    /** @test {FieldSpecAttributes#value} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { attributes.value = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecAttributes#toJSON} */
    it('should include the value when exporting', () => {
      const output = attributes.toJSON();
      expect(output.value).to.equal('example value');
    });
  });

  describe('without a value', function () {
    beforeEach(function () {
      attributes.value = null;
    });

    /** @test {FieldSpecAttributes#value} */
    it('should be allowed', () => {
      expect(attributes.value).to.be.null;
    });

    /** @test {FieldSpecAttributes#toJSON} */
    it('should not include a value when exporting', () => {
      const output = attributes.toJSON();
      expect(output.value).to.not.exist;
    });
  });

  /** @test {FieldSpecAttributes#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    attributes.randomAttribute = 5;
    const output = attributes.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldSpecAttributes.fromJSON} */
    it('should return a FieldSpecAttributes instance', () => {
      const jsonData = '{ "type": "textbox" }';
      const attributes = FieldSpecAttributes.fromJSON(jsonData);
      expect(attributes).to.be.an.instanceof(FieldSpecAttributes);
      expect(attributes.type).to.equal('textbox');
    });
  });

});
