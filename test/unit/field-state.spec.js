import chai from 'chai';
import {FieldState} from '../../src/index.js';

const expect = chai.expect;

var state;

/** @test {FieldState} */
describe('A FieldState instance', function () {
  beforeEach(function () {
    state = new FieldState();
  });

  describe('with an editable flag', function () {
    beforeEach(function () {
      state.editable = true;
    });

    /** @test {FieldState#editable} */
    it('should have the specified editable value', () => {
      expect(state.editable).to.be.true;
    });

    /** @test {FieldState#editable} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editable = 5; }).to.throw(TypeError);
    });

    /** @test {FieldState#toJSON} */
    it('should include the editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.be.true;
    });
  });

  describe('without a editable flag', function () {
    beforeEach(function () {
      state.editable = null;
    });

    /** @test {FieldState#editable} */
    it('should be allowed', () => {
      expect(state.editable).to.be.null;
    });

    /** @test {FieldState#toJSON} */
    it('should not include a editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.not.exist;
    });
  });

  describe('with an editMode flag', function () {
    beforeEach(function () {
      state.editMode = true;
    });

    /** @test {Step#editMode} */
    it('should have the specified editMode value', () => {
      expect(state.editMode).to.be.true;
    });

    /** @test {Step#editMode} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editMode = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.be.true;
    });
  });

  describe('without a editMode flag', function () {
    beforeEach(function () {
      state.editMode = null;
    });

    /** @test {FieldState#editMode} */
    it('should be allowed', () => {
      expect(state.editMode).to.be.null;
    });

    /** @test {FieldState#toJSON} */
    it('should not include a editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.not.exist;
    });
  });

  describe('with an external URL', function () {
    beforeEach(function () {
      state.externalUrl = 'http://www.example.com';
    });

    /** @test {Step#externalUrl} */
    it('should have the specified external URL value', () => {
      expect(state.externalUrl).to.equal('http://www.example.com');
    });

    /** @test {Step#externalUrl} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { state.externalUrl = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the external URL when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.equal('http://www.example.com');
    });
  });

  describe('without an external URL', function () {
    beforeEach(function () {
      state.externalUrl = null;
    });

    /** @test {FieldState#externalUrl} */
    it('should be allowed', () => {
      expect(state.externalUrl).to.be.null;
    });

    /** @test {FieldState#toJSON} */
    it('should not include a externalUrl when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.not.exist;
    });
  });

  describe('with an extraInfoCollapsed flag', function () {
    beforeEach(function () {
      state.extraInfoCollapsed = true;
    });

    /** @test {Step#extraInfoCollapsed} */
    it('should have the specified extraInfoCollapsed value', () => {
      expect(state.extraInfoCollapsed).to.be.true;
    });

    /** @test {Step#extraInfoCollapsed} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.extraInfoCollapsed = 5; }).to.throw(TypeError);
    });

    /** @test {Step#toJSON} */
    it('should include the extraInfoCollapsed flag when exporting', () => {
      const output = state.toJSON();
      expect(output.extraInfoCollapsed).to.be.true;
    });
  });

  describe('without a extraInfoCollapsed flag', function () {
    beforeEach(function () {
      state.extraInfoCollapsed = null;
    });

    /** @test {FieldState#extraInfoCollapsed} */
    it('should be allowed', () => {
      expect(state.extraInfoCollapsed).to.be.null;
    });

    /** @test {FieldState#toJSON} */
    it('should not include a extraInfoCollapsed flag when exporting', () => {
      const output = state.toJSON();
      expect(output.extraInfoCollapsed).to.not.exist;
    });
  });

  /** @test {FieldState#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    state.randomAttribute = 5;
    const output = state.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldState.fromJSON} */
    it('should return a FieldState instance', () => {
      const jsonData = '{ "editable": true, "editMode": false }';
      const state = FieldState.fromJSON(jsonData);
      expect(state).to.be.an.instanceof(FieldState);
      expect(state.editable).to.be.true;
      expect(state.editMode).to.be.false;
    });
  });
});
