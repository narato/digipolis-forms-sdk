import chai from 'chai';
import {StepState} from '../../src/index.js';

const expect = chai.expect;

var state;

/** @test {StepState} */
describe('A StepState instance', function () {
  beforeEach(function () {
    state = new StepState();
  });

  describe('with a showTitle flag', function () {
    beforeEach(function () {
      state.showTitle = true;
    });

    /** @test {StepState#showTitle} */
    it('should have the specified showTitle value', () => {
      expect(state.showTitle).to.be.true;
    });

    /** @test {StepState#showTitle} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.showTitle = 5; }).to.throw(TypeError);
    });

    /** @test {StepState#toJSON} */
    it('should include the showTitle flag when exporting', () => {
      const output = state.toJSON();
      expect(output.showTitle).to.be.true;
    });
  });

  describe('without a showTitle flag', function () {
    beforeEach(function () {
      state.showTitle = null;
    });

    /** @test {StepState#showTitle} */
    it('should be allowed', () => {
      expect(state.showTitle).to.be.null;
    });

    /** @test {StepState#toJSON} */
    it('should not include a showTitle flag when exporting', () => {
      const output = state.toJSON();
      expect(output.showTitle).to.not.exist;
    });
  });

  describe('with an editable flag', function () {
    beforeEach(function () {
      state.editable = true;
    });

    /** @test {StepState#editable} */
    it('should have the specified editable value', () => {
      expect(state.editable).to.be.true;
    });

    /** @test {StepState#editable} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editable = 5; }).to.throw(TypeError);
    });

    /** @test {StepState#toJSON} */
    it('should include the editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.be.true;
    });
  });

  describe('without a editable flag', function () {
    beforeEach(function () {
      state.editable = null;
    });

    /** @test {StepState#editable} */
    it('should be allowed', () => {
      expect(state.editable).to.be.null;
    });

    /** @test {StepState#toJSON} */
    it('should not include a editable flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editable).to.not.exist;
    });
  });

  describe('with an editMode flag', function () {
    beforeEach(function () {
      state.editMode = true;
    });

    /** @test {StepState#editMode} */
    it('should have the specified editMode value', () => {
      expect(state.editMode).to.be.true;
    });

    /** @test {StepState#editMode} */
    it('throws an exception when the specified value is not a boolean', () => {
      expect(() => { state.editMode = 5; }).to.throw(TypeError);
    });

    /** @test {StepState#toJSON} */
    it('should include the editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.be.true;
    });
  });

  describe('without a editMode flag', function () {
    beforeEach(function () {
      state.editMode = null;
    });

    /** @test {StepState#editMode} */
    it('should be allowed', () => {
      expect(state.editMode).to.be.null;
    });

    /** @test {StepState#toJSON} */
    it('should not include a editMode flag when exporting', () => {
      const output = state.toJSON();
      expect(output.editMode).to.not.exist;
    });
  });

  describe('with an external URL', function () {
    beforeEach(function () {
      state.externalUrl = 'http://www.example.com';
    });

    /** @test {StepState#externalUrl} */
    it('should have the specified external URL value', () => {
      expect(state.externalUrl).to.equal('http://www.example.com');
    });

    /** @test {StepState#externalUrl} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { state.externalUrl = 5; }).to.throw(TypeError);
    });

    /** @test {StepState#toJSON} */
    it('should include the external URL when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.equal('http://www.example.com');
    });
  });

  describe('without an external URL', function () {
    beforeEach(function () {
      state.externalUrl = null;
    });

    /** @test {StepState#externalUrl} */
    it('should be allowed', () => {
      expect(state.externalUrl).to.be.null;
    });

    /** @test {StepState#toJSON} */
    it('should not include a externalUrl when exporting', () => {
      const output = state.toJSON();
      expect(output.externalUrl).to.not.exist;
    });
  });

  /** @test {StepState#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    state.randomAttribute = 5;
    const output = state.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {StepState.fromJSON} */
    it('should return a StepState instance', () => {
      const jsonData = '{ "editable": true, "editMode": false }';
      const state = StepState.fromJSON(jsonData);
      expect(state).to.be.an.instanceof(StepState);
      expect(state.editable).to.be.true;
      expect(state.editMode).to.be.false;
    });
  });
});
