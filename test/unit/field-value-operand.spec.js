'use strict';

import chai from 'chai';
import {FieldValueOperand} from '../../src/index.js';

const expect = chai.expect;

var operand;

/** @test {FieldValueOperand} */
describe('A FieldValueOperand instance', function () {
  beforeEach(function () {
    operand = new FieldValueOperand();
  });

  describe('with a name', function () {
    beforeEach(function () {
      const n = 'field0';
      operand.name = n;
    });

    /** @test {FieldValueOperand#name} */
    it('should have the specified name', () => {
      expect(operand.name).to.equal('field0');
    });

    /** @test {FieldValueOperand#name} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { operand.name = 5; }).to.throw(TypeError);
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should include the name when exporting', () => {
      const output = operand.toJSON();
      expect(output.name).to.equal('field0');
    });
  });

  describe('without a name', function () {
    beforeEach(function () {
      operand.name = null;
    });

    /** @test {FieldValueOperand#name} */
    it('should be allowed', () => {
      expect(operand.name).to.be.null;
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should not include a name when exporting', () => {
      const output = operand.toJSON();
      expect(output.name).to.not.exist;
    });
  });

  describe('with a value', function () {
    beforeEach(function () {
      const v = 'example value';
      operand.value = v;
    });

    /** @test {FieldValueOperand#value} */
    it('should have the specified value', () => {
      expect(operand.value).to.equal('example value');
    });

    /** @test {FieldValueOperand#value} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { operand.value = 5; }).to.throw(TypeError);
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should include the value when exporting', () => {
      const output = operand.toJSON();
      expect(output.value).to.equal('example value');
    });
  });

  describe('without a value', function () {
    beforeEach(function () {
      operand.value = null;
    });

    /** @test {FieldValueOperand#value} */
    it('should be allowed', () => {
      expect(operand.value).to.be.null;
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should not include a value when exporting', () => {
      const output = operand.toJSON();
      expect(output.value).to.not.exist;
    });
  });

  describe('with a operator', function () {
    beforeEach(function () {
      const o = 'eq';
      operand.operator = o;
    });

    /** @test {FieldValueOperand#operator} */
    it('should have the specified operator', () => {
      expect(operand.operator).to.equal('eq');
    });

    /** @test {FieldValueOperand#operator} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { operand.operator = 5; }).to.throw(TypeError);
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should include the operator when exporting', () => {
      const output = operand.toJSON();
      expect(output.operator).to.equal('eq');
    });
  });

  describe('without a operator', function () {
    beforeEach(function () {
      operand.operator = null;
    });

    /** @test {FieldValueOperand#operator} */
    it('should be allowed', () => {
      expect(operand.operator).to.be.null;
    });

    /** @test {FieldValueOperand#toJSON} */
    it('should not include a operator when exporting', () => {
      const output = operand.toJSON();
      expect(output.operator).to.not.exist;
    });
  });

  /** @test {FieldValueOperand#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    operand.randomAttribute = 5;
    const output = operand.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {FieldValueOperand.fromJSON} */
    it('should return a FieldValueOperand instance', () => {
      const jsonData = '{ "name": "field0" }';
      const operand = FieldValueOperand.fromJSON(jsonData);
      expect(operand).to.be.an.instanceof(FieldValueOperand);
      expect(operand.name).to.equal('field0');
    });
  });

});
