'use strict';

import chai from 'chai';
import {FieldSpecOptionsValue} from '../../src/index.js';

const expect = chai.expect;
var value;

/** @test {FieldSpecOptionsValue} */
describe('A FieldSpecOptionsValue instance', function () {
  beforeEach(function () {
    value = new FieldSpecOptionsValue();
  });

  describe('with a key', function () {
    beforeEach(function () {
      value.key = 'key1';
    });

    /** @test {FieldSpecOptionsValue#key} */
    it('should have the specified key value', () => {
      expect(value.key).to.equal('key1');
    });

    /** @test {FieldSpecOptionsValue#key} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { value.key = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecOptionsValue#key} */
    it('should include the key in the JSON representation', () => {
      let output = value.toJSON();
      expect(output.key).to.equal('key1');
    });
  });

  describe('without a key', function () {
    beforeEach(function () {
      value.key = null;
    });

    it('should be allowed', () => {
      expect(value.key).to.be.null;
    });

    /** @test {FieldSpecOptionsValue#toJSON} */
    it('should not include a key when exporting', () => {
      const output = value.toJSON();
      expect(output.key).to.not.exist;
    });
  });

  describe('with a value', function () {
    beforeEach(function () {
      value.value = 'value1';
    });

    /** @test {FieldSpecOptionsValue#value} */
    it('should have the specified value', () => {
      expect(value.value).to.equal('value1');
    });

    /** @test {FieldSpecOptionsValue#value} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { value.value = 5; }).to.throw(TypeError);
    });

    /** @test {FieldSpecOptionsValue#value} */
    it('should include the value in the JSON representation', () => {
      let output = value.toJSON();
      expect(output.value).to.equal('value1');
    });
  });

  /** @test {FieldSpecOptionsValue#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    value.randomAttribute = 5;
    const output = value.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('without a value', function () {
    beforeEach(function () {
      value.value = null;
    });

    it('should be allowed', () => {
      expect(value.value).to.be.null;
    });

    /** @test {FieldSpecOptionsValue#toJSON} */
    it('should not include a value when exporting', () => {
      const output = value.toJSON();
      expect(output.value).to.not.exist;
    });
  });
});
