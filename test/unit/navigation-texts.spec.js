'use strict';

import chai from 'chai';
import {NavigationTexts} from '../../src/index.js';

const expect = chai.expect;

var nav;

/** @test {NavigationTexts} */
describe('A NavigationTexts instance', function () {
  beforeEach(function () {
    nav = new NavigationTexts();
  });

  describe('with a submit label', function () {
    beforeEach(function () {
      let l = 'example submit label';
      nav.submit = l;
    });

    /** @test {NavigationTexts#submit} */
    it('should have the specified submit label', () => {
      expect(nav.submit).to.equal('example submit label');
    });

    /** @test {NavigationTexts#submit} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { nav.submit = 5; }).to.throw(TypeError);
    });

    /** @test {NavigationTexts#toJSON} */
    it('should include the submit label when exporting', () => {
      const output = nav.toJSON();
      expect(output.submit).to.equal('example submit label');
    });
  });

  describe('without a submit label', function () {
    beforeEach(function () {
      nav.submit = null;
    });

    /** @test {NavigationTexts#submit} */
    it('should be allowed', () => {
      expect(nav.submit).to.be.null;
    });

    /** @test {NavigationTexts#toJSON} */
    it('should not include a submit label when exporting', () => {
      const output = nav.toJSON();
      expect(output.submit).to.not.exist;
    });

  });

  describe('with a next label', function () {
    beforeEach(function () {
      let l = 'example next label';
      nav.next = l;
    });

    /** @test {NavigationTexts#next} */
    it('should have the specified next label', () => {
      expect(nav.next).to.equal('example next label');
    });

    /** @test {NavigationTexts#next} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { nav.next = 5; }).to.throw(TypeError);
    });

    /** @test {NavigationTexts#toJSON} */
    it('should include the next label when exporting', () => {
      const output = nav.toJSON();
      expect(output.next).to.equal('example next label');
    });
  });

  describe('without a next label', function () {
    beforeEach(function () {
      nav.next = null;
    });

    /** @test {NavigationTexts#next} */
    it('should be allowed', () => {
      expect(nav.next).to.be.null;
    });

    /** @test {NavigationTexts#toJSON} */
    it('should not include a next label when exporting', () => {
      const output = nav.toJSON();
      expect(output.next).to.not.exist;
    });

  });

  describe('with a previous label', function () {
    beforeEach(function () {
      let l = 'example previous label';
      nav.previous = l;
    });

    /** @test {NavigationTexts#previous} */
    it('should have the specified previous label', () => {
      expect(nav.previous).to.equal('example previous label');
    });

    /** @test {NavigationTexts#previous} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { nav.previous = 5; }).to.throw(TypeError);
    });

    /** @test {NavigationTexts#toJSON} */
    it('should include the previous label when exporting', () => {
      const output = nav.toJSON();
      expect(output.previous).to.equal('example previous label');
    });
  });

  describe('without a previous label', function () {
    beforeEach(function () {
      nav.previous = null;
    });

    /** @test {NavigationTexts#previous} */
    it('should be allowed', () => {
      expect(nav.previous).to.be.null;
    });

    /** @test {NavigationTexts#toJSON} */
    it('should not include a previous label when exporting', () => {
      const output = nav.toJSON();
      expect(output.previous).to.not.exist;
    });

  });

  describe('with a concept label', function () {
    beforeEach(function () {
      let l = 'example concept label';
      nav.concept = l;
    });

    /** @test {NavigationTexts#concept} */
    it('should have the specified concept label', () => {
      expect(nav.concept).to.equal('example concept label');
    });

    /** @test {NavigationTexts#concept} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { nav.concept = 5; }).to.throw(TypeError);
    });

    /** @test {NavigationTexts#toJSON} */
    it('should include the concept label when exporting', () => {
      const output = nav.toJSON();
      expect(output.concept).to.equal('example concept label');
    });
  });

  describe('without a concept label', function () {
    beforeEach(function () {
      nav.concept = null;
    });

    /** @test {NavigationTexts#concept} */
    it('should be allowed', () => {
      expect(nav.concept).to.be.null;
    });

    /** @test {NavigationTexts#toJSON} */
    it('should not include a concept label when exporting', () => {
      const output = nav.toJSON();
      expect(output.concept).to.not.exist;
    });

  });

  /** @test {NavigationTexts#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    nav.randomAttribute = 5;
    const output = nav.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {NavigationTexts.fromJSON} */
    it('should return a NavigationTexts instance', () => {
      const jsonData = '{"next":"next test label"}';
      const nav = NavigationTexts.fromJSON(jsonData);
      expect(nav).to.be.an.instanceof(NavigationTexts);
      expect(nav.next).to.equal('next test label');
    });
  });

});
