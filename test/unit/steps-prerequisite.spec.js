'use strict';

import chai from 'chai';
import {Step, StepsPrerequisite} from '../../src/index.js';

const expect = chai.expect;

var prerequisites;

/** @test {StepsPrerequisite} */
describe('A StepsPrerequisite instance', function () {
  beforeEach(function () {
    prerequisites = new StepsPrerequisite();
  });

  describe('with a logical', function () {
    beforeEach(function () {
      const l = 'and';
      prerequisites.logical = l;
    });

    /** @test {StepsPrerequisite#logical} */
    it('should have the specified logical', () => {
      expect(prerequisites.logical).to.equal('and');
    });

    /** @test {StepsPrerequisite#logical} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.logical = 5; }).to.throw(TypeError);
    });

    /** @test {StepsPrerequisite#toJSON} */
    it('should include the logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.equal('and');
    });
  });

  describe('without a logical', function () {
    beforeEach(function () {
      prerequisites.logical = null;
    });

    /** @test {StepsPrerequisite#logical} */
    it('should be allowed', () => {
      expect(prerequisites.logical).to.be.null;
    });

    /** @test {StepsPrerequisite#toJSON} */
    it('should not include a logical when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.logical).to.not.exist;
    });
  });

  describe('with steps IDs', function () {
    var steps;
    beforeEach(function () {
      steps = ['step0', 'step1'];
      prerequisites.steps = steps;
    });

    /** @test {Prerequisites#steps} */
    it('should have the specified steps', () => {
      expect(prerequisites.steps).to.equal(steps);
    });

    /** @test {Prerequisites#steps} */
    it('throws an exception when the specified value is not an Array', () => {
      expect(() => { prerequisites.steps = 5; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#steps} */
    it('throws an exception when the specified value is not an Array of strings', () => {
      expect(() => { prerequisites.steps = [1, 2, 3]; }).to.throw(TypeError);
    });

    /** @test {Prerequisites#toJSON} */
    it('should include the steps when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.steps).to.exist;
    });
  });

  describe('with an empty steps array', function () {
    beforeEach(function () {
      prerequisites.steps = [];
    });

    /** @test {Prerequisites#steps} */
    it('should be allowed', () => {
      expect(prerequisites.steps).to.be.empty;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include steps when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.steps).to.not.exist;
    });
  });

  describe('without steps', function () {
    beforeEach(function () {
      prerequisites.steps = null;
    });

    /** @test {Prerequisites#steps} */
    it('should be allowed', () => {
      expect(prerequisites.steps).to.be.null;
    });

    /** @test {Prerequisites#toJSON} */
    it('should not include steps when exporting', () => {
      const output = prerequisites.toJSON();
      expect(output.steps).to.not.exist;
    });
  });

  describe('when adding a step', function () {
    /** @test {StepsPrerequisite#addStep} */
    it('should have added the step to the prerequisites', () => {
      let step = new Step('step0');
      prerequisites.addStep(step);
      let anotherStep = new Step('step1');
      prerequisites.addStep(anotherStep);

      expect(prerequisites.steps[0]).to.equal('step0');
      expect(prerequisites.steps[1]).to.equal('step1');
    });

    /** @test {StepsPrerequisite#addStepId} */
    it('should have added the step id to the prerequisites', () => {
      prerequisites.addStepId('step0');
      prerequisites.addStepId('step1');

      expect(prerequisites.steps[0]).to.equal('step0');
      expect(prerequisites.steps[1]).to.equal('step1');
    });

    /** @test {StepsPrerequisite#addStepId} */
    it('should not allow a duplicate step', () => {
      prerequisites.addStepId('step0');
      let addDuplicateStep = () => prerequisites.addStepId('step0');

      expect(addDuplicateStep).to.throw(/Step ID/);
    });

    /** @test {StepsPrerequisite#addStep} */
    it('throws an exception when the specified value is not a Step', () => {
      expect(() => { prerequisites.addStep(5); }).to.throw(TypeError);
    });

    /** @test {StepsPrerequisite#addStepId} */
    it('throws an exception when the specified value is not a string', () => {
      expect(() => { prerequisites.addStepId(5); }).to.throw(TypeError);
    });
  });

  describe('when removing a step', function () {
    /** @test {StepsPrerequisite#removeStepId} */
    it('should have removed the step id of the prerequisites', () => {
      prerequisites.addStepId('step0');
      prerequisites.addStepId('step1');
      prerequisites.addStepId('step2');
      expect(prerequisites.steps.length).to.equal(3);

      prerequisites.removeStepId('step1');

      expect(prerequisites.steps.length).to.equal(2);
      expect(prerequisites.steps).not.to.include('step1');
    });

    /** @test {StepsPrerequisite#removeStep} */
    it('should have removed the step of the prerequisites', () => {
      prerequisites.addStepId('step0');
      prerequisites.addStepId('step1');
      prerequisites.addStepId('step2');
      expect(prerequisites.steps.length).to.equal(3);

      prerequisites.removeStep(new Step('step1'));

      expect(prerequisites.steps.length).to.equal(2);
      expect(prerequisites.steps).not.to.include('step1');
    });

    /** @test {StepsPrerequisite#removeStepId} */
    it('throws an error if specified step does not exist', () => {
      let removeNonExistingStep = () => prerequisites.removeStepId('step0');

      expect(removeNonExistingStep).to.throw(Error);
    });

    /** @test {StepsPrerequisite#removeStepId} */
    it('throws an exception when the specified value is not a String', () => {
      expect(() => { prerequisites.removeStepId(5); }).to.throw(TypeError);
    });

    /** @test {StepsPrerequisite#removeStep} */
    it('throws an exception when the specified value is not a Step', () => {
      expect(() => { prerequisites.removeStep(5); }).to.throw(TypeError);
    });
  });

  /** @test {StepsPrerequisite#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    prerequisites.randomAttribute = 5;
    const output = prerequisites.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {StepsPrerequisite.fromJSON} */
    it('should return a StepsPrerequisite instance', () => {
      const jsonData = '{ "logical": "and" }';
      const prerequisites = StepsPrerequisite.fromJSON(jsonData);
      expect(prerequisites).to.be.an.instanceof(StepsPrerequisite);
    });
  });
});
