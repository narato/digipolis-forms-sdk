import chai from 'chai';
import {Field, FieldSpec, FieldState, FieldInputFilter, Prerequisites} from '../../src/index.js';

const expect = chai.expect;

var field;

/** @test {Field} */
describe('Creating a Field instance', function () {
  /** @test {Field#name} */
  it('throws an exception without a name', () => {
    expect(() => { const field = new Field(null); }).to.throw(/Field must have a name/);
  });

  /** @test {Field#name} */
  it('throws an exception when specified value is not a string', () => {
    expect(() => { const field = new Field(5); }).to.throw(TypeError);
  });
});

/** @test {Field} */
describe('A Field instance', function () {
  beforeEach(function () {
    field = new Field('name0');
  });

  describe('with a name', function () {
    /** @test {Field#name} */
    it('should use name from constructor', () => {
      expect(field.name).to.equal('name0');
    });

    /** @test {Field#toJSON} */
    it('should include the name when exporting', () => {
      const output = field.toJSON();
      expect(output.name).to.equal('name0');
    });
  });

  describe('with a spec', function () {
    var s;
    beforeEach(function () {
      s = new FieldSpec();
      field.spec = s;
    });

    /** @test {Field#spec} */
    it('should have the specified spec', () => {
      expect(field.spec).to.equal(s);
    });

    /** @test {Field#spec} */
    it('throws an exception when the specified value is not a FieldSpec object', () => {
      expect(() => { field.spec = 5; }).to.throw(TypeError);
    });

    /** @test {Field#toJSON} */
    it('should include the spec when exporting', () => {
      const output = field.toJSON();
      expect(output.spec).to.exist;
    });
  });

  describe('without a spec', function () {
    beforeEach(function () {
      field.spec = null;
    });

    /** @test {Field#spec} */
    it('should be allowed', () => {
      expect(field.spec).to.be.null;
    });

    /** @test {Field#toJSON} */
    it('should not include a spec when exporting', () => {
      const output = field.toJSON();
      expect(output.spec).to.not.exist;
    });
  });

  describe('with a state', function () {
    var s;
    beforeEach(function () {
      s = new FieldState();
      field.state = s;
    });

    /** @test {Field#state} */
    it('should have the specified state type value', () => {
      expect(field.state).to.equal(s);
    });

    /** @test {Field#state} */
    it('throws an exception when the specified value is not a FieldState object', () => {
      expect(() => { field.state = 5; }).to.throw(TypeError);
    });

    /** @test {Field#toJSON} */
    it('should include the state when exporting', () => {
      const output = field.toJSON();
      expect(output.state).to.exist;
    });
  });

  describe('without a state', function () {
    beforeEach(function () {
      field.state = null;
    });

    /** @test {Field#state} */
    it('should be allowed', () => {
      expect(field.state).to.be.null;
    });

    /** @test {Field#toJSON} */
    it('should not include a state when exporting', () => {
      const output = field.toJSON();
      expect(output.state).to.not.exist;
    });
  });

  describe('with prerequisites', function () {
    var p;
    beforeEach(function () {
      p = new Prerequisites();
      field.prerequisites = p;
    });

    /** @test {Field#prerequisites} */
    it('should have the specified prerequisites', () => {
      expect(field.prerequisites).to.equal(p);
    });

    /** @test {Field#prerequisites} */
    it('throws an exception when the specified value is not a Prerequisites object', () => {
      expect(() => { field.prerequisites = 5; }).to.throw(TypeError);
    });

    /** @test {Field#toJSON} */
    it('should include the prerequisites when exporting', () => {
      const output = field.toJSON();
      expect(output.prerequisites).to.exist;
    });
  });

  describe('without prerequisites', function () {
    beforeEach(function () {
      field.prerequisites = null;
    });

    /** @test {Field#prerequisites} */
    it('should be allowed', () => {
      expect(field.prerequisites).to.be.null;
    });

    /** @test {Field#toJSON} */
    it('should not include a prerequisites when exporting', () => {
      const output = field.toJSON();
      expect(output.prerequisites).to.not.exist;
    });
  });

  describe('with an inputFilter', function () {
    var inputFilter;
    beforeEach(function () {
      inputFilter = new FieldInputFilter();
      field.inputFilter = inputFilter;
    });

    /** @test {Field#inputFilter} */
    it('should have the specified inputFilter value', () => {
      expect(field.inputFilter).to.equal(inputFilter);
    });

    /** @test {Field#inputFilter} */
    it('throws an exception when the specified value is not a FieldInputFilter object', () => {
      expect(() => { field.inputFilter = 5; }).to.throw(TypeError);
    });

    /** @test {Field#toJSON} */
    it('should include the inputFilter when exporting', () => {
      const output = field.toJSON();
      expect(output.inputFilter).to.exist;
    });
  });

  describe('without an inputFilter', function () {
    beforeEach(function () {
      field.inputFilter = null;
    });

    /** @test {Field#inputFilter} */
    it('should be allowed', () => {
      expect(field.inputFilter).to.be.null;
    });

    /** @test {Field#toJSON} */
    it('should not include an inputFilter when exporting', () => {
      const output = field.toJSON();
      expect(output.inputFilter).to.not.exist;
    });
  });

  /** @test {Field#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    field.randomAttribute = 5;
    const output = field.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {Field.fromJSON} */
    it('should return a Field instance', () => {
      const jsonData = '{ "name": "field name" }';
      const field = Field.fromJSON(jsonData);
      expect(field).to.be.an.instanceof(Field);
      expect(field.name).to.equal('field name');
    });

    /** @test {Field.fromJSON} */
    it('should contain a nested FieldState object', () => {
      const jsonData = `{
        "name": "field name",
        "state": { "editMode": true }
      }`;
      const field = Field.fromJSON(jsonData);
      expect(field.state).to.be.an.instanceof(FieldState);
    });

    /** @test {Field.fromJSON} */
    it('should contain a nested FieldSpec object', () => {
      const jsonData = `{
        "name": "field name",
        "spec": { "attributes": { "type": "textbox" } }
      }`;
      const field = Field.fromJSON(jsonData);
      expect(field.spec).to.be.an.instanceof(FieldSpec);
    });

    /** @test {Field.fromJSON} */
    it('should contain a nested Prerequisites object', () => {
      const jsonData = `{
        "name": "field0",
        "prerequisites": { "fieldsCompleted": { "logical": "and" } }
      }`;
      const field = Field.fromJSON(jsonData);
      expect(field.prerequisites).to.be.an.instanceof(Prerequisites);
    });

    /** @test {Field.fromJSON} */
    it('should contain a nested FieldInputFilter object', () => {
      const jsonData = `{
        "name": "field name",
        "inputFilter": { "required": true }
      }`;
      const field = Field.fromJSON(jsonData);
      expect(field.inputFilter).to.be.an.instanceof(FieldInputFilter);
    });

    /** @test {Field.fromJSON} */
    it('should return null if null was given', () => {
      expect(Field.fromJSON(null)).to.be.null;
    });
  });
});
