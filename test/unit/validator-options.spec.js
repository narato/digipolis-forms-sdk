import chai from 'chai';
import {ValidatorOptions} from '../../src/index.js';

const expect = chai.expect;

var options;

/** @test {ValidatorOptions} */
describe('A ValidatorOptions instance', function () {
  beforeEach(function () {
    options = new ValidatorOptions();
  });

  describe('with a pattern', function () {
    beforeEach(function () {
      options.pattern = '/^[1-9]|[1-9][0-9]+$/';
    });

    /** @test {ValidatorOptions#pattern} */
    it('should have the pattern value', () => {
      expect(options.pattern).to.equal('/^[1-9]|[1-9][0-9]+$/');
    });

    /** @test {ValidatorOptions#toJSON} */
    it('should include the pattern when exporting', () => {
      const output = options.toJSON();
      expect(output.pattern).to.equal('/^[1-9]|[1-9][0-9]+$/');
    });
  });

  describe('without a pattern', function () {
    beforeEach(function () {
      options.pattern = null;
    });

    /** @test {ValidatorOptions#pattern} */
    it('should be allowed', () => {
      expect(options.pattern).to.be.null;
    });

    /** @test {ValidatorOptions#toJSON} */
    it('should not include a pattern when exporting', () => {
      const output = options.toJSON();
      expect(output.pattern).to.not.exist;
    });
  });

  /** @test {ValidatorOptions#toJSON} */
  it('should include a random attribute, for forward-compatibility purposes', () => {
    options.randomAttribute = 5;
    const output = options.toJSON();
    expect(output.randomAttribute).to.equal(5);
  });

  describe('created from a JSON string', function () {
    /** @test {ValidatorOptions.fromJSON} */
    it('should return a ValidatorOptions instance', () => {
      const jsonData = '{ "pattern": "/^[1-9]|[1-9][0-9]+$/" }';
      const options = ValidatorOptions.fromJSON(jsonData);
      expect(options).to.be.an.instanceof(ValidatorOptions);
      expect(options.pattern).to.equal('/^[1-9]|[1-9][0-9]+$/');
    });
  });
});
