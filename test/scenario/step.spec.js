import chai from 'chai';
import {Step} from '../../src/index.js';

const expect = chai.expect;
const jsonData = '{"id":"step0","title":"step title","subtitle":"step subtitle","body":"step body","prerequisites":{"fieldValues":{"logical":"AND"}}}';
var step;

describe('When deserializing and serializing a Step', function () {
  beforeEach(function () {
    step = Step.fromJSON(jsonData);
  });

  it('should return the correct JSON representation', () => {
    let output = JSON.stringify(step);
    expect(output).to.equal(jsonData);
  });

  describe('with additional unknown properties', function () {
    var jsonDataWithExtras;

    beforeEach(function () {
      let obj = JSON.parse(jsonData);
      obj.foo = 'bar';
      jsonDataWithExtras = JSON.stringify(obj);
      step = Step.fromJSON(jsonDataWithExtras);
    });

    it('should preserve all unknown properties', () => {
      let output = JSON.stringify(step);
      expect(output).to.equal(jsonDataWithExtras);
    });
  });
});
