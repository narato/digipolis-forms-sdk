import chai from 'chai';
import ComposerSdk from '../../src/index.js';
import fs from 'fs';

const expect = chai.expect;

const runs = [
  { form: fs.readFileSync('examples/minimal-form.json', 'utf8') },
  { form: fs.readFileSync('examples/main.json', 'utf8') },
  { form: fs.readFileSync('examples/maximal-form.json', 'utf8') }
];

/** @test {ComposerSdk} */
describe('When deserializing and serializing a form template', function () {

  before(function () {
  });

  runs.forEach(function (run, index) {
    /** @test {FormSkd.loadFromJson} */
    it(`should return the correct JSON template [${index}]`, () => {
      let obj = JSON.parse(run.form);
      let form = ComposerSdk.loadFromJSON(run.form);
      let output = form.toJSON();
      expect(output).to.eql(obj);
    });
  });
});
