import chai from 'chai';
import { Section } from '../../src/index.js';

const expect = chai.expect;
const jsonData = '{"id":"section0","title":"section title","prerequisites":{"fieldValues":{"logical":"AND"}}}';
var section;

describe('When deserializing and serializing a Section', function () {
  beforeEach(function () {
    section = Section.fromJSON(jsonData);
  });

  it('should return the correct JSON string representation', () => {
    let output = JSON.stringify(section);
    expect(output).to.equal(jsonData);
  });

  describe('with additional unknown properties', function () {
    var jsonDataWithExtras;

    beforeEach(function () {
      let obj = JSON.parse(jsonData);
      obj.foo = 'bar';
      jsonDataWithExtras = JSON.stringify(obj);
      section = Section.fromJSON(jsonDataWithExtras);
    });

    it('should preserve all unknown properties', () => {
      let output = JSON.stringify(section);
      expect(output).to.equal(jsonDataWithExtras);
    });
  });
});
