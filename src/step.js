import Field from './field.js';
import JsonConvertable from './json-convertable.js';
import Prerequisites from './prerequisites.js';
import NavigationTexts from './navigation-texts.js';
import Section from './section.js';
import StepState from './step-state.js';
import TypeCheck from './typecheck.js';

/**
 * Defines a Step for use inside a Form object
 */
export default class Step extends JsonConvertable {

  /**
   * Creates an instance of Step.
   *
   * @param {string} id The ID of the Step
   *
   * @throws {Error} when specified value is null
   * @throws {TypeError} when specified value is not a string
   */
  constructor(id) {
    super();

    if (id == null) {
      throw new Error('Step must have an id');
    }
    if (id && !TypeCheck.isString(id)) {
      throw new TypeError('Step id must be a string');
    }

    this._id = id;
    this._title;
    this._subtitle;
    this._body;
    this._type;
    this._state = new StepState();
    this._prerequisites = new Prerequisites();
    this._navigationTexts = new NavigationTexts();
    this._sections = [];
    this._fields = [];
  }

  /**
   * Gets the ID
   *
   * @type {string}
   */
  get id() {
    return this._id;
  }

  /**
   * Gets the title content
   *
   * @type {string}
   */
  get title() {
    return this._title;
  }

  /**
   * Sets the title content
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set title(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._title = value;
  }

  /**
   * Gets the subtitle content
   *
   * @type {string}
   */
  get subtitle() {
    return this._subtitle;
  }

  /**
   * Sets the subtitle content
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set subtitle(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._subtitle = value;
  }

  /**
   * Gets the body content
   *
   * @type {string}
   */
  get body() {
    return this._body;
  }

  /**
   * Sets the body content
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set body(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._body = value;
  }

  /**
   * Gets the type
   *
   * @type {string}
   */
  get type() {
    return this._type;
  }

  /**
   * Sets the type
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set type(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._type = value;
  }

  /**
   * Gets the state
   *
   * @type {StepState}
   */
  get state() {
    return this._state;
  }

  /**
   * Sets the state
   *
   * @type {StepState}
   *
   * @throws {TypeError} when specified value is not a StepState object
   */
  set state(value) {
    if (value && !(value instanceof StepState)) {
      throw new TypeError('Value must be a StepState object');
    }
    this._state = value;
  }

  /**
   * Gets the prerequisites
   *
   * @type {Prerequisites}
   */
  get prerequisites() {
    return this._prerequisites;
  }

  /**
   * Sets the prerequisites
   *
   * @type {Prerequisites}
   *
   * @throws {TypeError} when specified value is not a Prerequisites object
   */
  set prerequisites(value) {
    if (value && !(value instanceof Prerequisites)) {
      throw new TypeError('Value must be a Prerequisites object');
    }
    this._prerequisites = value;
  }

  /**
   * Retrieve the navigation texts
   *
   * @type {NavigationTexts}
   */
  get navigationTexts() {
    return this._navigationTexts;
  }

  /**
   * Sets the navigation texts
   *
   * @type {NavigationTexts}
   *
   * @throws {TypeError} when specified value is not a NavigationTexts object
   */
  set navigationTexts(value) {
    if (value && !(value instanceof NavigationTexts)) {
      throw new TypeError('Value must be a NavigationTexts object');
    }
    this._navigationTexts = value;
  }

  /**
   * Gets the entire collection of Section objects
   *
   * @type {Section[]}
   */
  get sections() {
    return this._sections;
  }

  /**
   * Sets the entire collection of Section objects,
   * overriding the existing collection.
   *
   * @type {Section[]}
   *
   * @throws {TypeError} when specified value is not an array of Sections
   */
  set sections(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Section; })) {
      throw new TypeError('Value must be an array of Sections');
    }
    this._sections = value;
  }

  /**
   * Gets the entire collection of Field objects
   *
   * @type {Field[]}
   */
  get fields() {
    return this._fields;
  }

  /**
   * Sets the entire collection of Field objects,
   * overriding the existing collection.
   *
   * @type {Field[]}
   *
   * @throws {TypeError} when specified value is not an array of Fields
   */
  set fields(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Field; })) {
      throw new TypeError('Value must be an array of Fields');
    }
    this._fields = value;
  }

  /**
   * Adds a Section object to the Step
   *
   * @param {Section} section The Section object to add to the step
   * @throws {Error} A Section with the same ID already exists
   * @throws {TypeError} when specified value is not a Section
   */
  addSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }
    if (this.getSection(section.id) !== undefined) {
      throw new Error(`A Section with id ${section.id} already exists`);
    }

    this._sections.push(section);
  }

  /**
   * Returns the Section object with the specified ID
   *
   * @param {string} sectionId The ID of the Section to retrieve
   * @return {Section} The Section with the specified ID.
   *                undefined if a section with the specified ID doesn't exist.
   */
  getSection(sectionId) {
    return this._sections.find(s => s.id === sectionId);
  }

  /**
   * Update a Section object from the Step
   *
   * @param {Section} section The Section object to update
   * @throws {Error} A Section with the given ID doesn't exist in the step
   * @throws {TypeError} when specified value is not a Section
   */
  updateSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }

    const index = this._sections.findIndex(s => s.id === section.id);
    if (index === -1) {
      throw new Error(`A Section with id ${section.id} doesn't exist`);
    }

    this._sections[index] = section;
  }

  /**
   * Remove a Section object from the Step
   *
   * @param {Section} section The Section object to remove from the step
   * @throws {Error} A Section with the given ID doesn't exist in the step
   * @throws {TypeError} when specified value is not a Section
   */
  removeSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }

    const index = this._sections.findIndex(s => s.id === section.id);
    if (index === -1) {
      throw new Error(`A Section with id ${section.id} doesn't exist`);
    }

    this._sections.splice(index, 1);
  }

  /**
   * Moves a Section object from the Step to a specific index
   *
   * @param {Section} section The Section object to move
   * @param {Number} index The index to move the section to
   * @throws {Error} the specified section doesn't exist in the step or the index is out of bounds
   * @throws {TypeError} when specified section is not a Section or specified index is not a number
   */
  moveSectionToIndex(section, index) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('section must be Section');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._sections.indexOf(section) === -1) {
      throw new Error(`The specified section with id ${section.id} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._sections.length - 1) {
      throw new Error('The specified index is outside the bounds of the sections array');
    }
    this._sections.splice(index, 0, this._sections.splice(this._sections.indexOf(section), 1)[0]);
  }

  /**
   * Adds a Field object to the Step
   *
   * @param {Field} field The Field object to add to the step
   * @throws {Error} A Field with the same name already exists
   * @throws {TypeError} when specified value is not a Field
   */
  addField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }
    if (this.getField(field.name) !== undefined) {
      throw new Error(`A Field with name ${field.name} already exists`);
    }

    this._fields.push(field);
  }

  /**
   * Returns the Field object with the specified name
   *
   * @param {string} fieldName The name of the Field to retrieve
   * @return {Field} The Field with the specified name.
   *                undefined if a field with the specified name doesn't exist.
   */
  getField(fieldName) {
    return this._fields.find(f => f.name === fieldName);
  }

  /**
   * Update a Field object from the Step
   *
   * @param {Field} field The Field object to update
   * @throws {Error} A Field with the given name doesn't exist in the step
   * @throws {TypeError} when specified value is not a Field
   */
  updateField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields[index] = field;
  }

  /**
   * Remove a Field object from the Step
   *
   * @param {Field} field The Field object to remove from the step
   * @throws {Error} A Field with the given name doesn't exist in the step
   * @throws {TypeError} when specified value is not a Field
   */
  removeField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields.splice(index, 1);
  }

  /**
   * Moves a Field object from the Step to a specific index
   *
   * @param {Field} field The Field object to move
   * @param {Number} index The index to move the field to
   * @throws {Error} the specified field doesn't exist in the step or the index is out of bounds
   * @throws {TypeError} when specified field is not a Field or specified index is not a number
   */
  moveFieldToIndex(field, index) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('field must be Field');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._fields.indexOf(field) === -1) {
      throw new Error(`The specified field with name ${field.name} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._fields.length - 1) {
      throw new Error('The specified index is outside the bounds of the fields array');
    }
    this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
  }

  /**
   * Exports the Step as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      id: this._id,
      title: this._title,
      subtitle: this._subtitle,
      body: this._body,
      type: this._type,
      state: this._state && this._state.toJSON(),
      prerequisites: this._prerequisites && this._prerequisites.toJSON(),
      navigationTexts: this._navigationTexts && this._navigationTexts.toJSON(),
      sections: this._sections && this._sections.length > 0 ? this._sections.map(f => f.toJSON()) : null,
      fields: this._fields && this._fields.length > 0 ? this._fields.map(f => f.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a Step instance.
   *
   * @param {Object|string} [data]
   * @return {Step} A step instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.state = StepState.fromJSON(obj.state);
    obj.prerequisites = Prerequisites.fromJSON(obj.prerequisites);
    obj.navigationTexts = NavigationTexts.fromJSON(obj.navigationTexts);

    obj.sections = obj.sections || [];
    obj.sections = obj.sections.map(f => Section.fromJSON(f));

    obj.fields = obj.fields || [];
    obj.fields = obj.fields.map(f => Field.fromJSON(f));

    let instance = new Step(obj.id);

    delete obj.id;
    Object.assign(instance, obj);

    return instance;
  }

}
