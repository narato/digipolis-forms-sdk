import Field from './field.js';
import FormInfo from './form-info.js';
import NavigationTexts from './navigation-texts.js';
import Section from './section.js';
import Step from './step.js';
import TypeCheck from './typecheck.js';
import Validator from './validator.js';
import ValidatorFactory from './validator-factory.js';

/**
 * Represents a form based on a JSON template
 */
export default class Form {

  /**
   * Creates a new instance of Form.
   * Use {@link FormSdk.createForm} to create a new Form.
   * @private
   */
  constructor() {
    this._name;
    this._formId;
    this._rendererVersion;
    this._info = new FormInfo();
    this._canSaveDraft;
    this._saveOnNavigate;
    this._navigationTexts = new NavigationTexts();
    this._steps = [];
    this._sections = [];
    this._fields = [];
    this._validators = [];
  }

  /**
   * Sets the specified title in the info object
   * which can be accessed using form.info.title
   *
   * @param {string} title The value to use as title
   */
  setTitle(title) {
    this.info.title = title;
  }

  /**
   * Sets the specified body in the info object
   * which can be accessed using form.info.body
   *
   * @param {string} body The value to use as body
   */
  setBody(body) {
    this.info.body = body;
  }

  /**
   * Gets the entire collection of Step objects
   *
   * @type {Step[]}
   */
  get steps() {
    return this._steps;
  }

  /**
   * Sets the entire collection of Step objects,
   * overriding the existing collection.
   *
   * @type {Step[]}
   *
   * @throws {TypeError} when specified value is not an array of Steps
   */
  set steps(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Step; })) {
      throw new TypeError('Value must be an array of Steps');
    }
    this._steps = value;
  }

  /**
   * Gets the entire collection of Section objects
   *
   * @type {Section[]}
   */
  get sections() {
    return this._sections;
  }

  /**
   * Sets the entire collection of Section objects,
   * overriding the existing collection.
   *
   * @type {Section[]}
   *
   * @throws {TypeError} when specified value is not an array of Sections
   */
  set sections(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Section; })) {
      throw new TypeError('Value must be an array of Sections');
    }
    this._sections = value;
  }

  /**
   * Gets the entire collection of Field objects
   *
   * @type {Field[]}
   */
  get fields() {
    return this._fields;
  }

  /**
   * Sets the entire collection of Field objects,
   * overriding the existing collection.
   *
   * @type {Field[]}
   *
   * @throws {TypeError} when specified value is not an array of Fields
   */
  set fields(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Field; })) {
      throw new TypeError('Value must be an array of Fields');
    }
    this._fields = value;
  }

  /**
   * Gets the entire collection of Validator objects
   *
   * @type {Validator[]}
   */
  get validators() {
    return this._validators;
  }

  /**
   * Sets the entire collection of Validator objects,
   * overriding the existing collection.
   *
   * @type {Validator[]}
   *
   * @throws {TypeError} when specified value is not an array of Validators
   */
  set validators(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Validator; })) {
      throw new TypeError('Value must be an array of Validators');
    }
    this._validators = value;
  }

  /**
   * Retrieve the name of the form
   *
   * @type {string}
   */
  get name() {
    return this._name;
  }

  /**
   * Set the form name
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set name(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._name = value;
  }

  /**
   * Retrieve the ID of the form
   *
   * @type {string}
   */
  get formId() {
    return this._formId;
  }

  /**
   * Set the form ID
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set formId(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._formId = value;
  }

  /**
   * Retrieve the version of the renderer this form should be rendered with
   *
   * @type {string}
   */
  get rendererVersion() {
    return this._rendererVersion;
  }

  /**
   * Set the form renderer version
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set rendererVersion(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._rendererVersion = value;
  }

  /**
   * Retrieve the form info
   *
   * @type {FormInfo}
   */
  get info() {
    return this._info;
  }

  /**
   * Set the form info
   *
   * @type {FormInfo}
   *
   * @throws {TypeError} when specified value is not a FormInfo instance
   */
  set info(value) {
    if (value && !(value instanceof FormInfo)) {
      throw new TypeError('Value must be a FormInfo object');
    }
    this._info = value;
  }

  /**
   * Retrieve the canSaveDraft flag
   *
   * @type {boolean}
   */
  get canSaveDraft() {
    return this._canSaveDraft;
  }

  /**
   * Sets the specified canSaveDraft flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set canSaveDraft(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._canSaveDraft = value;
  }

  /**
   * Retrieve the saveOnNavigate flag
   *
   * @type {boolean}
   */
  get saveOnNavigate() {
    return this._saveOnNavigate;
  }

  /**
   * Sets the specified saveOnNavigate flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set saveOnNavigate(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._saveOnNavigate = value;
  }

  /**
   * Retrieve the navigation texts
   *
   * @type {NavigationTexts}
   */
  get navigationTexts() {
    return this._navigationTexts;
  }

  /**
   * Sets the navigation texts
   *
   * @type {NavigationTexts}
   *
   * @throws {TypeError} when specified value is not a NavigationTexts object
   */
  set navigationTexts(value) {
    if (value && !(value instanceof NavigationTexts)) {
      throw new TypeError('Value must be a NavigationTexts object');
    }
    this._navigationTexts = value;
  }

  /**
   * Adds a Step object to the Form
   *
   * @param {Step} step The Step object to add to the form
   * @throws {Error} A Step with the same ID already exists
   * @throws {TypeError} when specified value is not a Step
   */
  addStep(step) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('Value must be Step');
    }
    if (this.getStep(step.id) !== undefined) {
      throw new Error(`A Step with id ${step.id} already exists`);
    }

    this._steps.push(step);
  }

  /**
   * Returns the Step object with the specified ID
   *
   * @param {string} stepId The ID of the Step to retrieve
   * @return {Step} The Step with the specified ID.
   *                undefined if a step with the specified ID doesn't exist.
   */
  getStep(stepId) {
    return this._steps.find(s => s.id === stepId);
  }

  /**
   * Update a Step object from the Form
   *
   * @param {Step} step The Step object to update
   * @throws {Error} A Step with the given ID doesn't exist in the form
   * @throws {TypeError} when specified value is not a Step
   */
  updateStep(step) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('Value must be Step');
    }

    const index = this._steps.findIndex(s => s.id === step.id);
    if (index === -1) {
      throw new Error(`A Step with id ${step.id} doesn't exist`);
    }

    this._steps[index] = step;
  }

  /**
   * Remove a Step object from the Form
   *
   * @param {Step} step The Step object to remove from the form
   * @throws {Error} A Step with the given ID doesn't exist in the form
   * @throws {TypeError} when specified value is not a Step
   */
  removeStep(step) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('Value must be Step');
    }

    const index = this._steps.findIndex(s => s.id === step.id);
    if (index === -1) {
      throw new Error(`A Step with id ${step.id} doesn't exist`);
    }

    this._steps.splice(index, 1);
  }

  /**
   * Moves a Step object from the Form to a specific index
   *
   * @param {Step} step The Step object to move
   * @param {Number} index The index to move the step to
   * @throws {Error} the specified step doesn't exist in the form or the index is out of bounds
   * @throws {TypeError} when specified step is not a Step or specified index is not a number
   */
  moveStepToIndex(step, index) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('step must be Step');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._steps.indexOf(step) === -1) {
      throw new Error(`The specified step with id ${step.id} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._steps.length - 1) {
      throw new Error('The specified index is outside the bounds of the steps array');
    }
    this._steps.splice(index, 0, this._steps.splice(this._steps.indexOf(step), 1)[0]);
  }

  /**
   * Adds a Section object to the Form
   *
   * @param {Section} section The Section object to add to the form
   * @throws {Error} A Section with the same ID already exists
   * @throws {TypeError} when specified value is not a Section
   */
  addSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }
    if (this.getSection(section.id) !== undefined) {
      throw new Error(`A Section with id ${section.id} already exists`);
    }

    this._sections.push(section);
  }

  /**
   * Returns the Section object with the specified ID
   *
   * @param {string} sectionId The ID of the Section to retrieve
   * @return {Section} The Section with the specified ID.
   *                undefined if a section with the specified ID doesn't exist.
   */
  getSection(sectionId) {
    return this._sections.find(s => s.id === sectionId);
  }

  /**
   * Update a Section object from the Form
   *
   * @param {Section} section The Section object to update
   * @throws {Error} A Section with the given ID doesn't exist in the form
   * @throws {TypeError} when specified value is not a Section
   */
  updateSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }

    const index = this._sections.findIndex(s => s.id === section.id);
    if (index === -1) {
      throw new Error(`A Section with id ${section.id} doesn't exist`);
    }

    this._sections[index] = section;
  }

  /**
   * Remove a Section object from the Form
   *
   * @param {Section} section The Section object to remove from the form
   * @throws {Error} A Section with the given ID doesn't exist in the form
   * @throws {TypeError} when specified value is not a Section
   */
  removeSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }

    const index = this._sections.findIndex(s => s.id === section.id);
    if (index === -1) {
      throw new Error(`A Section with id ${section.id} doesn't exist`);
    }

    this._sections.splice(index, 1);
  }

  /**
   * Moves a Section object from the Form to a specific index
   *
   * @param {Section} section The Section object to move
   * @param {Number} index The index to move the section to
   * @throws {Error} the specified section doesn't exist in the form or the index is out of bounds
   * @throws {TypeError} when specified section is not a Section or specified index is not a number
   */
  moveSectionToIndex(section, index) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('section must be Section');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._sections.indexOf(section) === -1) {
      throw new Error(`The specified section with id ${section.id} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._sections.length - 1) {
      throw new Error('The specified index is outside the bounds of the sections array');
    }
    this._sections.splice(index, 0, this._sections.splice(this._sections.indexOf(section), 1)[0]);
  }

  /**
   * Adds a Field object to the Form
   *
   * @param {Field} field The Field object to add to the form
   * @throws {Error} A Field with the same name already exists
   * @throws {TypeError} when specified value is not a Field
   */
  addField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }
    if (this.getField(field.name) !== undefined) {
      throw new Error(`A Field with name ${field.name} already exists`);
    }

    this._fields.push(field);
  }

  /**
   * Returns the Field object with the specified name
   *
   * @param {string} fieldName The name of the Field to retrieve
   * @return {Field} The Field with the specified name.
   *                undefined if a field with the specified name doesn't exist.
   */
  getField(fieldName) {
    return this._fields.find(f => f.name === fieldName);
  }

  /**
   * Update a Field object from the Form
   *
   * @param {Field} field The Field object to update
   * @throws {Error} A Field with the given name doesn't exist in the form
   * @throws {TypeError} when specified value is not a Field
   */
  updateField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields[index] = field;
  }

  /**
   * Remove a Field object from the Form
   *
   * @param {Field} field The Field object to remove from the form
   * @throws {Error} A Field with the given name doesn't exist in the form
   * @throws {TypeError} when specified value is not a Field
   */
  removeField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('field must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields.splice(index, 1);
  }

  /**
   * Moves a Field object from the Form to a specific index
   *
   * @param {Field} field The Field object to move
   * @param {Number} index The index to move the field to
   * @throws {Error} the specified field doesn't exist in the form or the index is out of bounds
   * @throws {TypeError} when specified field is not a Field or specified index is not a number
   */
  moveFieldToIndex(field, index) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('field must be Field');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._fields.indexOf(field) === -1) {
      throw new Error(`The specified field with name ${field.name} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._fields.length - 1) {
      throw new Error('The specified index is outside the bounds of the fields array');
    }
    this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
  }

  /**
   * Adds a Validator object to the Form
   *
   * @param {Validator} validator The Validator object to add to the Form
   * @throws {Error} A Validator with the same name already exists
   * @throws {TypeError} when specified value is not a Validator
   */
  addValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }
    if (this.getValidator(validator.name) !== undefined) {
      throw new Error(`A Validator with name ${validator.name} already exists`);
    }

    this._validators.push(validator);
  }

  /**
   * Returns the Validator object with the specified name
   *
   * @param {string} validatorName The name of the Validator to retrieve
   * @return {Validator} The Validator with the specified name.
   *                undefined if a validator with the specified name doesn't exist.
   */
  getValidator(validatorName) {
    return this._validators.find(v => v.name === validatorName);
  }

  /**
   * Update a Validator object from the Form
   *
   * @param {Validator} validator The Validator object to update
   * @throws {Error} A Validator with the given name doesn't exist in the Form
   * @throws {TypeError} when specified value is not a Validator
   */
  updateValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }

    const index = this._validators.findIndex(v => v.name === validator.name);
    if (index === -1) {
      throw new Error(`A Validator with name ${validator.name} doesn't exist`);
    }

    this._validators[index] = validator;
  }

  /**
   * Remove a Validator object from the Form
   *
   * @param {Validator} validator The Validator object to remove from the Form
   * @throws {Error} A Validator with the given name doesn't exist in the Form
   * @throws {TypeError} when specified value is not a Validator
   */
  removeValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }

    const index = this._validators.findIndex(v => v.name === validator.name);
    if (index === -1) {
      throw new Error(`A Validator with name ${validator.name} doesn't exist`);
    }

    this._validators.splice(index, 1);
  }

  /**
   * Exports the Form as a plain Javascript Object
   *
   * @return {Object} An Object describing the Form template
   *
   */
  toJSON() {
    let obj = {
      name: this._name,
      formId: this._formId,
      rendererVersion: this._rendererVersion,
      info: this._info && this._info.toJSON(),
      canSaveDraft: this._canSaveDraft,
      saveOnNavigate: this._saveOnNavigate,
      navigationTexts: this._navigationTexts && this._navigationTexts.toJSON(),
      steps: this._steps && this._steps.length > 0 ? this._steps.map(s => s.toJSON()) : null,
      sections: this._sections && this._sections.length > 0 ? this._sections.map(s => s.toJSON()) : null,
      fields: this._fields && this._fields.length > 0 ? this._fields.map(f => f.toJSON()) : null,
      validators: this._validators && this._validators.length > 0 ? this._validators.map(v => v.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Create a Form instance from a JSON string
   *
   * @param {String|Object} [data] A JSON string or Object literal describing the Form template
   * @return {Form} A Form instance containing the data from the JSON string
   *
   */
  static fromJSON(data) {
    let obj = data || {};

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.info = FormInfo.fromJSON(obj.info);
    obj.navigationTexts = NavigationTexts.fromJSON(obj.navigationTexts);

    obj.steps = obj.steps || [];
    obj.steps = obj.steps && obj.steps.map(s => Step.fromJSON(s));

    obj.sections = obj.sections || [];
    obj.sections = obj.sections.map(s => Section.fromJSON(s));

    obj.fields = obj.fields || [];
    obj.fields = obj.fields.map(f => Field.fromJSON(f));

    obj.validators = obj.validators || [];
    obj.validators = obj.validators.map(v => ValidatorFactory.createValidatorFromJSON(v));

    let instance = new Form();

    Object.assign(instance, obj);

    return instance;
  }
}
