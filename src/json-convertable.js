import TypeCheck from './typecheck.js';

/**
 * A base class that provides functionality to easily convert
 * Form template objects to JSON and back.
 */
export default class JsonConvertable {

  /**
   * Convert the specified JSON to the inheritor's object type.
   *
   * @param {Object|string} [data]
   * @return {Object} An object instance of the type that extends this class
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    let instance = new this(obj.id);

    delete obj.id;
    Object.assign(instance, obj);

    return instance;
  }
}
