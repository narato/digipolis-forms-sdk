import JsonConvertable from './json-convertable.js';
import Section from './section.js';
import TypeCheck from './typecheck.js';

/**
 * An object representing prerequisites defined on Sections
 */
export default class SectionsPrerequisite extends JsonConvertable {

  /**
   * Creates a new SectionsPrerequisite instance
   */
  constructor() {
    super();

    this._logical;
    this._sections = [];
  }

  /**
   * Retrieve the prerequisite combination logic
   *
   * @type {string}
   */
  get logical() {
    return this._logical;
  }

  /**
   * Set the prerequisite comibination logic
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set logical(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._logical = value;
  }

  /**
   * Gets the entire collection of Section ids
   *
   * @type {string[]}
   */
  get sections() {
    return this._sections;
  }

  /**
   * Sets the entire collection of Section ids,
   * overriding the existing collection.
   *
   * @type {string[]}
   *
   * @throws {TypeError} when specified value is not an array of Strings
   */
  set sections(value) {
    if (value && !TypeCheck.isArrayOf(value, TypeCheck.isString)) {
      throw new TypeError('Value must be an array of Strings');
    }
    this._sections = value;
  }

  /**
   * Adds the ID of a Section to the prerequisites completion list
   *
   * @param {!Section} section The Section to add to the completion list
   * @throws {TypeError} when specified value is not a Section
   */
  addSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }
    this.addSectionId(section.id);
  }

  /**
   * Adds a section ID to the prerequisites completion list
   *
   * @param {!string} sectionId The Section ID to add to the completion list
   * @throws {Error} The sectionId is already added to the list
   * @throws {TypeError} when specified value is not a String
   */
  addSectionId(sectionId) {
    if (!TypeCheck.isString(sectionId)) {
      throw new TypeError('Value must be String');
    }
    if (this._sections.indexOf(sectionId) !== -1) {
      throw new Error(`Section ID ${sectionId} is already added`);
    }

    this._sections.push(sectionId);
  }

  /**
   * Remove a Section from the prerequisites completion list
   *
   * @param {!Section} section The Section object to remove from the completion list
   * @throws {TypeError} when specified value is not a Section
   */
  removeSection(section) {
    if (section && !(section instanceof Section)) {
      throw new TypeError('Value must be Section');
    }
    this.removeSectionId(section.id);
  }

  /**
   * Remove a Section ID from the prerequisites completion list
   *
   * @param {!string} sectionId The Section ID to remove from the completion list
   * @throws {Error} A Section with the given ID doesn't exist in the list
   * @throws {TypeError} when specified value is not a string
   */
  removeSectionId(sectionId) {
    if (!TypeCheck.isString(sectionId)) {
      throw new TypeError('Value must be String');
    }

    const index = this._sections.findIndex(s => s === sectionId);
    if (index === -1) {
      throw new Error(`A Section with id ${sectionId} doesn't exist`);
    }

    this._sections.splice(index, 1);
  }

  /**
   * Exports the SectionsPrerequisite as a plain Javascript Object
   *
   * @return {Object} An Object describing the Sections Prerequisite
   *
   */
  toJSON() {
    let obj = {
      logical: this._logical,
      sections: this._sections && this._sections.length > 0 ? this._sections : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
