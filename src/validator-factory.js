import TypeCheck from './typecheck.js';
import Validator from './validator.js';
import AdvancedValidator from './advanced-validator.js';

/**
 * Factory class to create the correct validator type
 */
export default class ValidatorFactory {

  /**
   * Convert the specified JSON to a Validator or AdvancedValidator instance.
   *
   * @param {Object|string} [data]
   * @return {Validator|AdvancedValidator} A validator instance containing the specified data
   */
  static createValidatorFromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    if (obj.hasOwnProperty('options') && obj.options != null) {
      return AdvancedValidator.fromJSON(obj);
    }
    return Validator.fromJSON(obj);
  }

}
