import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A object containing an operand and value to validate fields
 */
export default class FieldValueOperand extends JsonConvertable {
  /**
   * Creates a new FieldValueOperand instance
   */
  constructor() {
    super();

    this._name;
    this._value;
    this._operator;
  }

  /**
   * Retrieve the field name to check
   *
   * @type {string}
   */
  get name() {
    return this._name;
  }

  /**
   * Set the field name to check
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set name(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._name = value;
  }

  /**
   * Retrieve the value to check for
   *
   * @type {string}
   */
  get value() {
    return this._value;
  }

  /**
   * Set the value content to check for
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set value(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._value = value;
  }

  /**
   * Retrieve the operator to check for
   *
   * @type {string}
   */
  get operator() {
    return this._operator;
  }

  /**
   * Set the operator content to check for
   *
   * @type {string}
   *
   * @throws {TypeError} when specified operator is not a string
   */
  set operator(operator) {
    if (operator && !TypeCheck.isString(operator)) {
      throw new TypeError('Operator must be a string');
    }
    this._operator = operator;
  }

  /**
   * Exports the FieldValueOperand as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      name: this._name,
      value: this._value,
      operator: this._operator
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }
}
