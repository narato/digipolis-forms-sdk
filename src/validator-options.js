import JsonConvertable from './json-convertable.js';

/**
 * A ValidatorOptions object containing options for a validator.
 * This is a very generic object, and basically just a wrapper around Object
 */
export default class ValidatorOptions extends JsonConvertable {
  /**
   * Creates a new ValidatorOptions instance
   */
  constructor() {
    super();
  }

  /**
   * Exports the ValidatorOptions as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
    };

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
