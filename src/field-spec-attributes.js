import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A small object containing attributes of a Field specification
 */
export default class FieldSpecAttributes extends JsonConvertable {
  /**
   * Creates a new FieldSpecAttributes instance
   */
  constructor() {
    super();

    this._type;
    this._value;
  }

  /**
   * Retrieve the field type
   *
   * @type {string}
   */
  get type() {
    return this._type;
  }

  /**
   * Set the field type
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set type(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._type = value;
  }

  /**
   * Retrieve the prefilled field value
   *
   * @type {string}
   */
  get value() {
    return this._value;
  }

  /**
   * Set the prefilled field value
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set value(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._value = value;
  }

  /**
   * Exports the FieldSpecAttributes as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      type: this._type,
      value: this._value
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
