import Field from './field.js';
import JsonConvertable from './json-convertable.js';
import Prerequisites from './prerequisites.js';
import SectionState from './section-state.js';
import TypeCheck from './typecheck.js';

/**
 * Defines a Section for use in a Form object
 */
export default class Section extends JsonConvertable {
  /**
   * Creates an instance of Section.
   *
   * @param {string} [id] The ID of the Section instance
   *
   * @throws {Error} when specified value is null
   * @throws {TypeError} when specified value is not a string
   */
  constructor(id) {
    super();

    if (id == null) {
      throw new Error('Section must have an id');
    }
    if (id && !TypeCheck.isString(id)) {
      throw new TypeError('Section id must be a string');
    }

    this._id = id;
    this._title;
    this._subtitle;
    this._state = new SectionState();
    this._prerequisites = new Prerequisites();
    this._fields = [];
  }

  /**
   * Gets the ID
   *
   * @type {string}
   */
  get id() {
    return this._id;
  }

  /**
   * Gets the subtitle content
   *
   * @type {string}
   */
  get subtitle() {
    return this._subtitle;
  }

  /**
   * Sets the subtitle content
   *
   * @type {string}
   */
  set subtitle(value) {
    this._subtitle = value;
  }

  /**
   * Gets the title content
   *
   * @type {string}
   */
  get title() {
    return this._title;
  }

  /**
   * Sets the title content
   *
   * @type {string}
   */
  set title(value) {
    this._title = value;
  }

  /**
   * Gets the state
   *
   * @type {SectionState}
   */
  get state() {
    return this._state;
  }

  /**
   * Sets the state
   *
   * @type {SectionState}
   *
   * @throws {TypeError} when specified value is not a SectionState object
   */
  set state(value) {
    if (value && !(value instanceof SectionState)) {
      throw new TypeError('Value must be a SectionState object');
    }
    this._state = value;
  }

  /**
   * Gets the prerequisites
   *
   * @type {Prerequisites}
   */
  get prerequisites() {
    return this._prerequisites;
  }

  /**
   * Sets the prerequisites
   *
   * @type {Prerequisites}
   *
   * @throws {TypeError} when specified value is not a Prerequisites object
   */
  set prerequisites(value) {
    if (value && !(value instanceof Prerequisites)) {
      throw new TypeError('Value must be a Prerequisites object');
    }
    this._prerequisites = value;
  }

  /**
   * Gets the entire collection of Field objects
   *
   * @type {Field[]}
   */
  get fields() {
    return this._fields;
  }

  /**
   * Sets the entire collection of Field objects,
   * overriding the existing collection.
   *
   * @type {Field[]}
   *
   * @throws {TypeError} when specified value is not an array of Fields
   */
  set fields(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Field; })) {
      throw new TypeError('Value must be an array of Fields');
    }
    this._fields = value;
  }

  /**
   * Adds a Field object to the Section
   *
   * @param {Field} field The Field object to add to the section
   * @throws {Error} A Field with the same name already exists
   * @throws {TypeError} when specified value is not a Field
   */
  addField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }
    if (this.getField(field.name) !== undefined) {
      throw new Error(`A Field with name ${field.name} already exists`);
    }

    this._fields.push(field);
  }

  /**
   * Returns the Field object with the specified name
   *
   * @param {string} fieldName The name of the Field to retrieve
   * @return {Field} The Field with the specified name.
   *                undefined if a field with the specified name doesn't exist.
   */
  getField(fieldName) {
    return this._fields.find(f => f.name === fieldName);
  }

  /**
   * Update a Field object from the Section
   *
   * @param {Field} field The Field object to update
   * @throws {Error} A Field with the given name doesn't exist in the section
   * @throws {TypeError} when specified value is not a Field
   */
  updateField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields[index] = field;
  }

  /**
   * Remove a Field object from the Section
   *
   * @param {Field} field The Field object to remove from the section
   * @throws {Error} A Field with the given name doesn't exist in the section
   * @throws {TypeError} when specified value is not a Field
   */
  removeField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }

    const index = this._fields.findIndex(f => f.name === field.name);
    if (index === -1) {
      throw new Error(`A Field with name ${field.name} doesn't exist`);
    }

    this._fields.splice(index, 1);
  }

  /**
   * Moves a Field object from the section to a specific index
   *
   * @param {Field} field The Field object to move
   * @param {Number} index The index to move the field to
   * @throws {Error} the specified field doesn't exist in the section or the index is out of bounds
   * @throws {TypeError} when specified field is not a Field or specified index is not a number
   */
  moveFieldToIndex(field, index) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('field must be Field');
    }

    if (index && !TypeCheck.isNumber(index)) {
      throw new TypeError('index must be a number');
    }

    if (this._fields.indexOf(field) === -1) {
      throw new Error(`The specified field with name ${field.name} doesn't exist`);
    }

    if (index === undefined || index < 0 || index >= this._fields.length - 1) {
      throw new Error('The specified index is outside the bounds of the fields array');
    }
    this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
  }

  /**
   * Convert the Section to a plain Javascript object.
   *
   * @return {Object}
   */
  toJSON() {
    let obj = {
      id: this._id,
      title: this._title,
      subtitle: this._subtitle,
      state: this._state && this._state.toJSON(),
      prerequisites: this._prerequisites && this._prerequisites.toJSON(),
      fields: this._fields && this._fields.length > 0 ? this._fields.map(f => f.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a Section instance.
   *
   * @param {Object|string} [data]
   * @return {Section} A section instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.state = SectionState.fromJSON(obj.state);
    obj.prerequisites = Prerequisites.fromJSON(obj.prerequisites);

    obj.fields = obj.fields || [];
    obj.fields = obj.fields.map(f => Field.fromJSON(f));

    let instance = new Section(obj.id);

    delete obj.id;
    Object.assign(instance, obj);

    return instance;
  }

}
