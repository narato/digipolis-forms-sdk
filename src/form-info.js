import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A small info object containing some meta fields
 * for informational purposes.
 */
export default class FormInfo extends JsonConvertable {
  /**
   * Creates a new FormInfo instance
   */
  constructor() {
    super();

    this._title;
    this._body;
  }

  /**
   * Retrieve the body content
   *
   * @type {string}
   */
  get body() {
    return this._body;
  }

  /**
   * Set the body content
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set body(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._body = value;
  }

  /**
   * Retrieve the title content
   *
   * @type {string}
   */
  get title() {
    return this._title;
  }

  /**
   * Set the title content
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set title(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._title = value;
  }

  /**
   * Exports the FormInfo as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      title: this._title,
      body: this._body
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
