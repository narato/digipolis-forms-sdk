'use strict';

import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * Contains key and value pairs as value options for Fields
 */
export default class FieldSpecOptionsValue extends JsonConvertable {
  /**
   * Creates a new FieldSpecOptionsValue instance
   */
  constructor() {
    super();

    this._key;
    this._value;
  }

  /**
   * Retrieve the key of the value
   *
   * @type {string}
   */
  get key() {
    return this._key;
  }

  /**
   * Set the key of the value
   *
   * @type {string}
   *
   * @throws {TypeError} when specified key is not a string
   */
  set key(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._key = value;
  }

  /**
   * Retrieve the content of the value
   *
   * @type {string}
   */
  get value() {
    return this._value;
  }

  /**
   * Set the content of the value
   *
   * @type {string}
   *
   * @throws {TypeError} when specified content is not a string
   */
  set value(val) {
    if (val && !TypeCheck.isString(val)) {
      throw new TypeError('Value must be a string');
    }
    this._value = val;
  }

  /**
   * Exports the FieldSpecOptionsValue as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      key: this.key,
      value: this.value
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }
}

