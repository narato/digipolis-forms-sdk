import FieldSpecOptionsLayout from './field-spec-options-layout.js';
import FieldSpecOptionsValue from './field-spec-options-value.js';
import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A small object containing options of a Field specification
 */
export default class FieldSpecOptions extends JsonConvertable {
  /**
   * Creates a new FieldSpecOptions instance
   */
  constructor() {
    super();

    this._layout = new FieldSpecOptionsLayout();
    this._valueOptions = [];
  }

  /**
   * Retrieve the field layout options
   *
   * @type {FieldSpecOptionsLayout}
   */
  get layout() {
    return this._layout;
  }

  /**
   * Set the field layout options
   *
   * @type {FieldSpecOptionsLayout}
   *
   * @throws {TypeError} when specified layout is not a FieldSpecOptionsLayout instance
   */
  set layout(value) {
    if (value && !(value instanceof FieldSpecOptionsLayout)) {
      throw new TypeError('Value must be a FieldSpecOptionsLayout');
    }
    this._layout = value;
  }

  /**
   * Retrieves the collection of value options
   *
   * @type {FieldSpecOptionsValue[]}
   */
  get valueOptions() {
    return this._valueOptions;
  }

  /**
   * Sets the value options collection
   *
   * @type {FieldSpecOptionsValue[]}
   */
  set valueOptions(value) {
    this._valueOptions = value;
  }

  /**
   * Adds the specified value option
   *
   * @param {FieldSpecOptionsValue} [opt] The value option to add
   *
   * @throws {Error} when the a value options already exists with the same key
   * @throws {TypeError} when the specified value is not a FieldSpecOptionsValue instance
   */
  addValueOption(opt) {
    if (opt && !(opt instanceof FieldSpecOptionsValue)) {
      throw new TypeError('Value must be FieldSpecOptionsValue');
    }

    if (this.getValueOption(opt.key) !== undefined) {
      throw new Error(`A value option with key ${opt.key} already exists`);
    }

    this._valueOptions.push(opt);
  }

  /**
   * Returns the value options with the specified key
   *
   * @param {string} [key] The key of the value options to retrieve
   * @return {FieldSpecOptionsValue} The value options with the specified key.
   *                                 undefined if no value options exists with the specified key.
   */
  getValueOption(key) {
    return this._valueOptions.find(v => v.key === key);
  }

  /**
   * Remove the value option with the specified key
   *
   * @param {string} [key] The key of the value option to remove
   * @throws {Error} No value option with the specified key exists
   */
  removeValueOption(key) {
    const index = this.valueOptions.findIndex(v => v.key === key);

    if (index === -1) {
      throw new Error(`No value option with key ${key} to remove`);
    }

    this.valueOptions.splice(index, 1);
  }

  /**
   * Sets the specified layout in the field specification object
   * which can be accessed using options.layout.fieldLayout
   *
   * @param {string} value Layout of the field
   */
  setFieldLayout(value) {
    this._layout.fieldLayout = value;
  }

  /**
   * Sets the specified layout classes in the field specification object
   * which can be accessed using options.layout.fieldClass
   *
   * @param {string} value CSS classes to add to the field's class attribute
   */
  setFieldClass(value) {
    this._layout.fieldClass = value;
  }

  /**
   * Exports the FieldSpecOptions as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      layout: this._layout && this._layout.toJSON(),
      valueOptions: this._valueOptions && this._valueOptions.length > 0 ? this._valueOptions.map(v => v.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a FieldSpecOptions instance.
   *
   * @param {Object|string} [data]
   * @return {FieldSpecOptions} A fieldSpecOptions instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.layout = FieldSpecOptionsLayout.fromJSON(obj.layout);

    obj.valueOptions = obj.valueOptions || [];
    obj.valueOptions = obj.valueOptions.map(v => FieldSpecOptionsValue.fromJSON(v));

    let instance = new FieldSpecOptions();

    Object.assign(instance, obj);

    return instance;
  }

}
