import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
* An object containing the labels for navigation buttons
*
*/
export default class NavigationTexts extends JsonConvertable {

  /**
   * Creates a new NavigationTexts instance
   */
  constructor() {
    super();

    this._submit;
    this._next;
    this._previous;
    this._concept;
  }

  /**
   * Retrieve the submit label
   *
   * @type {string}
   */
  get submit() {
    return this._submit;
  }

  /**
   * Set the submit label
   *
   * @type {?string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set submit(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._submit = value;
  }

  /**
   * Retrieve the next label
   *
   * @type {string}
   */
  get next() {
    return this._next;
  }

  /**
   * Set the next label
   *
   * @type {?string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set next(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._next = value;
  }

  /**
   * Retrieve the previous label
   *
   * @type {string}
   */
  get previous() {
    return this._previous;
  }

  /**
   * Set the previous label
   *
   * @type {?string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set previous(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._previous = value;
  }

  /**
   * Retrieve the concept label
   *
   * @type {string}
   */
  get concept() {
    return this._concept;
  }

  /**
   * Set the concept label
   *
   * @type {?string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set concept(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._concept = value;
  }

  /**
   * Exports the NavigationTexts as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      submit: this._submit,
      next: this._next,
      previous: this._previous,
      concept: this._concept
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
