/**
 * Helper class to assist in verifying the type of certain objects
 */
export default class TypeCheck {
  /**
   * Determines if an object is a string
   *
   * @param {any} [obj] The variable to verify
   * @return {Boolean} True if the object is a string, false otherwise
   *
   */
  static isString(obj) {
    return typeof (obj) === 'string' || obj instanceof String;
  }

  /**
   * Determines if an object is a boolean
   *
   * @param {any} [obj] The variable to verify
   * @return {Boolean} True if the object is a boolean, false otherwise
   *
   */
  static isBoolean(obj) {
    return typeof (obj) === 'boolean' || obj instanceof Boolean;
  }

  /**
   * Determines if an object is a number
   *
   * @param {any} [obj] The variable to verify
   * @return {Boolean} True if the object is a number, false otherwise
   *
   */
  static isNumber(obj) {
    return typeof (obj) === 'number' || obj instanceof Number;
  }

  /**
   * Determines if an object is an array
   *
   * @param {any} [obj] The variable to verify
   * @return {Boolean} True if the object is an array, false otherwise
   *
   */
  static isArray(obj) {
    return obj instanceof Array;
  }

  /**
   * Determines if an object is an array
   * and if the elements adhere to the specified function
   *
   * @param {any} [obj] The variable to verify
   * @param {Function} callback Function to test for each element
   * @return {Boolean} True if the object is an array
   *    and the elements adhere to the specified function, false otherwise
   *
   */
  static isArrayOf(obj, callback) {
    return TypeCheck.isArray(obj) && obj.every((i) => callback(i));
  }
}
