import JsonConvertable from './json-convertable.js';
import Field from './field.js';
import TypeCheck from './typecheck.js';

/**
 * An object representing prerequisites defined on Fields
 */
export default class FieldsPrerequisite extends JsonConvertable {

  /**
   * Creates a new FieldsPrerequisite instance
   */
  constructor() {
    super();

    this._logical;
    this._fields = [];
  }

  /**
   * Retrieve the prerequisite combination logic
   *
   * @type {string}
   */
  get logical() {
    return this._logical;
  }

  /**
   * Set the prerequisite comibination logic
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set logical(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._logical = value;
  }

  /**
   * Gets the entire collection of Field names
   *
   * @type {string[]}
   */
  get fields() {
    return this._fields;
  }

  /**
   * Sets the entire collection of Field names,
   * overriding the existing collection.
   *
   * @type {string[]}
   *
   * @throws {TypeError} when specified value is not an array of Strings
   */
  set fields(value) {
    if (value && !TypeCheck.isArrayOf(value, TypeCheck.isString)) {
      throw new TypeError('Value must be an array of Strings');
    }
    this._fields = value;
  }

  /**
   * Adds the name of a Field to the prerequisites completion list
   *
   * @param {!Field} field The Field to add to the completion list
   * @throws {TypeError} when specified value is not a Field
   */
  addField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }
    this.addFieldId(field.name);
  }

  /**
   * Adds a field name to the prerequisites completion list
   *
   * @param {!string} fieldName The Field name to add to the completion list
   * @throws {Error} The fieldName is already added to the list
   * @throws {TypeError} when specified value is not a String
   */
  addFieldId(fieldName) {
    if (!TypeCheck.isString(fieldName)) {
      throw new TypeError('Value must be String');
    }
    if (this._fields.indexOf(fieldName) !== -1) {
      throw new Error(`Field name ${fieldName} is already added`);
    }

    this._fields.push(fieldName);
  }

  /**
   * Remove a Field from the prerequisites completion list
   *
   * @param {!Field} field The Field object to remove from the completion list
   * @throws {TypeError} when specified value is not a Field
   */
  removeField(field) {
    if (field && !(field instanceof Field)) {
      throw new TypeError('Value must be Field');
    }
    this.removeFieldId(field.name);
  }

  /**
   * Remove a Field name from the prerequisites completion list
   *
   * @param {!string} fieldName The Field name to remove from the completion list
   * @throws {Error} A Field with the given name doesn't exist in the list
   * @throws {TypeError} when specified value is not a string
   */
  removeFieldId(fieldName) {
    if (!TypeCheck.isString(fieldName)) {
      throw new TypeError('Value must be String');
    }

    const index = this._fields.findIndex(s => s === fieldName);
    if (index === -1) {
      throw new Error(`A Field with id ${fieldName} doesn't exist`);
    }

    this._fields.splice(index, 1);
  }

  /**
   * Exports the FieldsPrerequisite as a plain Javascript Object
   *
   * @return {Object} An Object describing the Fields Prerequisite
   *
   */
  toJSON() {
    let obj = {
      logical: this._logical,
      fields: this._fields && this._fields.length > 0 ? this._fields : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
