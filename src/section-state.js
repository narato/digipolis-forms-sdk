import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A state object containig status fields of a Section
 */
export default class SectionState extends JsonConvertable {
  /**
   * Creates an instance of SectionState.
   */
  constructor() {
    super();

    this._editable;
    this._editMode;
    this._externalUrl;
    this._collapsible;
    this._collapsed;
  }

  /**
   * Retrieve the collapsed flag
   *
   * @type {boolean}
   */
  get collapsed() {
    return this._collapsed;
  }

  /**
   * Set the collapsed flag
   *
   * @type {boolean}
   */
  set collapsed(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }

    this._collapsed = value;
  }

  /**
   * Retrieve the collapsible flag
   *
   * @type {boolean}
   */
  get collapsible() {
    return this._collapsible;
  }

  /**
   * Set the collapsible flag
   *
   * @type {boolean}
   */
  set collapsible(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }

    this._collapsible = value;
  }

  /**
   * Retrieve the editable flag
   *
   * @type {boolean}
   */
  get editable() {
    return this._editable;
  }

  /**
   * Set the editable flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set editable(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }

    this._editable = value;
  }

  /**
   * Retrieve the editMode flag
   *
   * @type {boolean}
   */
  get editMode() {
    return this._editMode;
  }

  /**
   * Set the editMode flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set editMode(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._editMode = value;
  }

  /**
   * Retrieve the externalUrl
   *
   * @type {string}
   */
  get externalUrl() {
    return this._externalUrl;
  }

  /**
   * Set the externalUrl
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set externalUrl(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._externalUrl = value;
  }

  /**
   * Exports the SectionState as a plain Javascript object.
   *
   * @return {Object} A plain Javascript object.
   */
  toJSON() {
    let obj = {
      editable: this.editable,
      editMode: this.editMode,
      externalUrl: this.externalUrl,
      collapsed: this.collapsed,
      collapsible: this.collapsible
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }
}
