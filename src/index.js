'use strict';

/**
 * A module for creating and editing Form templates in JSON format
 *
 */

import AdvancedValidator from './advanced-validator.js';
import Field from './field.js';
import FieldInputFilter from './field-input-filter.js';
import FieldSpec from './field-spec.js';
import FieldSpecAttributes from './field-spec-attributes.js';
import FieldSpecOptions from './field-spec-options.js';
import FieldSpecOptionsLayout from './field-spec-options-layout.js';
import FieldSpecOptionsValue from './field-spec-options-value.js';
import FieldsPrerequisite from './fields-prerequisite.js';
import FieldState from './field-state.js';
import FieldValueOperand from './field-value-operand.js';
import FieldValuesPrerequisite from './field-values-prerequisite.js';
import Form from './form.js';
import FormInfo from './form-info.js';
import JsonConvertable from './json-convertable.js';
import NavigationTexts from './navigation-texts.js';
import Prerequisites from './prerequisites.js';
import Section from './section.js';
import SectionsPrerequisite from './sections-prerequisite.js';
import SectionState from './section-state.js';
import Step from './step.js';
import StepsPrerequisite from './steps-prerequisite.js';
import StepState from './step-state.js';
import TypeCheck from './typecheck.js';
import Validator from './validator.js';
import ValidatorFactory from './validator-factory.js';
import ValidatorOptions from './validator-options.js';

/**
 * Factory class to create SDK objects
 *
 */
export default class ComposerSdk {
  /**
   * Load a Form instance from a JSON template
   *
   * @param {Object|String} [data] An object literal or JSON string
   * @return {Form} A Form instance
   *
   */
  static loadFromJSON(data) {
    let obj = data || {};

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    return Form.fromJSON(obj);
  }

  /**
   * Create a new empty Field instance
   *
   * @param {string} [name] The name for the new Field
   * @return {Field}
   */
  static createField(name) {
    return new Field(name);
  }

  /**
   * Creates a new empty Form instance
   *
   * @return {Form} A Form instance
   *
   */
  static createForm() {
    return new Form();
  }

  /**
   * Creates a new Step which can be added to a Form instance.
   *
   * @param {string} [id] An ID which identifies the Step
   * @return {Step}
   */
  static createStep(id) {
    return new Step(id);
  }

  /**
   * Creates a new Section which can be added to a Form instance.
   *
   * @param {string} [id] An ID which identifies the Section
   * @return {Section}
   */
  static createSection(id) {
    return new Section(id);
  }
}

export {AdvancedValidator as AdvancedValidator};
export {Field as Field};
export {FieldInputFilter as FieldInputFilter};
export {FieldSpec as FieldSpec};
export {FieldSpecAttributes as FieldSpecAttributes};
export {FieldSpecOptions as FieldSpecOptions};
export {FieldSpecOptionsLayout as FieldSpecOptionsLayout};
export {FieldSpecOptionsValue as FieldSpecOptionsValue};
export {FieldsPrerequisite as FieldsPrerequisite};
export {FieldState as FieldState};
export {FieldValueOperand as FieldValueOperand};
export {FieldValuesPrerequisite as FieldValuesPrerequisite};
export {Form as Form};
export {FormInfo as FormInfo};
export {JsonConvertable as JsonConvertable};
export {NavigationTexts as NavigationTexts};
export {Prerequisites as Prerequisites};
export {Section as Section};
export {SectionsPrerequisite as SectionsPrerequisite};
export {SectionState as SectionState};
export {Step as Step};
export {StepsPrerequisite as StepsPrerequisite};
export {StepState as StepState};
export {TypeCheck as TypeCheck};
export {Validator as Validator};
export {ValidatorFactory as ValidatorFactory};
export {ValidatorOptions as ValidatorOptions};
