import JsonConvertable from './json-convertable.js';
import Step from './step.js';
import TypeCheck from './typecheck.js';

/**
 * An object representing prerequisites defined on Steps
 */
export default class StepsPrerequisite extends JsonConvertable {

  /**
   * Creates a new StepsPrerequisite instance
   */
  constructor() {
    super();

    this._logical;
    this._steps = [];
  }

  /**
   * Retrieve the prerequisite combination logic
   *
   * @type {string}
   */
  get logical() {
    return this._logical;
  }

  /**
   * Set the prerequisite comibination logic
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set logical(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._logical = value;
  }

  /**
   * Gets the entire collection of Step ids
   *
   * @type {string[]}
   */
  get steps() {
    return this._steps;
  }

  /**
   * Sets the entire collection of Step ids,
   * overriding the existing collection.
   *
   * @type {string[]}
   *
   * @throws {TypeError} when specified value is not an array of Strings
   */
  set steps(value) {
    if (value && !TypeCheck.isArrayOf(value, TypeCheck.isString)) {
      throw new TypeError('Value must be an array of Strings');
    }
    this._steps = value;
  }

  /**
   * Adds the ID of a Step to the prerequisites completion list
   *
   * @param {!Step} step The Step to add to the completion list
   * @throws {TypeError} when specified value is not a Step
   */
  addStep(step) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('Value must be Step');
    }
    this.addStepId(step.id);
  }

  /**
   * Adds a step ID to the prerequisites completion list
   *
   * @param {!string} stepId The Step ID to add to the completion list
   * @throws {Error} The stepId is already added to the list
   * @throws {TypeError} when specified value is not a String
   */
  addStepId(stepId) {
    if (!TypeCheck.isString(stepId)) {
      throw new TypeError('Value must be String');
    }
    if (this._steps.indexOf(stepId) !== -1) {
      throw new Error(`Step ID ${stepId} is already added`);
    }

    this._steps.push(stepId);
  }

  /**
   * Remove a Step from the prerequisites completion list
   *
   * @param {!Step} step The Step object to remove from the completion list
   * @throws {TypeError} when specified value is not a Step
   */
  removeStep(step) {
    if (step && !(step instanceof Step)) {
      throw new TypeError('Value must be Step');
    }
    this.removeStepId(step.id);
  }

  /**
   * Remove a Step ID from the prerequisites completion list
   *
   * @param {!string} stepId The Step ID to remove from the completion list
   * @throws {Error} A Step with the given ID doesn't exist in the list
   * @throws {TypeError} when specified value is not a string
   */
  removeStepId(stepId) {
    if (!TypeCheck.isString(stepId)) {
      throw new TypeError('Value must be String');
    }

    const index = this._steps.findIndex(s => s === stepId);
    if (index === -1) {
      throw new Error(`A Step with id ${stepId} doesn't exist`);
    }

    this._steps.splice(index, 1);
  }

  /**
   * Exports the StepsPrerequisite as a plain Javascript Object
   *
   * @return {Object} An Object describing the Steps Prerequisite
   *
   */
  toJSON() {
    let obj = {
      logical: this._logical,
      steps: this._steps && this._steps.length > 0 ? this._steps : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }
}
