import FieldSpec from './field-spec.js';
import FieldState from './field-state.js';
import FieldInputFilter from './field-input-filter.js';
import JsonConvertable from './json-convertable.js';
import Prerequisites from './prerequisites.js';
import TypeCheck from './typecheck.js';

/**
 * Defines a Field for use inside a Form, Step or Section object
 */
export default class Field extends JsonConvertable {

  /**
   * Creates an instance of Field.
   *
   * @param {string} name The name of the Field
   *
   * @throws {Error} when specified value is null
   * @throws {TypeError} when specified value is not a string
   */
  constructor(name) {
    super();

    if (name == null) {
      throw new Error('Field must have a name');
    }
    if (name && !TypeCheck.isString(name)) {
      throw new TypeError('Field name must be a string');
    }

    this._name = name;
    this._spec = new FieldSpec();
    this._state = new FieldState();
    this._prerequisites = new Prerequisites();
    this._inputFilter = new FieldInputFilter();
  }

  /**
   * Gets the field name
   *
   * @type {string}
   */
  get name() {
    return this._name;
  }

  /**
   * Retrieve the field spec
   *
   * @type {FieldSpec}
   */
  get spec() {
    return this._spec;
  }

  /**
   * Sets the field spec
   *
   * @type {FieldSpec}
   *
   * @throws {TypeError} when specified value is not a FieldSpec object
   */
  set spec(value) {
    if (value && !(value instanceof FieldSpec)) {
      throw new TypeError('Value must be a FieldSpec object');
    }
    this._spec = value;
  }

  /**
   * Gets the state
   *
   * @type {FieldState}
   */
  get state() {
    return this._state;
  }

  /**
   * Sets the state
   *
   * @type {FieldState}
   *
   * @throws {TypeError} when specified value is not a FieldState object
   */
  set state(value) {
    if (value && !(value instanceof FieldState)) {
      throw new TypeError('Value must be a FieldState object');
    }
    this._state = value;
  }

  /**
   * Gets the prerequisites
   *
   * @type {Prerequisites}
   */
  get prerequisites() {
    return this._prerequisites;
  }

  /**
   * Sets the prerequisites
   *
   * @type {Prerequisites}
   *
   * @throws {TypeError} when specified value is not a Prerequisites object
   */
  set prerequisites(value) {
    if (value && !(value instanceof Prerequisites)) {
      throw new TypeError('Value must be a Prerequisites object');
    }
    this._prerequisites = value;
  }

  /**
   * Gets the inputFilter
   *
   * @type {FieldInputFilter}
   */
  get inputFilter() {
    return this._inputFilter;
  }

  /**
   * Sets the inputFilter
   *
   * @type {FieldInputFilter}
   *
   * @throws {TypeError} when specified value is not a FieldInputFilter object
   */
  set inputFilter(value) {
    if (value && !(value instanceof FieldInputFilter)) {
      throw new TypeError('Value must be a FieldInputFilter object');
    }
    this._inputFilter = value;
  }

  /**
   * Exports the Field as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      name: this._name,
      spec: this._spec && this._spec.toJSON(),
      state: this._state && this._state.toJSON(),
      prerequisites: this._prerequisites && this._prerequisites.toJSON(),
      inputFilter: this._inputFilter && this._inputFilter.toJSON()
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a Field instance.
   *
   * @param {Object|string} [data]
   * @return {Field} A step instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.spec = FieldSpec.fromJSON(obj.spec);
    obj.state = FieldState.fromJSON(obj.state);
    obj.prerequisites = Prerequisites.fromJSON(obj.prerequisites);
    obj.inputFilter = FieldInputFilter.fromJSON(obj.inputFilter);

    let instance = new Field(obj.name);

    delete obj.name;
    Object.assign(instance, obj);

    return instance;
  }

}
