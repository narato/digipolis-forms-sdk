import FieldSpecAttributes from './field-spec-attributes.js';
import FieldSpecOptions from './field-spec-options.js';
import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * An object containing the specifications of a Field
 */
export default class FieldSpec extends JsonConvertable {
  /**
   * Creates a new FieldSpec instance
   */
  constructor() {
    super();

    this._attributes = new FieldSpecAttributes();
    this._options = new FieldSpecOptions();
  }

  /**
   * Retrieve the field attributes
   *
   * @type {FieldSpecAttributes}
   */
  get attributes() {
    return this._attributes;
  }

  /**
   * Set the field attributes
   *
   * @type {FieldSpecAttributes}
   *
   * @throws {TypeError} when specified value is not a FieldSpecAttributes instance
   */
  set attributes(value) {
    if (value && !(value instanceof FieldSpecAttributes)) {
      throw new TypeError('Value must be a FieldSpecAttributes');
    }
    this._attributes = value;
  }

  /**
   * Retrieve the field options
   *
   * @type {FieldSpecOptions}
   */
  get options() {
    return this._options;
  }

  /**
   * Set the field options
   *
   * @type {FieldSpecOptions}
   *
   * @throws {TypeError} when specified value is not a FieldSpecOptions instance
   */
  set options(value) {
    if (value && !(value instanceof FieldSpecOptions)) {
      throw new TypeError('Value must be a FieldSpecOptions');
    }
    this._options = value;
  }

  /**
   * Sets the specified type in the field specification object
   * which can be accessed using field.attributes.type
   *
   * @param {string} value Type of the field
   */
  setTypeAttribute(value) {
    this._attributes.type = value;
  }

  /**
   * Sets the specified value in the field specification object
   * which can be accessed using field.attributes.value
   *
   * @param {string} value Prefilled value of the field
   */
  setValueAttribute(value) {
    this._attributes.value = value;
  }

  /**
   * Exports the FieldSpec as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      attributes: this._attributes && this.attributes.toJSON(),
      options: this._options && this.options.toJSON()
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a FieldSpec instance.
   *
   * @param {Object|string} [data]
   * @return {FieldSpec} A field specification containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.attributes = FieldSpecAttributes.fromJSON(obj.attributes);
    obj.options = FieldSpecOptions.fromJSON(obj.options);

    let instance = new FieldSpec();

    Object.assign(instance, obj);

    return instance;
  }

}
