import JsonConvertable from './json-convertable.js';
import FieldValueOperand from './field-value-operand.js';
import TypeCheck from './typecheck.js';

/**
 * An object representing prerequisites defined on FieldValues
 */
export default class FieldValuesPrerequisite extends JsonConvertable {

  /**
   * Creates a new FieldValuesPrerequisite instance
   */
  constructor() {
    super();

    this._logical;
    this._operands = [];
  }

  /**
   * Retrieve the prerequisite combination logic
   *
   * @type {string}
   */
  get logical() {
    return this._logical;
  }

  /**
   * Set the prerequisite combination logic
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set logical(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._logical = value;
  }

  /**
   * Gets the entire collection of Field Value Operands
   *
   * @type {FieldValueOperand[]}
   */
  get operands() {
    return this._operands;
  }

  /**
   * Sets the entire collection of Field Value Operands,
   * overriding the existing collection.
   *
   * @type {FieldValueOperand[]}
   *
   * @throws {TypeError} when specified value is not an array of Field Value Operands
   */
  set operands(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof FieldValueOperand; })) {
      throw new TypeError('Value must be an array of FieldValueOperands');
    }
    this._operands = value;
  }

  /**
   * Adds a FieldValueOperand object to the Form
   *
   * @param {FieldValueOperand} operand The FieldValueOperand object to add to the prerequisite
   * @throws {TypeError} when specified value is not a FieldValueOperand
   */
  addOperand(operand) {
    if (operand && !(operand instanceof FieldValueOperand)) {
      throw new TypeError('Value must be FieldValueOperand');
    }

    this._operands.push(operand);
  }

  /**
   * Remove a FieldValueOperand object from the Form
   *
   * @param {FieldValueOperand} operand The Operand object to remove from the prerequisites
   * @throws {Error} The operand doesn't exist in the prerequisite
   * @throws {TypeError} when specified value is not a FieldValueOperand
   */
  removeOperand(operand) {
    if (operand && !(operand instanceof FieldValueOperand)) {
      throw new TypeError('Value must be FieldValueOperand');
    }

    const index = this._operands.findIndex(o => o === operand);
    if (index === -1) {
      throw new Error("The Operand doesn't exist");
    }

    this._operands.splice(index, 1);
  }

  /**
   * Exports the FieldValuesPrerequisite as a plain Javascript Object
   *
   * @return {Object} An Object describing the FieldValues Prerequisite
   *
   */
  toJSON() {
    let obj = {
      logical: this._logical,
      operands: this._operands && this._operands.length > 0 ? this._operands.map(o => o.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Create a FieldValuePrerequisite instance from a JSON string
   *
   * @param {String|Object} [data] A JSON string or Object literal describing the Field Value Prerequisite
   * @return {FieldValuePrerequisite} A FieldValuePrerequisite instance containing the data from the JSON string
   *
   */
  static fromJSON(data) {
    let obj = data || {};

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.operands = obj.operands || [];
    obj.operands = obj.operands.map(o => FieldValueOperand.fromJSON(o));

    let instance = new FieldValuesPrerequisite();

    Object.assign(instance, obj);

    return instance;
  }
}
