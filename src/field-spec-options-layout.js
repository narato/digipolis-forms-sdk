import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A small object containing layout options of a Field specification
 */
export default class FieldSpecOptionsLayout extends JsonConvertable {
  /**
   * Creates a new FieldSpecOptionsLayout instance
   */
  constructor() {
    super();

    this._fieldLayout;
    this._fieldClass;
  }

  /**
   * Retrieve the layout options' fieldLayout
   *
   * @type {string}
   */
  get fieldLayout() {
    return this._fieldLayout;
  }

  /**
   * Set the layout options' fieldLayout
   *
   * @type {string}
   *
   * @throws {TypeError} when specified layout is not a string
   */
  set fieldLayout(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._fieldLayout = value;
  }

  /**
   * Retrieve the layout options' fieldClass
   *
   * @type {string}
   */
  get fieldClass() {
    return this._fieldClass;
  }

  /**
   * Set the layout options' fieldClass
   *
   * @type {string}
   *
   * @throws {TypeError} when specified fieldClass is not a string
   */
  set fieldClass(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._fieldClass = value;
  }

  /**
   * Exports the FieldSpecOptionsLayout as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      fieldLayout: this._fieldLayout,
      fieldClass: this._fieldClass
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
