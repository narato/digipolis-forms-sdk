import ValidatorOptions from './validator-options.js';
import Validator from './validator.js';
import TypeCheck from './typecheck.js';

/**
 * Defines a AdvancedValidator which is a validator with extended options
 */
export default class AdvancedValidator extends Validator {

  /**
   * Creates an instance of AdvancedValidator.
   *
   * @param {string} name The name of the AdvancedValidator
   * @param {string} type The type of the AdvancedValidator
   * @param {ValidatorOptions} options The validator options of the AdvancedValidator
   *
   * @throws {Error} when specified name, type or options object is null
   * @throws {TypeError} when specified name or type is not a string or when options is not a ValidatorOptions object
   */
  constructor(name, type, options) {
    super(name, type); // checing of name and type happens here

    if (options == null) {
      throw new Error('AdvancedValidator must have an options object');
    }

    if (options && !(options instanceof ValidatorOptions)) {
      throw new TypeError('options must be a ValidatorOptions object');
    }

    this._errorMessage;
    this._options = options;
  }

  /**
   * Gets the options
   *
   * @type {ValidatorOptions}
   */
  get options() {
    return this._options;
  }

  /**
   * Sets the options
   *
   * @type {ValidatorOptions}
   *
   * @throws {Error} when specified value is null
   * @throws {TypeError} when specified value is not a ValidatorOptions object
   */
  set options(value) {
    if (value == null) {
      throw new Error('AdvancedValidator must have an options object');
    }
    if (value && !(value instanceof ValidatorOptions)) {
      throw new TypeError('Value must be a ValidatorOptions object');
    }
    this._options = value;
  }

  /**
   * Exports the AdvancedValidator as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      name: this._name,
      type: this._type,
      errorMessage: this._errorMessage,
      options: this._options && this._options.toJSON()
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a AdvancedValidator instance.
   *
   * @param {Object|string} [data]
   * @return {AdvancedValidator} A section instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.options = ValidatorOptions.fromJSON(obj.options);

    let instance = new AdvancedValidator(obj.name, obj.type, obj.options);

    delete obj.name;
    delete obj.type;
    delete obj.options;
    Object.assign(instance, obj);

    return instance;
  }

}
