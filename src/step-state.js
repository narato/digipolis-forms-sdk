import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A state object containing status fields of a Step
 */
export default class StepState extends JsonConvertable {
  /**
   * Creates a new StepState instance
   */
  constructor() {
    super();

    this._showTitle;
    this._editable;
    this._editMode;
    this._externalUrl;
  }

  /**
   * Retrieve the showTitle flag
   *
   * @type {boolean}
   */
  get showTitle() {
    return this._showTitle;
  }

  /**
   * Set the showTitle flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set showTitle(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._showTitle = value;
  }

  /**
   * Retrieve the editable flag
   *
   * @type {boolean}
   */
  get editable() {
    return this._editable;
  }

  /**
   * Set the editable flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set editable(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._editable = value;
  }

  /**
   * Retrieve the editMode flag
   *
   * @type {boolean}
   */
  get editMode() {
    return this._editMode;
  }

  /**
   * Set the editMode flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set editMode(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._editMode = value;
  }

  /**
   * Retrieve the externalUrl
   *
   * @type {string}
   */
  get externalUrl() {
    return this._externalUrl;
  }

  /**
   * Set the externalUrl
   *
   * @type {string}
   *
   * @throws {TypeError} when specified value is not a string
   */
  set externalUrl(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._externalUrl = value;
  }

  /**
   * Exports the StepState as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      showTitle: this._showTitle,
      editable: this._editable,
      editMode: this._editMode,
      externalUrl: this._externalUrl
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

}
