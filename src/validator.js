import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';

/**
 * A base class that provides functionality that every validator has
 */
export default class Validator extends JsonConvertable {

  /**
   * Creates an instance of Validator.
   *
   * @param {string} name The name of the Validator
   * @param {string} type The type of the Validator
   *
   * @throws {Error} when specified name or type is null
   * @throws {TypeError} when specified name or type is not a string
   */
  constructor(name, type) {
    super();

    if (name == null || type == null) {
      throw new Error('Validator must have a name and type');
    }
    if (name && !TypeCheck.isString(name)) {
      throw new TypeError('Validator name must be a string');
    }
    if (name && !TypeCheck.isString(type)) {
      throw new TypeError('Validator type must be a string');
    }

    this._name = name;
    this._type = type;
    this._errorMessage;
  }

  /**
   * Retrieve the name
   *
   * @type {string}
   */
  get name() {
    return this._name;
  }

  /**
   * Retrieve the type
   *
   * @type {string}
   */
  get type() {
    return this._type;
  }

  /**
   * Retrieve the errorMessage
   *
   * @errorMessage {string}
   */
  get errorMessage() {
    return this._errorMessage;
  }

  /**
   * Sets the errorMessage
   *
   * @errorMessage {string}
   */
  set errorMessage(value) {
    if (value && !TypeCheck.isString(value)) {
      throw new TypeError('Value must be a string');
    }
    this._errorMessage = value;
  }

  /**
   * Exports the Validator as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      name: this._name,
      type: this._type,
      errorMessage: this._errorMessage
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a Validator instance.
   *
   * @param {Object|string} [data]
   * @return {Validator} A section instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    let instance = new Validator(obj.name, obj.type);

    delete obj.name;
    delete obj.type;
    Object.assign(instance, obj);

    return instance;
  }

}
