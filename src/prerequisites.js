import JsonConvertable from './json-convertable.js';
import FieldsPrerequisite from './fields-prerequisite.js';
import FieldValuesPrerequisite from './field-values-prerequisite.js';
import SectionsPrerequisite from './sections-prerequisite.js';
import StepsPrerequisite from './steps-prerequisite.js';
import TypeCheck from './typecheck.js';

/**
 * An object representing prerequisites on a Form, Step or Section
 */
export default class Prerequisites extends JsonConvertable {

  /**
   * Creates a new Prerequisites instance
   */
  constructor() {
    super();

    this._sectionsCompleted = new SectionsPrerequisite();
    this._stepsCompleted = new StepsPrerequisite();
    this._fieldsCompleted = new FieldsPrerequisite();
    this._fieldValues = new FieldValuesPrerequisite();
  }

  /**
   * Retrieve the completed sections prerequisite
   *
   * @type {SectionsPrerequisite}
   */
  get sectionsCompleted() {
    return this._sectionsCompleted;
  }

  /**
   * Set the completed sections prerequisite
   *
   * @type {SectionsPrerequisite}
   *
   * @throws {TypeError} when specified value is not a SectionsPrerequisite instance
   */
  set sectionsCompleted(value) {
    if (value && !(value instanceof SectionsPrerequisite)) {
      throw new TypeError('Value must be a SectionsPrerequisite object');
    }
    this._sectionsCompleted = value;
  }

  /**
   * Retrieve the completed steps prerequisite
   *
   * @type {StepsPrerequisite}
   */
  get stepsCompleted() {
    return this._stepsCompleted;
  }

  /**
   * Set the completed steps prerequisite
   *
   * @type {StepsPrerequisite}
   *
   * @throws {TypeError} when specified value is not a StepsPrerequisite instance
   */
  set stepsCompleted(value) {
    if (value && !(value instanceof StepsPrerequisite)) {
      throw new TypeError('Value must be a StepsPrerequisite object');
    }
    this._stepsCompleted = value;
  }

  /**
   * Retrieve the completed fields prerequisite
   *
   * @type {FieldsPrerequisite}
   */
  get fieldsCompleted() {
    return this._fieldsCompleted;
  }

  /**
   * Set the completed fields prerequisite
   *
   * @type {FieldsPrerequisite}
   *
   * @throws {TypeError} when specified value is not a FieldsPrerequisite instance
   */
  set fieldsCompleted(value) {
    if (value && !(value instanceof FieldsPrerequisite)) {
      throw new TypeError('Value must be a FieldsPrerequisite object');
    }
    this._fieldsCompleted = value;
  }

  /**
   * Retrieve the field values prerequisite
   *
   * @type {FieldValuesPrerequisite}
   */
  get fieldValues() {
    return this._fieldValues;
  }

  /**
   * Set the field values prerequisite
   *
   * @type {FieldValuesPrerequisite}
   *
   * @throws {TypeError} when specified value is not a FieldValuesPrerequisite instance
   */
  set fieldValues(value) {
    if (value && !(value instanceof FieldValuesPrerequisite)) {
      throw new TypeError('Value must be a FieldValuesPrerequisite object');
    }
    this._fieldValues = value;
  }

  /**
   * Exports the Prerequisites as a plain Javascript Object
   *
   * @return {Object} An Object describing the Prerequisites
   *
   */
  toJSON() {
    let obj = {
      sectionsCompleted: this._sectionsCompleted && this._sectionsCompleted.toJSON(),
      stepsCompleted: this._stepsCompleted && this._stepsCompleted.toJSON(),
      fieldsCompleted: this._fieldsCompleted && this._fieldsCompleted.toJSON(),
      fieldValues: this._fieldValues && this._fieldValues.toJSON()
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Create a Prerequisites instance from a JSON string
   *
   * @param {String|Object} [data] A JSON string or Object literal describing the prerequisites
   * @return {Prerequisites} A Prerequisites instance containing the data from the JSON string
   *
   */
  static fromJSON(data) {
    let obj = data || {};

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.sectionsCompleted = SectionsPrerequisite.fromJSON(obj.sectionsCompleted);
    obj.stepsCompleted = StepsPrerequisite.fromJSON(obj.stepsCompleted);
    obj.fieldsCompleted = FieldsPrerequisite.fromJSON(obj.fieldsCompleted);
    obj.fieldValues = FieldValuesPrerequisite.fromJSON(obj.fieldValues);

    let instance = new Prerequisites();

    Object.assign(instance, obj);

    return instance;
  }

}
