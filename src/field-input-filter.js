import Validator from './validator.js';
import JsonConvertable from './json-convertable.js';
import TypeCheck from './typecheck.js';
import ValidatorFactory from './validator-factory.js';

/**
 * An inputFilter object
 */
export default class FieldInputFilter extends JsonConvertable {
  /**
   * Creates a new FieldInputFilter instance
   */
  constructor() {
    super();

    this._required;
    this._validators = [];
  }

  /**
   * Retrieve the required flag
   *
   * @type {boolean}
   */
  get required() {
    return this._required;
  }

  /**
   * Set the required flag
   *
   * @type {boolean}
   *
   * @throws {TypeError} when specified value is not a Boolean
   */
  set required(value) {
    if (value && !TypeCheck.isBoolean(value)) {
      throw new TypeError('Value must be a boolean');
    }
    this._required = value;
  }

  /**
   * Gets the entire collection of Validator objects
   *
   * @type {Validator[]}
   */
  get validators() {
    return this._validators;
  }

  /**
   * Sets the entire collection of Validator objects,
   * overriding the existing collection.
   *
   * @type {Validator[]}
   *
   * @throws {TypeError} when specified value is not an array of Validators
   */
  set validators(value) {
    if (value && !TypeCheck.isArrayOf(value, (i) => { return i instanceof Validator; })) {
      throw new TypeError('Value must be an array of Validators');
    }
    this._validators = value;
  }

  /**
   * Adds a Validator object to the InputFilter
   *
   * @param {Validator} validator The Validator object to add to the InputFilter
   * @throws {Error} A Validator with the same name already exists
   * @throws {TypeError} when specified value is not a Validator
   */
  addValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }
    if (this.getValidator(validator.name) !== undefined) {
      throw new Error(`A Validator with name ${validator.name} already exists`);
    }

    this._validators.push(validator);
  }

  /**
   * Returns the Validator object with the specified name
   *
   * @param {string} validatorName The name of the Validator to retrieve
   * @return {Validator} The Validator with the specified name.
   *                undefined if a validator with the specified name doesn't exist.
   */
  getValidator(validatorName) {
    return this._validators.find(v => v.name === validatorName);
  }

  /**
   * Update a Validator object from the InputFilter
   *
   * @param {Validator} validator The Validator object to update
   * @throws {Error} A Validator with the given name doesn't exist in the inputFilter
   * @throws {TypeError} when specified value is not a Validator
   */
  updateValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }

    const index = this._validators.findIndex(v => v.name === validator.name);
    if (index === -1) {
      throw new Error(`A Validator with name ${validator.name} doesn't exist`);
    }

    this._validators[index] = validator;
  }

  /**
   * Remove a Validator object from the InputFilter
   *
   * @param {Validator} validator The Validator object to remove from the inputFilter
   * @throws {Error} A Validator with the given name doesn't exist in the inputFilter
   * @throws {TypeError} when specified value is not a Validator
   */
  removeValidator(validator) {
    if (validator && !(validator instanceof Validator)) {
      throw new TypeError('Value must be Validator');
    }

    const index = this._validators.findIndex(v => v.name === validator.name);
    if (index === -1) {
      throw new Error(`A Validator with name ${validator.name} doesn't exist`);
    }

    this._validators.splice(index, 1);
  }

  /**
   * Exports the FieldInputFilter as a plain Javascript object.
   *
   * @return {Object} A plain Javascript Object
   */
  toJSON() {
    let obj = {
      required: this._required,
      validators: this._validators && this._validators.length > 0 ? this._validators.map(v => v.toJSON()) : null
    };

    Object.keys(obj).forEach((key) => (obj[key] == null) && delete obj[key]);

    Object.keys(this)
      .filter(key => key[0] !== '_')
      .forEach(key => {
        obj[key] = this[key];
      });

    return obj;
  }

  /**
   * Convert the specified JSON to a FieldInputFilter instance.
   *
   * @param {Object|string} [data]
   * @return {FieldInputFilter} A section instance containing the specified data
   */
  static fromJSON(data) {
    if (data == null) { return data; }

    let obj = data;

    if (TypeCheck.isString(data)) {
      obj = JSON.parse(data);
    }

    obj.validators = obj.validators || [];
    obj.validators = obj.validators.map(v => ValidatorFactory.createValidatorFromJSON(v));

    let instance = new FieldInputFilter();

    Object.assign(instance, obj);

    return instance;
  }

}
