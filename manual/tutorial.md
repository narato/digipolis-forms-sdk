# Creating a new Form template from scratch

Creating a new form template is easy. Just create a new form instance and convert it to JSON.

```
import ComposerSdk from 'acpaas-composer-core-sdk';

let form = ComposerSdk.createForm();
let template = JSON.stringify(form);
```

At this point you have a template string containing the required JSON to render an empty form without content.

# Editing an existing Form template

Editing an existing template is just as easy.

```
import ComposerSdk from 'acpaas-composer-core-sdk';

// for brevity the JSON content is omitted here
// but this variable should hold your existing form template
let myJsonTemplate = "{ ... }";

let form = ComposerSdk.loadFromJSON(myJsonTemplate);

// now let's change the title to something else
form.info.title = 'This new title is much better!';

// no need to call any save method(s)
// just convert the form to JSON, your changes will be included
myJsonTemplate = JSON.stringify(form);
```

# Integration with the Template API
## Loading an existing template from the Template API

Loading an existing template from the Template API using the [node-rest-client library](https://www.npmjs.com/package/node-rest-client) can be done as described below. The example loads the latest version of the template. To select the latest draft or published version, add `'draft'` or `'published'` to the `scope` query paramater array.

```
import ComposerSdk from 'acpaas-composer-core-sdk';
import { Client } from 'node-rest-client';

// load the latest version of the template with the specified templateLookupKey
let client = new Client();
const args = {
    headers: {
        accept: 'application/json',
        tenantKey: 'the-template-api-tenant-key' // replace with the correct key
    },
    parameters: { scope: ['latest'] },
    path: { templateLookupKey: 'the-template-lookup-key' } // replace with the correct templateLookupKey
};

// get the JSON template from the Template API - replace localhost with the correct Template API host
client.get('http://localhost:5001/api/Templates/${templateLookupKey}/versions', args, function(data, response) {
    let myJsonTemplate = data.data[0].content;
    let form = ComposerSdk.loadFromJSON(myJsonTemplate);

    // make changes to the template

    // no need to call any save method(s)
    // just convert the form to JSON, your changes will be included
    myJsonTemplate = JSON.stringify(form);
});
```

## Updating a Form template in the Template API

Updating an existing template in the Template API using the [node-rest-client library](https://www.npmjs.com/package/node-rest-client) can be done as described below. The template may be saved as a draft or published by setting the `isDraft` flag in the request body to `true` or `false`.
```
import ComposerSdk from 'acpaas-composer-core-sdk';
import { Client } from 'node-rest-client';

// initialize the REST Client
let client = new Client();

// load the form as described in 'Loading an existing template from the template API'
let form = ...
form.addStep(ComposerSdk.createStep('a new step'));

// update an existing template in the Template API
const args = {
    headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        tenantKey: 'the-template-api-tenant-key' // replace with the correct key
    },
    data: {
        name: 'my-template',
        description: 'A test template',
        lookupKey: 'the-template-lookup-key' // replace with the correct lookupKey
        isDraft: true, // set to false if template should not be saved as draft
        content: form.toJSON()
    }
};
// send update request to Template API - replace localhost with the correct Template API host
client.put('http://localhost:5001/api/Templates', args, function(data, response) {
    console.log('Response: ' + JSON.stringify(data, null, 2));
});
```

## Saving a new Form template in the Template API
Saving a new template in the Template API using the [node-rest-client library](https://www.npmjs.com/package/node-rest-client) is similar to updating an existing template. Just send a `POST` request instead of a `PUT` request. The template may be saved as a draft or published by setting the `isDraft` flag in the request body to `true` or `false`.

```
import ComposerSdk from 'acpaas-composer-core-sdk';
import { FormInfo } from 'acpaas-composer-core-sdk';
import { Client } from 'node-rest-client';

// initialize the REST Client
let client = new Client();

// create a new form
let form = ComposerSdk.createForm();
let info = new FormInfo();
info.title = 'Yet another form';
form.info = info;

// save the form in the Template API
const args = {
    headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        tenantKey: 'the-template-api-tenant-key' // replace with the correct key
    },
    data: {
        name: 'my-new-template',
        description: 'A new test template',
        isDraft: false,
        content: form.toJSON()
    }
};
// send save request to Template API - replace localhost with the correct Template API host
client.post('http://localhost:5001/api/Templates', args, function(data, response) {
    console.log('Response: ' + JSON.stringify(data, null, 2));
});
```

# Using the Form Renderer
You can visualize your template using the Form Renderer. First, install the Form Renderer. Next, render [a JSON schema directly](tutorial.html#rendering-a-json-schema) or [retrieve a template from the Template API](tutorial.html#rendering-a-template-from-the-template-api).

## Running the Form Renderer
To run the Form Renderer, make sure you have the following tools installed: git, npm, bower, grunt. Next, clone the repository, install the dependencies and start the server:
```
git clone https://bitbucket.org/narato/digipolis-survey-engine-renderer.git
cd digipolis-survey-engine-renderer
git checkout acpaas_development
npm install
bower install
grunt server
```

The Form Renderer is available on http://localhost:35732.

## Rendering a JSON schema
Go to http://localhost:35732 and open 'Your example'. Paste your JSON schema in the text box. The form on the right will automatically rerender.

## Rendering a template from the Template API
Go to http://localhost:35732 and open the 'API integration' example. Fill in the parameters and click 'reload'. The Template will be fetched from the Template API and visualized on the right. Submitting the form will send a request to the Response API.
