# From NPM

The ACPaaS Composer Core SDK is available as an NPM package
on the Digipolis NPM registry.

You can install the package with the standard NPM install command.

```
npm set registry https://npm.antwerpen.be
npm install acpaas-composer-core-sdk
```

# From repository

Besides a packaged version you can of course always clone the Git repository and use the library directly from source.

```
git clone https://stash.antwerpen.be/scm/acpfse/composer_sdk_js.git
```

# Usage
## ES5 environments

If you need to use the library in an environment where import statements are not allowed yet, you can fall back to regular require.

```
var sdk = require('acpaas-composer-core-sdk').default;
```

Note the fact that we need to add .default at the end. This is due to the transpilation of the default export of the library.

## ES6 environments

If you are using a modern ES6 environment you can just use a standard import statement.

```
import ComposerSdk from 'acpaas-composer-core-sdk';
```
