# ES5 environments

If you need to use the library in an environment where import statements are not allowed yet, you can fall back to regular require.

```
var sdk = require('acpaas-composer-core-sdk').default;
```

Note the fact that we need to add .default at the end. This is due to the transpilation of the default export of the library.

# ES6 environments

If you are using a modern ES6 environment you can just use a standard import statement.


```
import ComposerSdk from 'acpaas-composer-core-sdk';
```
