# Getting started

The Composer Core SDK is a Javascript library for producing JSON files used in the ACPaaS Form & Survey Engine. The JSON spec is [described here](https://stash.antwerpen.be/projects/ASTAD/repos/formrenderer_tools_angularjs/browse/wiki/frSchema.md).

Instead of learning this spec, developers can use this SDK and develop in javascript. This way they can have a clean developer experience with a fully functional javascript library, including documentation and support for code editors.

Check out the [installation](installation.html), [tutorial](tutorial.html) and [examples](example.html) to get started.

# License
[ISC](http://opensource.org/licenses/ISC)

# Author
[Narato NV](http://www.narato.be)
