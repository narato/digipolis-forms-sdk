# How do I get started with the library?
The [installation](installation.html) and [usage](usage.html) pages explain how to include the library in your project. Next, go to the [tutorial](tutorial.html) and [example](example.html) pages to get started. The [reference](../identifiers.html) pages give a complete overview of the SDK.

# How will this library handle changes to the JSON template schema?

This library tries to respect properties that it doesn't know about and will preserve them for you.
Suppose you use the library to edit a JSON template that has an unknown "form.just_testing" property. When you convert that form back to JSON the "form.just_testing" property should still exist.
