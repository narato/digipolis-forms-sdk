(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("acpaas-composer-core-sdk", [], factory);
	else if(typeof exports === 'object')
		exports["acpaas-composer-core-sdk"] = factory();
	else
		root["acpaas-composer-core-sdk"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	/**
	 * A module for creating and editing Form templates in JSON format
	 *
	 */
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.ValidatorOptions = exports.ValidatorFactory = exports.Validator = exports.TypeCheck = exports.StepState = exports.StepsPrerequisite = exports.Step = exports.SectionState = exports.SectionsPrerequisite = exports.Section = exports.Prerequisites = exports.NavigationTexts = exports.JsonConvertable = exports.FormInfo = exports.Form = exports.FieldValuesPrerequisite = exports.FieldValueOperand = exports.FieldState = exports.FieldsPrerequisite = exports.FieldSpecOptionsValue = exports.FieldSpecOptionsLayout = exports.FieldSpecOptions = exports.FieldSpecAttributes = exports.FieldSpec = exports.FieldInputFilter = exports.Field = exports.AdvancedValidator = undefined;
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _advancedValidator = __webpack_require__(2);
	
	var _advancedValidator2 = _interopRequireDefault(_advancedValidator);
	
	var _field = __webpack_require__(7);
	
	var _field2 = _interopRequireDefault(_field);
	
	var _fieldInputFilter = __webpack_require__(14);
	
	var _fieldInputFilter2 = _interopRequireDefault(_fieldInputFilter);
	
	var _fieldSpec = __webpack_require__(8);
	
	var _fieldSpec2 = _interopRequireDefault(_fieldSpec);
	
	var _fieldSpecAttributes = __webpack_require__(9);
	
	var _fieldSpecAttributes2 = _interopRequireDefault(_fieldSpecAttributes);
	
	var _fieldSpecOptions = __webpack_require__(10);
	
	var _fieldSpecOptions2 = _interopRequireDefault(_fieldSpecOptions);
	
	var _fieldSpecOptionsLayout = __webpack_require__(11);
	
	var _fieldSpecOptionsLayout2 = _interopRequireDefault(_fieldSpecOptionsLayout);
	
	var _fieldSpecOptionsValue = __webpack_require__(12);
	
	var _fieldSpecOptionsValue2 = _interopRequireDefault(_fieldSpecOptionsValue);
	
	var _fieldsPrerequisite = __webpack_require__(17);
	
	var _fieldsPrerequisite2 = _interopRequireDefault(_fieldsPrerequisite);
	
	var _fieldState = __webpack_require__(13);
	
	var _fieldState2 = _interopRequireDefault(_fieldState);
	
	var _fieldValueOperand = __webpack_require__(19);
	
	var _fieldValueOperand2 = _interopRequireDefault(_fieldValueOperand);
	
	var _fieldValuesPrerequisite = __webpack_require__(18);
	
	var _fieldValuesPrerequisite2 = _interopRequireDefault(_fieldValuesPrerequisite);
	
	var _form = __webpack_require__(27);
	
	var _form2 = _interopRequireDefault(_form);
	
	var _formInfo = __webpack_require__(28);
	
	var _formInfo2 = _interopRequireDefault(_formInfo);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _navigationTexts = __webpack_require__(25);
	
	var _navigationTexts2 = _interopRequireDefault(_navigationTexts);
	
	var _prerequisites = __webpack_require__(16);
	
	var _prerequisites2 = _interopRequireDefault(_prerequisites);
	
	var _section = __webpack_require__(21);
	
	var _section2 = _interopRequireDefault(_section);
	
	var _sectionsPrerequisite = __webpack_require__(20);
	
	var _sectionsPrerequisite2 = _interopRequireDefault(_sectionsPrerequisite);
	
	var _sectionState = __webpack_require__(22);
	
	var _sectionState2 = _interopRequireDefault(_sectionState);
	
	var _step = __webpack_require__(24);
	
	var _step2 = _interopRequireDefault(_step);
	
	var _stepsPrerequisite = __webpack_require__(23);
	
	var _stepsPrerequisite2 = _interopRequireDefault(_stepsPrerequisite);
	
	var _stepState = __webpack_require__(26);
	
	var _stepState2 = _interopRequireDefault(_stepState);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	var _validator = __webpack_require__(6);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var _validatorFactory = __webpack_require__(15);
	
	var _validatorFactory2 = _interopRequireDefault(_validatorFactory);
	
	var _validatorOptions = __webpack_require__(3);
	
	var _validatorOptions2 = _interopRequireDefault(_validatorOptions);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * Factory class to create SDK objects
	 *
	 */
	var ComposerSdk = function () {
	  function ComposerSdk() {
	    _classCallCheck(this, ComposerSdk);
	  }
	
	  _createClass(ComposerSdk, null, [{
	    key: 'loadFromJSON',
	
	    /**
	     * Load a Form instance from a JSON template
	     *
	     * @param {Object|String} [data] An object literal or JSON string
	     * @return {Form} A Form instance
	     *
	     */
	    value: function loadFromJSON(data) {
	      var obj = data || {};
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      return _form2.default.fromJSON(obj);
	    }
	
	    /**
	     * Create a new empty Field instance
	     *
	     * @param {string} [name] The name for the new Field
	     * @return {Field}
	     */
	
	  }, {
	    key: 'createField',
	    value: function createField(name) {
	      return new _field2.default(name);
	    }
	
	    /**
	     * Creates a new empty Form instance
	     *
	     * @return {Form} A Form instance
	     *
	     */
	
	  }, {
	    key: 'createForm',
	    value: function createForm() {
	      return new _form2.default();
	    }
	
	    /**
	     * Creates a new Step which can be added to a Form instance.
	     *
	     * @param {string} [id] An ID which identifies the Step
	     * @return {Step}
	     */
	
	  }, {
	    key: 'createStep',
	    value: function createStep(id) {
	      return new _step2.default(id);
	    }
	
	    /**
	     * Creates a new Section which can be added to a Form instance.
	     *
	     * @param {string} [id] An ID which identifies the Section
	     * @return {Section}
	     */
	
	  }, {
	    key: 'createSection',
	    value: function createSection(id) {
	      return new _section2.default(id);
	    }
	  }]);
	
	  return ComposerSdk;
	}();
	
	exports.default = ComposerSdk;
	exports.AdvancedValidator = _advancedValidator2.default;
	exports.Field = _field2.default;
	exports.FieldInputFilter = _fieldInputFilter2.default;
	exports.FieldSpec = _fieldSpec2.default;
	exports.FieldSpecAttributes = _fieldSpecAttributes2.default;
	exports.FieldSpecOptions = _fieldSpecOptions2.default;
	exports.FieldSpecOptionsLayout = _fieldSpecOptionsLayout2.default;
	exports.FieldSpecOptionsValue = _fieldSpecOptionsValue2.default;
	exports.FieldsPrerequisite = _fieldsPrerequisite2.default;
	exports.FieldState = _fieldState2.default;
	exports.FieldValueOperand = _fieldValueOperand2.default;
	exports.FieldValuesPrerequisite = _fieldValuesPrerequisite2.default;
	exports.Form = _form2.default;
	exports.FormInfo = _formInfo2.default;
	exports.JsonConvertable = _jsonConvertable2.default;
	exports.NavigationTexts = _navigationTexts2.default;
	exports.Prerequisites = _prerequisites2.default;
	exports.Section = _section2.default;
	exports.SectionsPrerequisite = _sectionsPrerequisite2.default;
	exports.SectionState = _sectionState2.default;
	exports.Step = _step2.default;
	exports.StepsPrerequisite = _stepsPrerequisite2.default;
	exports.StepState = _stepState2.default;
	exports.TypeCheck = _typecheck2.default;
	exports.Validator = _validator2.default;
	exports.ValidatorFactory = _validatorFactory2.default;
	exports.ValidatorOptions = _validatorOptions2.default;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _validatorOptions = __webpack_require__(3);
	
	var _validatorOptions2 = _interopRequireDefault(_validatorOptions);
	
	var _validator = __webpack_require__(6);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * Defines a AdvancedValidator which is a validator with extended options
	 */
	var AdvancedValidator = function (_Validator) {
	  _inherits(AdvancedValidator, _Validator);
	
	  /**
	   * Creates an instance of AdvancedValidator.
	   *
	   * @param {string} name The name of the AdvancedValidator
	   * @param {string} type The type of the AdvancedValidator
	   * @param {ValidatorOptions} options The validator options of the AdvancedValidator
	   *
	   * @throws {Error} when specified name, type or options object is null
	   * @throws {TypeError} when specified name or type is not a string or when options is not a ValidatorOptions object
	   */
	  function AdvancedValidator(name, type, options) {
	    _classCallCheck(this, AdvancedValidator);
	
	    // checing of name and type happens here
	
	    var _this = _possibleConstructorReturn(this, (AdvancedValidator.__proto__ || Object.getPrototypeOf(AdvancedValidator)).call(this, name, type));
	
	    if (options == null) {
	      throw new Error('AdvancedValidator must have an options object');
	    }
	
	    if (options && !(options instanceof _validatorOptions2.default)) {
	      throw new TypeError('options must be a ValidatorOptions object');
	    }
	
	    _this._errorMessage;
	    _this._options = options;
	    return _this;
	  }
	
	  /**
	   * Gets the options
	   *
	   * @type {ValidatorOptions}
	   */
	
	
	  _createClass(AdvancedValidator, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the AdvancedValidator as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        name: this._name,
	        type: this._type,
	        errorMessage: this._errorMessage,
	        options: this._options && this._options.toJSON()
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a AdvancedValidator instance.
	     *
	     * @param {Object|string} [data]
	     * @return {AdvancedValidator} A section instance containing the specified data
	     */
	
	  }, {
	    key: 'options',
	    get: function get() {
	      return this._options;
	    }
	
	    /**
	     * Sets the options
	     *
	     * @type {ValidatorOptions}
	     *
	     * @throws {Error} when specified value is null
	     * @throws {TypeError} when specified value is not a ValidatorOptions object
	     */
	    ,
	    set: function set(value) {
	      if (value == null) {
	        throw new Error('AdvancedValidator must have an options object');
	      }
	      if (value && !(value instanceof _validatorOptions2.default)) {
	        throw new TypeError('Value must be a ValidatorOptions object');
	      }
	      this._options = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.options = _validatorOptions2.default.fromJSON(obj.options);
	
	      var instance = new AdvancedValidator(obj.name, obj.type, obj.options);
	
	      delete obj.name;
	      delete obj.type;
	      delete obj.options;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return AdvancedValidator;
	}(_validator2.default);
	
	exports.default = AdvancedValidator;
	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A ValidatorOptions object containing options for a validator.
	 * This is a very generic object, and basically just a wrapper around Object
	 */
	var ValidatorOptions = function (_JsonConvertable) {
	  _inherits(ValidatorOptions, _JsonConvertable);
	
	  /**
	   * Creates a new ValidatorOptions instance
	   */
	  function ValidatorOptions() {
	    _classCallCheck(this, ValidatorOptions);
	
	    return _possibleConstructorReturn(this, (ValidatorOptions.__proto__ || Object.getPrototypeOf(ValidatorOptions)).call(this));
	  }
	
	  /**
	   * Exports the ValidatorOptions as a plain Javascript object.
	   *
	   * @return {Object} A plain Javascript Object
	   */
	
	
	  _createClass(ValidatorOptions, [{
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {};
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }]);
	
	  return ValidatorOptions;
	}(_jsonConvertable2.default);
	
	exports.default = ValidatorOptions;
	module.exports = exports['default'];

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * A base class that provides functionality to easily convert
	 * Form template objects to JSON and back.
	 */
	var JsonConvertable = function () {
	  function JsonConvertable() {
	    _classCallCheck(this, JsonConvertable);
	  }
	
	  _createClass(JsonConvertable, null, [{
	    key: 'fromJSON',
	
	
	    /**
	     * Convert the specified JSON to the inheritor's object type.
	     *
	     * @param {Object|string} [data]
	     * @return {Object} An object instance of the type that extends this class
	     */
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      var instance = new this(obj.id);
	
	      delete obj.id;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return JsonConvertable;
	}();
	
	exports.default = JsonConvertable;
	module.exports = exports['default'];

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * Helper class to assist in verifying the type of certain objects
	 */
	var TypeCheck = function () {
	  function TypeCheck() {
	    _classCallCheck(this, TypeCheck);
	  }
	
	  _createClass(TypeCheck, null, [{
	    key: 'isString',
	
	    /**
	     * Determines if an object is a string
	     *
	     * @param {any} [obj] The variable to verify
	     * @return {Boolean} True if the object is a string, false otherwise
	     *
	     */
	    value: function isString(obj) {
	      return typeof obj === 'string' || obj instanceof String;
	    }
	
	    /**
	     * Determines if an object is a boolean
	     *
	     * @param {any} [obj] The variable to verify
	     * @return {Boolean} True if the object is a boolean, false otherwise
	     *
	     */
	
	  }, {
	    key: 'isBoolean',
	    value: function isBoolean(obj) {
	      return typeof obj === 'boolean' || obj instanceof Boolean;
	    }
	
	    /**
	     * Determines if an object is a number
	     *
	     * @param {any} [obj] The variable to verify
	     * @return {Boolean} True if the object is a number, false otherwise
	     *
	     */
	
	  }, {
	    key: 'isNumber',
	    value: function isNumber(obj) {
	      return typeof obj === 'number' || obj instanceof Number;
	    }
	
	    /**
	     * Determines if an object is an array
	     *
	     * @param {any} [obj] The variable to verify
	     * @return {Boolean} True if the object is an array, false otherwise
	     *
	     */
	
	  }, {
	    key: 'isArray',
	    value: function isArray(obj) {
	      return obj instanceof Array;
	    }
	
	    /**
	     * Determines if an object is an array
	     * and if the elements adhere to the specified function
	     *
	     * @param {any} [obj] The variable to verify
	     * @param {Function} callback Function to test for each element
	     * @return {Boolean} True if the object is an array
	     *    and the elements adhere to the specified function, false otherwise
	     *
	     */
	
	  }, {
	    key: 'isArrayOf',
	    value: function isArrayOf(obj, callback) {
	      return TypeCheck.isArray(obj) && obj.every(function (i) {
	        return callback(i);
	      });
	    }
	  }]);
	
	  return TypeCheck;
	}();
	
	exports.default = TypeCheck;
	module.exports = exports['default'];

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A base class that provides functionality that every validator has
	 */
	var Validator = function (_JsonConvertable) {
	  _inherits(Validator, _JsonConvertable);
	
	  /**
	   * Creates an instance of Validator.
	   *
	   * @param {string} name The name of the Validator
	   * @param {string} type The type of the Validator
	   *
	   * @throws {Error} when specified name or type is null
	   * @throws {TypeError} when specified name or type is not a string
	   */
	  function Validator(name, type) {
	    _classCallCheck(this, Validator);
	
	    var _this = _possibleConstructorReturn(this, (Validator.__proto__ || Object.getPrototypeOf(Validator)).call(this));
	
	    if (name == null || type == null) {
	      throw new Error('Validator must have a name and type');
	    }
	    if (name && !_typecheck2.default.isString(name)) {
	      throw new TypeError('Validator name must be a string');
	    }
	    if (name && !_typecheck2.default.isString(type)) {
	      throw new TypeError('Validator type must be a string');
	    }
	
	    _this._name = name;
	    _this._type = type;
	    _this._errorMessage;
	    return _this;
	  }
	
	  /**
	   * Retrieve the name
	   *
	   * @type {string}
	   */
	
	
	  _createClass(Validator, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the Validator as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        name: this._name,
	        type: this._type,
	        errorMessage: this._errorMessage
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a Validator instance.
	     *
	     * @param {Object|string} [data]
	     * @return {Validator} A section instance containing the specified data
	     */
	
	  }, {
	    key: 'name',
	    get: function get() {
	      return this._name;
	    }
	
	    /**
	     * Retrieve the type
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'type',
	    get: function get() {
	      return this._type;
	    }
	
	    /**
	     * Retrieve the errorMessage
	     *
	     * @errorMessage {string}
	     */
	
	  }, {
	    key: 'errorMessage',
	    get: function get() {
	      return this._errorMessage;
	    }
	
	    /**
	     * Sets the errorMessage
	     *
	     * @errorMessage {string}
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._errorMessage = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      var instance = new Validator(obj.name, obj.type);
	
	      delete obj.name;
	      delete obj.type;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Validator;
	}(_jsonConvertable2.default);
	
	exports.default = Validator;
	module.exports = exports['default'];

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _fieldSpec = __webpack_require__(8);
	
	var _fieldSpec2 = _interopRequireDefault(_fieldSpec);
	
	var _fieldState = __webpack_require__(13);
	
	var _fieldState2 = _interopRequireDefault(_fieldState);
	
	var _fieldInputFilter = __webpack_require__(14);
	
	var _fieldInputFilter2 = _interopRequireDefault(_fieldInputFilter);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _prerequisites = __webpack_require__(16);
	
	var _prerequisites2 = _interopRequireDefault(_prerequisites);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * Defines a Field for use inside a Form, Step or Section object
	 */
	var Field = function (_JsonConvertable) {
	  _inherits(Field, _JsonConvertable);
	
	  /**
	   * Creates an instance of Field.
	   *
	   * @param {string} name The name of the Field
	   *
	   * @throws {Error} when specified value is null
	   * @throws {TypeError} when specified value is not a string
	   */
	  function Field(name) {
	    _classCallCheck(this, Field);
	
	    var _this = _possibleConstructorReturn(this, (Field.__proto__ || Object.getPrototypeOf(Field)).call(this));
	
	    if (name == null) {
	      throw new Error('Field must have a name');
	    }
	    if (name && !_typecheck2.default.isString(name)) {
	      throw new TypeError('Field name must be a string');
	    }
	
	    _this._name = name;
	    _this._spec = new _fieldSpec2.default();
	    _this._state = new _fieldState2.default();
	    _this._prerequisites = new _prerequisites2.default();
	    _this._inputFilter = new _fieldInputFilter2.default();
	    return _this;
	  }
	
	  /**
	   * Gets the field name
	   *
	   * @type {string}
	   */
	
	
	  _createClass(Field, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the Field as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        name: this._name,
	        spec: this._spec && this._spec.toJSON(),
	        state: this._state && this._state.toJSON(),
	        prerequisites: this._prerequisites && this._prerequisites.toJSON(),
	        inputFilter: this._inputFilter && this._inputFilter.toJSON()
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a Field instance.
	     *
	     * @param {Object|string} [data]
	     * @return {Field} A step instance containing the specified data
	     */
	
	  }, {
	    key: 'name',
	    get: function get() {
	      return this._name;
	    }
	
	    /**
	     * Retrieve the field spec
	     *
	     * @type {FieldSpec}
	     */
	
	  }, {
	    key: 'spec',
	    get: function get() {
	      return this._spec;
	    }
	
	    /**
	     * Sets the field spec
	     *
	     * @type {FieldSpec}
	     *
	     * @throws {TypeError} when specified value is not a FieldSpec object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldSpec2.default)) {
	        throw new TypeError('Value must be a FieldSpec object');
	      }
	      this._spec = value;
	    }
	
	    /**
	     * Gets the state
	     *
	     * @type {FieldState}
	     */
	
	  }, {
	    key: 'state',
	    get: function get() {
	      return this._state;
	    }
	
	    /**
	     * Sets the state
	     *
	     * @type {FieldState}
	     *
	     * @throws {TypeError} when specified value is not a FieldState object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldState2.default)) {
	        throw new TypeError('Value must be a FieldState object');
	      }
	      this._state = value;
	    }
	
	    /**
	     * Gets the prerequisites
	     *
	     * @type {Prerequisites}
	     */
	
	  }, {
	    key: 'prerequisites',
	    get: function get() {
	      return this._prerequisites;
	    }
	
	    /**
	     * Sets the prerequisites
	     *
	     * @type {Prerequisites}
	     *
	     * @throws {TypeError} when specified value is not a Prerequisites object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _prerequisites2.default)) {
	        throw new TypeError('Value must be a Prerequisites object');
	      }
	      this._prerequisites = value;
	    }
	
	    /**
	     * Gets the inputFilter
	     *
	     * @type {FieldInputFilter}
	     */
	
	  }, {
	    key: 'inputFilter',
	    get: function get() {
	      return this._inputFilter;
	    }
	
	    /**
	     * Sets the inputFilter
	     *
	     * @type {FieldInputFilter}
	     *
	     * @throws {TypeError} when specified value is not a FieldInputFilter object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldInputFilter2.default)) {
	        throw new TypeError('Value must be a FieldInputFilter object');
	      }
	      this._inputFilter = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.spec = _fieldSpec2.default.fromJSON(obj.spec);
	      obj.state = _fieldState2.default.fromJSON(obj.state);
	      obj.prerequisites = _prerequisites2.default.fromJSON(obj.prerequisites);
	      obj.inputFilter = _fieldInputFilter2.default.fromJSON(obj.inputFilter);
	
	      var instance = new Field(obj.name);
	
	      delete obj.name;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Field;
	}(_jsonConvertable2.default);
	
	exports.default = Field;
	module.exports = exports['default'];

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _fieldSpecAttributes = __webpack_require__(9);
	
	var _fieldSpecAttributes2 = _interopRequireDefault(_fieldSpecAttributes);
	
	var _fieldSpecOptions = __webpack_require__(10);
	
	var _fieldSpecOptions2 = _interopRequireDefault(_fieldSpecOptions);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object containing the specifications of a Field
	 */
	var FieldSpec = function (_JsonConvertable) {
	  _inherits(FieldSpec, _JsonConvertable);
	
	  /**
	   * Creates a new FieldSpec instance
	   */
	  function FieldSpec() {
	    _classCallCheck(this, FieldSpec);
	
	    var _this = _possibleConstructorReturn(this, (FieldSpec.__proto__ || Object.getPrototypeOf(FieldSpec)).call(this));
	
	    _this._attributes = new _fieldSpecAttributes2.default();
	    _this._options = new _fieldSpecOptions2.default();
	    return _this;
	  }
	
	  /**
	   * Retrieve the field attributes
	   *
	   * @type {FieldSpecAttributes}
	   */
	
	
	  _createClass(FieldSpec, [{
	    key: 'setTypeAttribute',
	
	
	    /**
	     * Sets the specified type in the field specification object
	     * which can be accessed using field.attributes.type
	     *
	     * @param {string} value Type of the field
	     */
	    value: function setTypeAttribute(value) {
	      this._attributes.type = value;
	    }
	
	    /**
	     * Sets the specified value in the field specification object
	     * which can be accessed using field.attributes.value
	     *
	     * @param {string} value Prefilled value of the field
	     */
	
	  }, {
	    key: 'setValueAttribute',
	    value: function setValueAttribute(value) {
	      this._attributes.value = value;
	    }
	
	    /**
	     * Exports the FieldSpec as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        attributes: this._attributes && this.attributes.toJSON(),
	        options: this._options && this.options.toJSON()
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a FieldSpec instance.
	     *
	     * @param {Object|string} [data]
	     * @return {FieldSpec} A field specification containing the specified data
	     */
	
	  }, {
	    key: 'attributes',
	    get: function get() {
	      return this._attributes;
	    }
	
	    /**
	     * Set the field attributes
	     *
	     * @type {FieldSpecAttributes}
	     *
	     * @throws {TypeError} when specified value is not a FieldSpecAttributes instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldSpecAttributes2.default)) {
	        throw new TypeError('Value must be a FieldSpecAttributes');
	      }
	      this._attributes = value;
	    }
	
	    /**
	     * Retrieve the field options
	     *
	     * @type {FieldSpecOptions}
	     */
	
	  }, {
	    key: 'options',
	    get: function get() {
	      return this._options;
	    }
	
	    /**
	     * Set the field options
	     *
	     * @type {FieldSpecOptions}
	     *
	     * @throws {TypeError} when specified value is not a FieldSpecOptions instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldSpecOptions2.default)) {
	        throw new TypeError('Value must be a FieldSpecOptions');
	      }
	      this._options = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.attributes = _fieldSpecAttributes2.default.fromJSON(obj.attributes);
	      obj.options = _fieldSpecOptions2.default.fromJSON(obj.options);
	
	      var instance = new FieldSpec();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return FieldSpec;
	}(_jsonConvertable2.default);
	
	exports.default = FieldSpec;
	module.exports = exports['default'];

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A small object containing attributes of a Field specification
	 */
	var FieldSpecAttributes = function (_JsonConvertable) {
	  _inherits(FieldSpecAttributes, _JsonConvertable);
	
	  /**
	   * Creates a new FieldSpecAttributes instance
	   */
	  function FieldSpecAttributes() {
	    _classCallCheck(this, FieldSpecAttributes);
	
	    var _this = _possibleConstructorReturn(this, (FieldSpecAttributes.__proto__ || Object.getPrototypeOf(FieldSpecAttributes)).call(this));
	
	    _this._type;
	    _this._value;
	    return _this;
	  }
	
	  /**
	   * Retrieve the field type
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldSpecAttributes, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FieldSpecAttributes as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        type: this._type,
	        value: this._value
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'type',
	    get: function get() {
	      return this._type;
	    }
	
	    /**
	     * Set the field type
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._type = value;
	    }
	
	    /**
	     * Retrieve the prefilled field value
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'value',
	    get: function get() {
	      return this._value;
	    }
	
	    /**
	     * Set the prefilled field value
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._value = value;
	    }
	  }]);
	
	  return FieldSpecAttributes;
	}(_jsonConvertable2.default);
	
	exports.default = FieldSpecAttributes;
	module.exports = exports['default'];

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _fieldSpecOptionsLayout = __webpack_require__(11);
	
	var _fieldSpecOptionsLayout2 = _interopRequireDefault(_fieldSpecOptionsLayout);
	
	var _fieldSpecOptionsValue = __webpack_require__(12);
	
	var _fieldSpecOptionsValue2 = _interopRequireDefault(_fieldSpecOptionsValue);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A small object containing options of a Field specification
	 */
	var FieldSpecOptions = function (_JsonConvertable) {
	  _inherits(FieldSpecOptions, _JsonConvertable);
	
	  /**
	   * Creates a new FieldSpecOptions instance
	   */
	  function FieldSpecOptions() {
	    _classCallCheck(this, FieldSpecOptions);
	
	    var _this = _possibleConstructorReturn(this, (FieldSpecOptions.__proto__ || Object.getPrototypeOf(FieldSpecOptions)).call(this));
	
	    _this._layout = new _fieldSpecOptionsLayout2.default();
	    _this._valueOptions = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the field layout options
	   *
	   * @type {FieldSpecOptionsLayout}
	   */
	
	
	  _createClass(FieldSpecOptions, [{
	    key: 'addValueOption',
	
	
	    /**
	     * Adds the specified value option
	     *
	     * @param {FieldSpecOptionsValue} [opt] The value option to add
	     *
	     * @throws {Error} when the a value options already exists with the same key
	     * @throws {TypeError} when the specified value is not a FieldSpecOptionsValue instance
	     */
	    value: function addValueOption(opt) {
	      if (opt && !(opt instanceof _fieldSpecOptionsValue2.default)) {
	        throw new TypeError('Value must be FieldSpecOptionsValue');
	      }
	
	      if (this.getValueOption(opt.key) !== undefined) {
	        throw new Error('A value option with key ' + opt.key + ' already exists');
	      }
	
	      this._valueOptions.push(opt);
	    }
	
	    /**
	     * Returns the value options with the specified key
	     *
	     * @param {string} [key] The key of the value options to retrieve
	     * @return {FieldSpecOptionsValue} The value options with the specified key.
	     *                                 undefined if no value options exists with the specified key.
	     */
	
	  }, {
	    key: 'getValueOption',
	    value: function getValueOption(key) {
	      return this._valueOptions.find(function (v) {
	        return v.key === key;
	      });
	    }
	
	    /**
	     * Remove the value option with the specified key
	     *
	     * @param {string} [key] The key of the value option to remove
	     * @throws {Error} No value option with the specified key exists
	     */
	
	  }, {
	    key: 'removeValueOption',
	    value: function removeValueOption(key) {
	      var index = this.valueOptions.findIndex(function (v) {
	        return v.key === key;
	      });
	
	      if (index === -1) {
	        throw new Error('No value option with key ' + key + ' to remove');
	      }
	
	      this.valueOptions.splice(index, 1);
	    }
	
	    /**
	     * Sets the specified layout in the field specification object
	     * which can be accessed using options.layout.fieldLayout
	     *
	     * @param {string} value Layout of the field
	     */
	
	  }, {
	    key: 'setFieldLayout',
	    value: function setFieldLayout(value) {
	      this._layout.fieldLayout = value;
	    }
	
	    /**
	     * Sets the specified layout classes in the field specification object
	     * which can be accessed using options.layout.fieldClass
	     *
	     * @param {string} value CSS classes to add to the field's class attribute
	     */
	
	  }, {
	    key: 'setFieldClass',
	    value: function setFieldClass(value) {
	      this._layout.fieldClass = value;
	    }
	
	    /**
	     * Exports the FieldSpecOptions as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        layout: this._layout && this._layout.toJSON(),
	        valueOptions: this._valueOptions && this._valueOptions.length > 0 ? this._valueOptions.map(function (v) {
	          return v.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a FieldSpecOptions instance.
	     *
	     * @param {Object|string} [data]
	     * @return {FieldSpecOptions} A fieldSpecOptions instance containing the specified data
	     */
	
	  }, {
	    key: 'layout',
	    get: function get() {
	      return this._layout;
	    }
	
	    /**
	     * Set the field layout options
	     *
	     * @type {FieldSpecOptionsLayout}
	     *
	     * @throws {TypeError} when specified layout is not a FieldSpecOptionsLayout instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldSpecOptionsLayout2.default)) {
	        throw new TypeError('Value must be a FieldSpecOptionsLayout');
	      }
	      this._layout = value;
	    }
	
	    /**
	     * Retrieves the collection of value options
	     *
	     * @type {FieldSpecOptionsValue[]}
	     */
	
	  }, {
	    key: 'valueOptions',
	    get: function get() {
	      return this._valueOptions;
	    }
	
	    /**
	     * Sets the value options collection
	     *
	     * @type {FieldSpecOptionsValue[]}
	     */
	    ,
	    set: function set(value) {
	      this._valueOptions = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.layout = _fieldSpecOptionsLayout2.default.fromJSON(obj.layout);
	
	      obj.valueOptions = obj.valueOptions || [];
	      obj.valueOptions = obj.valueOptions.map(function (v) {
	        return _fieldSpecOptionsValue2.default.fromJSON(v);
	      });
	
	      var instance = new FieldSpecOptions();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return FieldSpecOptions;
	}(_jsonConvertable2.default);
	
	exports.default = FieldSpecOptions;
	module.exports = exports['default'];

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A small object containing layout options of a Field specification
	 */
	var FieldSpecOptionsLayout = function (_JsonConvertable) {
	  _inherits(FieldSpecOptionsLayout, _JsonConvertable);
	
	  /**
	   * Creates a new FieldSpecOptionsLayout instance
	   */
	  function FieldSpecOptionsLayout() {
	    _classCallCheck(this, FieldSpecOptionsLayout);
	
	    var _this = _possibleConstructorReturn(this, (FieldSpecOptionsLayout.__proto__ || Object.getPrototypeOf(FieldSpecOptionsLayout)).call(this));
	
	    _this._fieldLayout;
	    _this._fieldClass;
	    return _this;
	  }
	
	  /**
	   * Retrieve the layout options' fieldLayout
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldSpecOptionsLayout, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FieldSpecOptionsLayout as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        fieldLayout: this._fieldLayout,
	        fieldClass: this._fieldClass
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'fieldLayout',
	    get: function get() {
	      return this._fieldLayout;
	    }
	
	    /**
	     * Set the layout options' fieldLayout
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified layout is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._fieldLayout = value;
	    }
	
	    /**
	     * Retrieve the layout options' fieldClass
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'fieldClass',
	    get: function get() {
	      return this._fieldClass;
	    }
	
	    /**
	     * Set the layout options' fieldClass
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified fieldClass is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._fieldClass = value;
	    }
	  }]);
	
	  return FieldSpecOptionsLayout;
	}(_jsonConvertable2.default);
	
	exports.default = FieldSpecOptionsLayout;
	module.exports = exports['default'];

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * Contains key and value pairs as value options for Fields
	 */
	var FieldSpecOptionsValue = function (_JsonConvertable) {
	  _inherits(FieldSpecOptionsValue, _JsonConvertable);
	
	  /**
	   * Creates a new FieldSpecOptionsValue instance
	   */
	  function FieldSpecOptionsValue() {
	    _classCallCheck(this, FieldSpecOptionsValue);
	
	    var _this = _possibleConstructorReturn(this, (FieldSpecOptionsValue.__proto__ || Object.getPrototypeOf(FieldSpecOptionsValue)).call(this));
	
	    _this._key;
	    _this._value;
	    return _this;
	  }
	
	  /**
	   * Retrieve the key of the value
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldSpecOptionsValue, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FieldSpecOptionsValue as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        key: this.key,
	        value: this.value
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'key',
	    get: function get() {
	      return this._key;
	    }
	
	    /**
	     * Set the key of the value
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified key is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._key = value;
	    }
	
	    /**
	     * Retrieve the content of the value
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'value',
	    get: function get() {
	      return this._value;
	    }
	
	    /**
	     * Set the content of the value
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified content is not a string
	     */
	    ,
	    set: function set(val) {
	      if (val && !_typecheck2.default.isString(val)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._value = val;
	    }
	  }]);
	
	  return FieldSpecOptionsValue;
	}(_jsonConvertable2.default);
	
	exports.default = FieldSpecOptionsValue;
	module.exports = exports['default'];

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A state object containing status fields of a Field
	 */
	var FieldState = function (_JsonConvertable) {
	  _inherits(FieldState, _JsonConvertable);
	
	  /**
	   * Creates a new FieldState instance
	   */
	  function FieldState() {
	    _classCallCheck(this, FieldState);
	
	    var _this = _possibleConstructorReturn(this, (FieldState.__proto__ || Object.getPrototypeOf(FieldState)).call(this));
	
	    _this._editable;
	    _this._editMode;
	    _this._externalUrl;
	    _this._extraInfoCollapsed;
	    return _this;
	  }
	
	  /**
	   * Retrieve the editable flag
	   *
	   * @type {boolean}
	   */
	
	
	  _createClass(FieldState, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FieldState as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        editable: this._editable,
	        editMode: this._editMode,
	        externalUrl: this._externalUrl,
	        extraInfoCollapsed: this._extraInfoCollapsed
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'editable',
	    get: function get() {
	      return this._editable;
	    }
	
	    /**
	     * Set the editable flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._editable = value;
	    }
	
	    /**
	     * Retrieve the editMode flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'editMode',
	    get: function get() {
	      return this._editMode;
	    }
	
	    /**
	     * Set the editMode flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._editMode = value;
	    }
	
	    /**
	     * Retrieve the externalUrl
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'externalUrl',
	    get: function get() {
	      return this._externalUrl;
	    }
	
	    /**
	     * Set the externalUrl
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._externalUrl = value;
	    }
	
	    /**
	     * Retrieve the extraInfoCollapsed flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'extraInfoCollapsed',
	    get: function get() {
	      return this._extraInfoCollapsed;
	    }
	
	    /**
	     * Set the extraInfoCollapsed flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._extraInfoCollapsed = value;
	    }
	  }]);
	
	  return FieldState;
	}(_jsonConvertable2.default);
	
	exports.default = FieldState;
	module.exports = exports['default'];

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _validator = __webpack_require__(6);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	var _validatorFactory = __webpack_require__(15);
	
	var _validatorFactory2 = _interopRequireDefault(_validatorFactory);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An inputFilter object
	 */
	var FieldInputFilter = function (_JsonConvertable) {
	  _inherits(FieldInputFilter, _JsonConvertable);
	
	  /**
	   * Creates a new FieldInputFilter instance
	   */
	  function FieldInputFilter() {
	    _classCallCheck(this, FieldInputFilter);
	
	    var _this = _possibleConstructorReturn(this, (FieldInputFilter.__proto__ || Object.getPrototypeOf(FieldInputFilter)).call(this));
	
	    _this._required;
	    _this._validators = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the required flag
	   *
	   * @type {boolean}
	   */
	
	
	  _createClass(FieldInputFilter, [{
	    key: 'addValidator',
	
	
	    /**
	     * Adds a Validator object to the InputFilter
	     *
	     * @param {Validator} validator The Validator object to add to the InputFilter
	     * @throws {Error} A Validator with the same name already exists
	     * @throws {TypeError} when specified value is not a Validator
	     */
	    value: function addValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	      if (this.getValidator(validator.name) !== undefined) {
	        throw new Error('A Validator with name ' + validator.name + ' already exists');
	      }
	
	      this._validators.push(validator);
	    }
	
	    /**
	     * Returns the Validator object with the specified name
	     *
	     * @param {string} validatorName The name of the Validator to retrieve
	     * @return {Validator} The Validator with the specified name.
	     *                undefined if a validator with the specified name doesn't exist.
	     */
	
	  }, {
	    key: 'getValidator',
	    value: function getValidator(validatorName) {
	      return this._validators.find(function (v) {
	        return v.name === validatorName;
	      });
	    }
	
	    /**
	     * Update a Validator object from the InputFilter
	     *
	     * @param {Validator} validator The Validator object to update
	     * @throws {Error} A Validator with the given name doesn't exist in the inputFilter
	     * @throws {TypeError} when specified value is not a Validator
	     */
	
	  }, {
	    key: 'updateValidator',
	    value: function updateValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	
	      var index = this._validators.findIndex(function (v) {
	        return v.name === validator.name;
	      });
	      if (index === -1) {
	        throw new Error('A Validator with name ' + validator.name + ' doesn\'t exist');
	      }
	
	      this._validators[index] = validator;
	    }
	
	    /**
	     * Remove a Validator object from the InputFilter
	     *
	     * @param {Validator} validator The Validator object to remove from the inputFilter
	     * @throws {Error} A Validator with the given name doesn't exist in the inputFilter
	     * @throws {TypeError} when specified value is not a Validator
	     */
	
	  }, {
	    key: 'removeValidator',
	    value: function removeValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	
	      var index = this._validators.findIndex(function (v) {
	        return v.name === validator.name;
	      });
	      if (index === -1) {
	        throw new Error('A Validator with name ' + validator.name + ' doesn\'t exist');
	      }
	
	      this._validators.splice(index, 1);
	    }
	
	    /**
	     * Exports the FieldInputFilter as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        required: this._required,
	        validators: this._validators && this._validators.length > 0 ? this._validators.map(function (v) {
	          return v.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a FieldInputFilter instance.
	     *
	     * @param {Object|string} [data]
	     * @return {FieldInputFilter} A section instance containing the specified data
	     */
	
	  }, {
	    key: 'required',
	    get: function get() {
	      return this._required;
	    }
	
	    /**
	     * Set the required flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._required = value;
	    }
	
	    /**
	     * Gets the entire collection of Validator objects
	     *
	     * @type {Validator[]}
	     */
	
	  }, {
	    key: 'validators',
	    get: function get() {
	      return this._validators;
	    }
	
	    /**
	     * Sets the entire collection of Validator objects,
	     * overriding the existing collection.
	     *
	     * @type {Validator[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Validators
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _validator2.default;
	      })) {
	        throw new TypeError('Value must be an array of Validators');
	      }
	      this._validators = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.validators = obj.validators || [];
	      obj.validators = obj.validators.map(function (v) {
	        return _validatorFactory2.default.createValidatorFromJSON(v);
	      });
	
	      var instance = new FieldInputFilter();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return FieldInputFilter;
	}(_jsonConvertable2.default);
	
	exports.default = FieldInputFilter;
	module.exports = exports['default'];

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	var _validator = __webpack_require__(6);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var _advancedValidator = __webpack_require__(2);
	
	var _advancedValidator2 = _interopRequireDefault(_advancedValidator);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * Factory class to create the correct validator type
	 */
	var ValidatorFactory = function () {
	  function ValidatorFactory() {
	    _classCallCheck(this, ValidatorFactory);
	  }
	
	  _createClass(ValidatorFactory, null, [{
	    key: 'createValidatorFromJSON',
	
	
	    /**
	     * Convert the specified JSON to a Validator or AdvancedValidator instance.
	     *
	     * @param {Object|string} [data]
	     * @return {Validator|AdvancedValidator} A validator instance containing the specified data
	     */
	    value: function createValidatorFromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      if (obj.hasOwnProperty('options') && obj.options != null) {
	        return _advancedValidator2.default.fromJSON(obj);
	      }
	      return _validator2.default.fromJSON(obj);
	    }
	  }]);
	
	  return ValidatorFactory;
	}();
	
	exports.default = ValidatorFactory;
	module.exports = exports['default'];

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _fieldsPrerequisite = __webpack_require__(17);
	
	var _fieldsPrerequisite2 = _interopRequireDefault(_fieldsPrerequisite);
	
	var _fieldValuesPrerequisite = __webpack_require__(18);
	
	var _fieldValuesPrerequisite2 = _interopRequireDefault(_fieldValuesPrerequisite);
	
	var _sectionsPrerequisite = __webpack_require__(20);
	
	var _sectionsPrerequisite2 = _interopRequireDefault(_sectionsPrerequisite);
	
	var _stepsPrerequisite = __webpack_require__(23);
	
	var _stepsPrerequisite2 = _interopRequireDefault(_stepsPrerequisite);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object representing prerequisites on a Form, Step or Section
	 */
	var Prerequisites = function (_JsonConvertable) {
	  _inherits(Prerequisites, _JsonConvertable);
	
	  /**
	   * Creates a new Prerequisites instance
	   */
	  function Prerequisites() {
	    _classCallCheck(this, Prerequisites);
	
	    var _this = _possibleConstructorReturn(this, (Prerequisites.__proto__ || Object.getPrototypeOf(Prerequisites)).call(this));
	
	    _this._sectionsCompleted = new _sectionsPrerequisite2.default();
	    _this._stepsCompleted = new _stepsPrerequisite2.default();
	    _this._fieldsCompleted = new _fieldsPrerequisite2.default();
	    _this._fieldValues = new _fieldValuesPrerequisite2.default();
	    return _this;
	  }
	
	  /**
	   * Retrieve the completed sections prerequisite
	   *
	   * @type {SectionsPrerequisite}
	   */
	
	
	  _createClass(Prerequisites, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the Prerequisites as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the Prerequisites
	     *
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        sectionsCompleted: this._sectionsCompleted && this._sectionsCompleted.toJSON(),
	        stepsCompleted: this._stepsCompleted && this._stepsCompleted.toJSON(),
	        fieldsCompleted: this._fieldsCompleted && this._fieldsCompleted.toJSON(),
	        fieldValues: this._fieldValues && this._fieldValues.toJSON()
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Create a Prerequisites instance from a JSON string
	     *
	     * @param {String|Object} [data] A JSON string or Object literal describing the prerequisites
	     * @return {Prerequisites} A Prerequisites instance containing the data from the JSON string
	     *
	     */
	
	  }, {
	    key: 'sectionsCompleted',
	    get: function get() {
	      return this._sectionsCompleted;
	    }
	
	    /**
	     * Set the completed sections prerequisite
	     *
	     * @type {SectionsPrerequisite}
	     *
	     * @throws {TypeError} when specified value is not a SectionsPrerequisite instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _sectionsPrerequisite2.default)) {
	        throw new TypeError('Value must be a SectionsPrerequisite object');
	      }
	      this._sectionsCompleted = value;
	    }
	
	    /**
	     * Retrieve the completed steps prerequisite
	     *
	     * @type {StepsPrerequisite}
	     */
	
	  }, {
	    key: 'stepsCompleted',
	    get: function get() {
	      return this._stepsCompleted;
	    }
	
	    /**
	     * Set the completed steps prerequisite
	     *
	     * @type {StepsPrerequisite}
	     *
	     * @throws {TypeError} when specified value is not a StepsPrerequisite instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _stepsPrerequisite2.default)) {
	        throw new TypeError('Value must be a StepsPrerequisite object');
	      }
	      this._stepsCompleted = value;
	    }
	
	    /**
	     * Retrieve the completed fields prerequisite
	     *
	     * @type {FieldsPrerequisite}
	     */
	
	  }, {
	    key: 'fieldsCompleted',
	    get: function get() {
	      return this._fieldsCompleted;
	    }
	
	    /**
	     * Set the completed fields prerequisite
	     *
	     * @type {FieldsPrerequisite}
	     *
	     * @throws {TypeError} when specified value is not a FieldsPrerequisite instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldsPrerequisite2.default)) {
	        throw new TypeError('Value must be a FieldsPrerequisite object');
	      }
	      this._fieldsCompleted = value;
	    }
	
	    /**
	     * Retrieve the field values prerequisite
	     *
	     * @type {FieldValuesPrerequisite}
	     */
	
	  }, {
	    key: 'fieldValues',
	    get: function get() {
	      return this._fieldValues;
	    }
	
	    /**
	     * Set the field values prerequisite
	     *
	     * @type {FieldValuesPrerequisite}
	     *
	     * @throws {TypeError} when specified value is not a FieldValuesPrerequisite instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _fieldValuesPrerequisite2.default)) {
	        throw new TypeError('Value must be a FieldValuesPrerequisite object');
	      }
	      this._fieldValues = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      var obj = data || {};
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.sectionsCompleted = _sectionsPrerequisite2.default.fromJSON(obj.sectionsCompleted);
	      obj.stepsCompleted = _stepsPrerequisite2.default.fromJSON(obj.stepsCompleted);
	      obj.fieldsCompleted = _fieldsPrerequisite2.default.fromJSON(obj.fieldsCompleted);
	      obj.fieldValues = _fieldValuesPrerequisite2.default.fromJSON(obj.fieldValues);
	
	      var instance = new Prerequisites();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Prerequisites;
	}(_jsonConvertable2.default);
	
	exports.default = Prerequisites;
	module.exports = exports['default'];

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _field = __webpack_require__(7);
	
	var _field2 = _interopRequireDefault(_field);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object representing prerequisites defined on Fields
	 */
	var FieldsPrerequisite = function (_JsonConvertable) {
	  _inherits(FieldsPrerequisite, _JsonConvertable);
	
	  /**
	   * Creates a new FieldsPrerequisite instance
	   */
	  function FieldsPrerequisite() {
	    _classCallCheck(this, FieldsPrerequisite);
	
	    var _this = _possibleConstructorReturn(this, (FieldsPrerequisite.__proto__ || Object.getPrototypeOf(FieldsPrerequisite)).call(this));
	
	    _this._logical;
	    _this._fields = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the prerequisite combination logic
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldsPrerequisite, [{
	    key: 'addField',
	
	
	    /**
	     * Adds the name of a Field to the prerequisites completion list
	     *
	     * @param {!Field} field The Field to add to the completion list
	     * @throws {TypeError} when specified value is not a Field
	     */
	    value: function addField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	      this.addFieldId(field.name);
	    }
	
	    /**
	     * Adds a field name to the prerequisites completion list
	     *
	     * @param {!string} fieldName The Field name to add to the completion list
	     * @throws {Error} The fieldName is already added to the list
	     * @throws {TypeError} when specified value is not a String
	     */
	
	  }, {
	    key: 'addFieldId',
	    value: function addFieldId(fieldName) {
	      if (!_typecheck2.default.isString(fieldName)) {
	        throw new TypeError('Value must be String');
	      }
	      if (this._fields.indexOf(fieldName) !== -1) {
	        throw new Error('Field name ' + fieldName + ' is already added');
	      }
	
	      this._fields.push(fieldName);
	    }
	
	    /**
	     * Remove a Field from the prerequisites completion list
	     *
	     * @param {!Field} field The Field object to remove from the completion list
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'removeField',
	    value: function removeField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	      this.removeFieldId(field.name);
	    }
	
	    /**
	     * Remove a Field name from the prerequisites completion list
	     *
	     * @param {!string} fieldName The Field name to remove from the completion list
	     * @throws {Error} A Field with the given name doesn't exist in the list
	     * @throws {TypeError} when specified value is not a string
	     */
	
	  }, {
	    key: 'removeFieldId',
	    value: function removeFieldId(fieldName) {
	      if (!_typecheck2.default.isString(fieldName)) {
	        throw new TypeError('Value must be String');
	      }
	
	      var index = this._fields.findIndex(function (s) {
	        return s === fieldName;
	      });
	      if (index === -1) {
	        throw new Error('A Field with id ' + fieldName + ' doesn\'t exist');
	      }
	
	      this._fields.splice(index, 1);
	    }
	
	    /**
	     * Exports the FieldsPrerequisite as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the Fields Prerequisite
	     *
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        logical: this._logical,
	        fields: this._fields && this._fields.length > 0 ? this._fields : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'logical',
	    get: function get() {
	      return this._logical;
	    }
	
	    /**
	     * Set the prerequisite comibination logic
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._logical = value;
	    }
	
	    /**
	     * Gets the entire collection of Field names
	     *
	     * @type {string[]}
	     */
	
	  }, {
	    key: 'fields',
	    get: function get() {
	      return this._fields;
	    }
	
	    /**
	     * Sets the entire collection of Field names,
	     * overriding the existing collection.
	     *
	     * @type {string[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Strings
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, _typecheck2.default.isString)) {
	        throw new TypeError('Value must be an array of Strings');
	      }
	      this._fields = value;
	    }
	  }]);
	
	  return FieldsPrerequisite;
	}(_jsonConvertable2.default);
	
	exports.default = FieldsPrerequisite;
	module.exports = exports['default'];

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _fieldValueOperand = __webpack_require__(19);
	
	var _fieldValueOperand2 = _interopRequireDefault(_fieldValueOperand);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object representing prerequisites defined on FieldValues
	 */
	var FieldValuesPrerequisite = function (_JsonConvertable) {
	  _inherits(FieldValuesPrerequisite, _JsonConvertable);
	
	  /**
	   * Creates a new FieldValuesPrerequisite instance
	   */
	  function FieldValuesPrerequisite() {
	    _classCallCheck(this, FieldValuesPrerequisite);
	
	    var _this = _possibleConstructorReturn(this, (FieldValuesPrerequisite.__proto__ || Object.getPrototypeOf(FieldValuesPrerequisite)).call(this));
	
	    _this._logical;
	    _this._operands = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the prerequisite combination logic
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldValuesPrerequisite, [{
	    key: 'addOperand',
	
	
	    /**
	     * Adds a FieldValueOperand object to the Form
	     *
	     * @param {FieldValueOperand} operand The FieldValueOperand object to add to the prerequisite
	     * @throws {TypeError} when specified value is not a FieldValueOperand
	     */
	    value: function addOperand(operand) {
	      if (operand && !(operand instanceof _fieldValueOperand2.default)) {
	        throw new TypeError('Value must be FieldValueOperand');
	      }
	
	      this._operands.push(operand);
	    }
	
	    /**
	     * Remove a FieldValueOperand object from the Form
	     *
	     * @param {FieldValueOperand} operand The Operand object to remove from the prerequisites
	     * @throws {Error} The operand doesn't exist in the prerequisite
	     * @throws {TypeError} when specified value is not a FieldValueOperand
	     */
	
	  }, {
	    key: 'removeOperand',
	    value: function removeOperand(operand) {
	      if (operand && !(operand instanceof _fieldValueOperand2.default)) {
	        throw new TypeError('Value must be FieldValueOperand');
	      }
	
	      var index = this._operands.findIndex(function (o) {
	        return o === operand;
	      });
	      if (index === -1) {
	        throw new Error("The Operand doesn't exist");
	      }
	
	      this._operands.splice(index, 1);
	    }
	
	    /**
	     * Exports the FieldValuesPrerequisite as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the FieldValues Prerequisite
	     *
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        logical: this._logical,
	        operands: this._operands && this._operands.length > 0 ? this._operands.map(function (o) {
	          return o.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Create a FieldValuePrerequisite instance from a JSON string
	     *
	     * @param {String|Object} [data] A JSON string or Object literal describing the Field Value Prerequisite
	     * @return {FieldValuePrerequisite} A FieldValuePrerequisite instance containing the data from the JSON string
	     *
	     */
	
	  }, {
	    key: 'logical',
	    get: function get() {
	      return this._logical;
	    }
	
	    /**
	     * Set the prerequisite combination logic
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._logical = value;
	    }
	
	    /**
	     * Gets the entire collection of Field Value Operands
	     *
	     * @type {FieldValueOperand[]}
	     */
	
	  }, {
	    key: 'operands',
	    get: function get() {
	      return this._operands;
	    }
	
	    /**
	     * Sets the entire collection of Field Value Operands,
	     * overriding the existing collection.
	     *
	     * @type {FieldValueOperand[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Field Value Operands
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _fieldValueOperand2.default;
	      })) {
	        throw new TypeError('Value must be an array of FieldValueOperands');
	      }
	      this._operands = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      var obj = data || {};
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.operands = obj.operands || [];
	      obj.operands = obj.operands.map(function (o) {
	        return _fieldValueOperand2.default.fromJSON(o);
	      });
	
	      var instance = new FieldValuesPrerequisite();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return FieldValuesPrerequisite;
	}(_jsonConvertable2.default);
	
	exports.default = FieldValuesPrerequisite;
	module.exports = exports['default'];

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A object containing an operand and value to validate fields
	 */
	var FieldValueOperand = function (_JsonConvertable) {
	  _inherits(FieldValueOperand, _JsonConvertable);
	
	  /**
	   * Creates a new FieldValueOperand instance
	   */
	  function FieldValueOperand() {
	    _classCallCheck(this, FieldValueOperand);
	
	    var _this = _possibleConstructorReturn(this, (FieldValueOperand.__proto__ || Object.getPrototypeOf(FieldValueOperand)).call(this));
	
	    _this._name;
	    _this._value;
	    _this._operator;
	    return _this;
	  }
	
	  /**
	   * Retrieve the field name to check
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FieldValueOperand, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FieldValueOperand as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        name: this._name,
	        value: this._value,
	        operator: this._operator
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'name',
	    get: function get() {
	      return this._name;
	    }
	
	    /**
	     * Set the field name to check
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._name = value;
	    }
	
	    /**
	     * Retrieve the value to check for
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'value',
	    get: function get() {
	      return this._value;
	    }
	
	    /**
	     * Set the value content to check for
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._value = value;
	    }
	
	    /**
	     * Retrieve the operator to check for
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'operator',
	    get: function get() {
	      return this._operator;
	    }
	
	    /**
	     * Set the operator content to check for
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified operator is not a string
	     */
	    ,
	    set: function set(operator) {
	      if (operator && !_typecheck2.default.isString(operator)) {
	        throw new TypeError('Operator must be a string');
	      }
	      this._operator = operator;
	    }
	  }]);
	
	  return FieldValueOperand;
	}(_jsonConvertable2.default);
	
	exports.default = FieldValueOperand;
	module.exports = exports['default'];

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _section = __webpack_require__(21);
	
	var _section2 = _interopRequireDefault(_section);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object representing prerequisites defined on Sections
	 */
	var SectionsPrerequisite = function (_JsonConvertable) {
	  _inherits(SectionsPrerequisite, _JsonConvertable);
	
	  /**
	   * Creates a new SectionsPrerequisite instance
	   */
	  function SectionsPrerequisite() {
	    _classCallCheck(this, SectionsPrerequisite);
	
	    var _this = _possibleConstructorReturn(this, (SectionsPrerequisite.__proto__ || Object.getPrototypeOf(SectionsPrerequisite)).call(this));
	
	    _this._logical;
	    _this._sections = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the prerequisite combination logic
	   *
	   * @type {string}
	   */
	
	
	  _createClass(SectionsPrerequisite, [{
	    key: 'addSection',
	
	
	    /**
	     * Adds the ID of a Section to the prerequisites completion list
	     *
	     * @param {!Section} section The Section to add to the completion list
	     * @throws {TypeError} when specified value is not a Section
	     */
	    value: function addSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	      this.addSectionId(section.id);
	    }
	
	    /**
	     * Adds a section ID to the prerequisites completion list
	     *
	     * @param {!string} sectionId The Section ID to add to the completion list
	     * @throws {Error} The sectionId is already added to the list
	     * @throws {TypeError} when specified value is not a String
	     */
	
	  }, {
	    key: 'addSectionId',
	    value: function addSectionId(sectionId) {
	      if (!_typecheck2.default.isString(sectionId)) {
	        throw new TypeError('Value must be String');
	      }
	      if (this._sections.indexOf(sectionId) !== -1) {
	        throw new Error('Section ID ' + sectionId + ' is already added');
	      }
	
	      this._sections.push(sectionId);
	    }
	
	    /**
	     * Remove a Section from the prerequisites completion list
	     *
	     * @param {!Section} section The Section object to remove from the completion list
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'removeSection',
	    value: function removeSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	      this.removeSectionId(section.id);
	    }
	
	    /**
	     * Remove a Section ID from the prerequisites completion list
	     *
	     * @param {!string} sectionId The Section ID to remove from the completion list
	     * @throws {Error} A Section with the given ID doesn't exist in the list
	     * @throws {TypeError} when specified value is not a string
	     */
	
	  }, {
	    key: 'removeSectionId',
	    value: function removeSectionId(sectionId) {
	      if (!_typecheck2.default.isString(sectionId)) {
	        throw new TypeError('Value must be String');
	      }
	
	      var index = this._sections.findIndex(function (s) {
	        return s === sectionId;
	      });
	      if (index === -1) {
	        throw new Error('A Section with id ' + sectionId + ' doesn\'t exist');
	      }
	
	      this._sections.splice(index, 1);
	    }
	
	    /**
	     * Exports the SectionsPrerequisite as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the Sections Prerequisite
	     *
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        logical: this._logical,
	        sections: this._sections && this._sections.length > 0 ? this._sections : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'logical',
	    get: function get() {
	      return this._logical;
	    }
	
	    /**
	     * Set the prerequisite comibination logic
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._logical = value;
	    }
	
	    /**
	     * Gets the entire collection of Section ids
	     *
	     * @type {string[]}
	     */
	
	  }, {
	    key: 'sections',
	    get: function get() {
	      return this._sections;
	    }
	
	    /**
	     * Sets the entire collection of Section ids,
	     * overriding the existing collection.
	     *
	     * @type {string[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Strings
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, _typecheck2.default.isString)) {
	        throw new TypeError('Value must be an array of Strings');
	      }
	      this._sections = value;
	    }
	  }]);
	
	  return SectionsPrerequisite;
	}(_jsonConvertable2.default);
	
	exports.default = SectionsPrerequisite;
	module.exports = exports['default'];

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _field = __webpack_require__(7);
	
	var _field2 = _interopRequireDefault(_field);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _prerequisites = __webpack_require__(16);
	
	var _prerequisites2 = _interopRequireDefault(_prerequisites);
	
	var _sectionState = __webpack_require__(22);
	
	var _sectionState2 = _interopRequireDefault(_sectionState);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * Defines a Section for use in a Form object
	 */
	var Section = function (_JsonConvertable) {
	  _inherits(Section, _JsonConvertable);
	
	  /**
	   * Creates an instance of Section.
	   *
	   * @param {string} [id] The ID of the Section instance
	   *
	   * @throws {Error} when specified value is null
	   * @throws {TypeError} when specified value is not a string
	   */
	  function Section(id) {
	    _classCallCheck(this, Section);
	
	    var _this = _possibleConstructorReturn(this, (Section.__proto__ || Object.getPrototypeOf(Section)).call(this));
	
	    if (id == null) {
	      throw new Error('Section must have an id');
	    }
	    if (id && !_typecheck2.default.isString(id)) {
	      throw new TypeError('Section id must be a string');
	    }
	
	    _this._id = id;
	    _this._title;
	    _this._subtitle;
	    _this._state = new _sectionState2.default();
	    _this._prerequisites = new _prerequisites2.default();
	    _this._fields = [];
	    return _this;
	  }
	
	  /**
	   * Gets the ID
	   *
	   * @type {string}
	   */
	
	
	  _createClass(Section, [{
	    key: 'addField',
	
	
	    /**
	     * Adds a Field object to the Section
	     *
	     * @param {Field} field The Field object to add to the section
	     * @throws {Error} A Field with the same name already exists
	     * @throws {TypeError} when specified value is not a Field
	     */
	    value: function addField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	      if (this.getField(field.name) !== undefined) {
	        throw new Error('A Field with name ' + field.name + ' already exists');
	      }
	
	      this._fields.push(field);
	    }
	
	    /**
	     * Returns the Field object with the specified name
	     *
	     * @param {string} fieldName The name of the Field to retrieve
	     * @return {Field} The Field with the specified name.
	     *                undefined if a field with the specified name doesn't exist.
	     */
	
	  }, {
	    key: 'getField',
	    value: function getField(fieldName) {
	      return this._fields.find(function (f) {
	        return f.name === fieldName;
	      });
	    }
	
	    /**
	     * Update a Field object from the Section
	     *
	     * @param {Field} field The Field object to update
	     * @throws {Error} A Field with the given name doesn't exist in the section
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'updateField',
	    value: function updateField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields[index] = field;
	    }
	
	    /**
	     * Remove a Field object from the Section
	     *
	     * @param {Field} field The Field object to remove from the section
	     * @throws {Error} A Field with the given name doesn't exist in the section
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'removeField',
	    value: function removeField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields.splice(index, 1);
	    }
	
	    /**
	     * Moves a Field object from the section to a specific index
	     *
	     * @param {Field} field The Field object to move
	     * @param {Number} index The index to move the field to
	     * @throws {Error} the specified field doesn't exist in the section or the index is out of bounds
	     * @throws {TypeError} when specified field is not a Field or specified index is not a number
	     */
	
	  }, {
	    key: 'moveFieldToIndex',
	    value: function moveFieldToIndex(field, index) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('field must be Field');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._fields.indexOf(field) === -1) {
	        throw new Error('The specified field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._fields.length - 1) {
	        throw new Error('The specified index is outside the bounds of the fields array');
	      }
	      this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
	    }
	
	    /**
	     * Convert the Section to a plain Javascript object.
	     *
	     * @return {Object}
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        id: this._id,
	        title: this._title,
	        subtitle: this._subtitle,
	        state: this._state && this._state.toJSON(),
	        prerequisites: this._prerequisites && this._prerequisites.toJSON(),
	        fields: this._fields && this._fields.length > 0 ? this._fields.map(function (f) {
	          return f.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a Section instance.
	     *
	     * @param {Object|string} [data]
	     * @return {Section} A section instance containing the specified data
	     */
	
	  }, {
	    key: 'id',
	    get: function get() {
	      return this._id;
	    }
	
	    /**
	     * Gets the subtitle content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'subtitle',
	    get: function get() {
	      return this._subtitle;
	    }
	
	    /**
	     * Sets the subtitle content
	     *
	     * @type {string}
	     */
	    ,
	    set: function set(value) {
	      this._subtitle = value;
	    }
	
	    /**
	     * Gets the title content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'title',
	    get: function get() {
	      return this._title;
	    }
	
	    /**
	     * Sets the title content
	     *
	     * @type {string}
	     */
	    ,
	    set: function set(value) {
	      this._title = value;
	    }
	
	    /**
	     * Gets the state
	     *
	     * @type {SectionState}
	     */
	
	  }, {
	    key: 'state',
	    get: function get() {
	      return this._state;
	    }
	
	    /**
	     * Sets the state
	     *
	     * @type {SectionState}
	     *
	     * @throws {TypeError} when specified value is not a SectionState object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _sectionState2.default)) {
	        throw new TypeError('Value must be a SectionState object');
	      }
	      this._state = value;
	    }
	
	    /**
	     * Gets the prerequisites
	     *
	     * @type {Prerequisites}
	     */
	
	  }, {
	    key: 'prerequisites',
	    get: function get() {
	      return this._prerequisites;
	    }
	
	    /**
	     * Sets the prerequisites
	     *
	     * @type {Prerequisites}
	     *
	     * @throws {TypeError} when specified value is not a Prerequisites object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _prerequisites2.default)) {
	        throw new TypeError('Value must be a Prerequisites object');
	      }
	      this._prerequisites = value;
	    }
	
	    /**
	     * Gets the entire collection of Field objects
	     *
	     * @type {Field[]}
	     */
	
	  }, {
	    key: 'fields',
	    get: function get() {
	      return this._fields;
	    }
	
	    /**
	     * Sets the entire collection of Field objects,
	     * overriding the existing collection.
	     *
	     * @type {Field[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Fields
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _field2.default;
	      })) {
	        throw new TypeError('Value must be an array of Fields');
	      }
	      this._fields = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.state = _sectionState2.default.fromJSON(obj.state);
	      obj.prerequisites = _prerequisites2.default.fromJSON(obj.prerequisites);
	
	      obj.fields = obj.fields || [];
	      obj.fields = obj.fields.map(function (f) {
	        return _field2.default.fromJSON(f);
	      });
	
	      var instance = new Section(obj.id);
	
	      delete obj.id;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Section;
	}(_jsonConvertable2.default);
	
	exports.default = Section;
	module.exports = exports['default'];

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A state object containig status fields of a Section
	 */
	var SectionState = function (_JsonConvertable) {
	  _inherits(SectionState, _JsonConvertable);
	
	  /**
	   * Creates an instance of SectionState.
	   */
	  function SectionState() {
	    _classCallCheck(this, SectionState);
	
	    var _this = _possibleConstructorReturn(this, (SectionState.__proto__ || Object.getPrototypeOf(SectionState)).call(this));
	
	    _this._editable;
	    _this._editMode;
	    _this._externalUrl;
	    _this._collapsible;
	    _this._collapsed;
	    return _this;
	  }
	
	  /**
	   * Retrieve the collapsed flag
	   *
	   * @type {boolean}
	   */
	
	
	  _createClass(SectionState, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the SectionState as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript object.
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        editable: this.editable,
	        editMode: this.editMode,
	        externalUrl: this.externalUrl,
	        collapsed: this.collapsed,
	        collapsible: this.collapsible
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'collapsed',
	    get: function get() {
	      return this._collapsed;
	    }
	
	    /**
	     * Set the collapsed flag
	     *
	     * @type {boolean}
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	
	      this._collapsed = value;
	    }
	
	    /**
	     * Retrieve the collapsible flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'collapsible',
	    get: function get() {
	      return this._collapsible;
	    }
	
	    /**
	     * Set the collapsible flag
	     *
	     * @type {boolean}
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	
	      this._collapsible = value;
	    }
	
	    /**
	     * Retrieve the editable flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'editable',
	    get: function get() {
	      return this._editable;
	    }
	
	    /**
	     * Set the editable flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	
	      this._editable = value;
	    }
	
	    /**
	     * Retrieve the editMode flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'editMode',
	    get: function get() {
	      return this._editMode;
	    }
	
	    /**
	     * Set the editMode flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._editMode = value;
	    }
	
	    /**
	     * Retrieve the externalUrl
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'externalUrl',
	    get: function get() {
	      return this._externalUrl;
	    }
	
	    /**
	     * Set the externalUrl
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._externalUrl = value;
	    }
	  }]);
	
	  return SectionState;
	}(_jsonConvertable2.default);
	
	exports.default = SectionState;
	module.exports = exports['default'];

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _step = __webpack_require__(24);
	
	var _step2 = _interopRequireDefault(_step);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * An object representing prerequisites defined on Steps
	 */
	var StepsPrerequisite = function (_JsonConvertable) {
	  _inherits(StepsPrerequisite, _JsonConvertable);
	
	  /**
	   * Creates a new StepsPrerequisite instance
	   */
	  function StepsPrerequisite() {
	    _classCallCheck(this, StepsPrerequisite);
	
	    var _this = _possibleConstructorReturn(this, (StepsPrerequisite.__proto__ || Object.getPrototypeOf(StepsPrerequisite)).call(this));
	
	    _this._logical;
	    _this._steps = [];
	    return _this;
	  }
	
	  /**
	   * Retrieve the prerequisite combination logic
	   *
	   * @type {string}
	   */
	
	
	  _createClass(StepsPrerequisite, [{
	    key: 'addStep',
	
	
	    /**
	     * Adds the ID of a Step to the prerequisites completion list
	     *
	     * @param {!Step} step The Step to add to the completion list
	     * @throws {TypeError} when specified value is not a Step
	     */
	    value: function addStep(step) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('Value must be Step');
	      }
	      this.addStepId(step.id);
	    }
	
	    /**
	     * Adds a step ID to the prerequisites completion list
	     *
	     * @param {!string} stepId The Step ID to add to the completion list
	     * @throws {Error} The stepId is already added to the list
	     * @throws {TypeError} when specified value is not a String
	     */
	
	  }, {
	    key: 'addStepId',
	    value: function addStepId(stepId) {
	      if (!_typecheck2.default.isString(stepId)) {
	        throw new TypeError('Value must be String');
	      }
	      if (this._steps.indexOf(stepId) !== -1) {
	        throw new Error('Step ID ' + stepId + ' is already added');
	      }
	
	      this._steps.push(stepId);
	    }
	
	    /**
	     * Remove a Step from the prerequisites completion list
	     *
	     * @param {!Step} step The Step object to remove from the completion list
	     * @throws {TypeError} when specified value is not a Step
	     */
	
	  }, {
	    key: 'removeStep',
	    value: function removeStep(step) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('Value must be Step');
	      }
	      this.removeStepId(step.id);
	    }
	
	    /**
	     * Remove a Step ID from the prerequisites completion list
	     *
	     * @param {!string} stepId The Step ID to remove from the completion list
	     * @throws {Error} A Step with the given ID doesn't exist in the list
	     * @throws {TypeError} when specified value is not a string
	     */
	
	  }, {
	    key: 'removeStepId',
	    value: function removeStepId(stepId) {
	      if (!_typecheck2.default.isString(stepId)) {
	        throw new TypeError('Value must be String');
	      }
	
	      var index = this._steps.findIndex(function (s) {
	        return s === stepId;
	      });
	      if (index === -1) {
	        throw new Error('A Step with id ' + stepId + ' doesn\'t exist');
	      }
	
	      this._steps.splice(index, 1);
	    }
	
	    /**
	     * Exports the StepsPrerequisite as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the Steps Prerequisite
	     *
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        logical: this._logical,
	        steps: this._steps && this._steps.length > 0 ? this._steps : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'logical',
	    get: function get() {
	      return this._logical;
	    }
	
	    /**
	     * Set the prerequisite comibination logic
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._logical = value;
	    }
	
	    /**
	     * Gets the entire collection of Step ids
	     *
	     * @type {string[]}
	     */
	
	  }, {
	    key: 'steps',
	    get: function get() {
	      return this._steps;
	    }
	
	    /**
	     * Sets the entire collection of Step ids,
	     * overriding the existing collection.
	     *
	     * @type {string[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Strings
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, _typecheck2.default.isString)) {
	        throw new TypeError('Value must be an array of Strings');
	      }
	      this._steps = value;
	    }
	  }]);
	
	  return StepsPrerequisite;
	}(_jsonConvertable2.default);
	
	exports.default = StepsPrerequisite;
	module.exports = exports['default'];

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _field = __webpack_require__(7);
	
	var _field2 = _interopRequireDefault(_field);
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _prerequisites = __webpack_require__(16);
	
	var _prerequisites2 = _interopRequireDefault(_prerequisites);
	
	var _navigationTexts = __webpack_require__(25);
	
	var _navigationTexts2 = _interopRequireDefault(_navigationTexts);
	
	var _section = __webpack_require__(21);
	
	var _section2 = _interopRequireDefault(_section);
	
	var _stepState = __webpack_require__(26);
	
	var _stepState2 = _interopRequireDefault(_stepState);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * Defines a Step for use inside a Form object
	 */
	var Step = function (_JsonConvertable) {
	  _inherits(Step, _JsonConvertable);
	
	  /**
	   * Creates an instance of Step.
	   *
	   * @param {string} id The ID of the Step
	   *
	   * @throws {Error} when specified value is null
	   * @throws {TypeError} when specified value is not a string
	   */
	  function Step(id) {
	    _classCallCheck(this, Step);
	
	    var _this = _possibleConstructorReturn(this, (Step.__proto__ || Object.getPrototypeOf(Step)).call(this));
	
	    if (id == null) {
	      throw new Error('Step must have an id');
	    }
	    if (id && !_typecheck2.default.isString(id)) {
	      throw new TypeError('Step id must be a string');
	    }
	
	    _this._id = id;
	    _this._title;
	    _this._subtitle;
	    _this._body;
	    _this._type;
	    _this._state = new _stepState2.default();
	    _this._prerequisites = new _prerequisites2.default();
	    _this._navigationTexts = new _navigationTexts2.default();
	    _this._sections = [];
	    _this._fields = [];
	    return _this;
	  }
	
	  /**
	   * Gets the ID
	   *
	   * @type {string}
	   */
	
	
	  _createClass(Step, [{
	    key: 'addSection',
	
	
	    /**
	     * Adds a Section object to the Step
	     *
	     * @param {Section} section The Section object to add to the step
	     * @throws {Error} A Section with the same ID already exists
	     * @throws {TypeError} when specified value is not a Section
	     */
	    value: function addSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	      if (this.getSection(section.id) !== undefined) {
	        throw new Error('A Section with id ' + section.id + ' already exists');
	      }
	
	      this._sections.push(section);
	    }
	
	    /**
	     * Returns the Section object with the specified ID
	     *
	     * @param {string} sectionId The ID of the Section to retrieve
	     * @return {Section} The Section with the specified ID.
	     *                undefined if a section with the specified ID doesn't exist.
	     */
	
	  }, {
	    key: 'getSection',
	    value: function getSection(sectionId) {
	      return this._sections.find(function (s) {
	        return s.id === sectionId;
	      });
	    }
	
	    /**
	     * Update a Section object from the Step
	     *
	     * @param {Section} section The Section object to update
	     * @throws {Error} A Section with the given ID doesn't exist in the step
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'updateSection',
	    value: function updateSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	
	      var index = this._sections.findIndex(function (s) {
	        return s.id === section.id;
	      });
	      if (index === -1) {
	        throw new Error('A Section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      this._sections[index] = section;
	    }
	
	    /**
	     * Remove a Section object from the Step
	     *
	     * @param {Section} section The Section object to remove from the step
	     * @throws {Error} A Section with the given ID doesn't exist in the step
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'removeSection',
	    value: function removeSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	
	      var index = this._sections.findIndex(function (s) {
	        return s.id === section.id;
	      });
	      if (index === -1) {
	        throw new Error('A Section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      this._sections.splice(index, 1);
	    }
	
	    /**
	     * Moves a Section object from the Step to a specific index
	     *
	     * @param {Section} section The Section object to move
	     * @param {Number} index The index to move the section to
	     * @throws {Error} the specified section doesn't exist in the step or the index is out of bounds
	     * @throws {TypeError} when specified section is not a Section or specified index is not a number
	     */
	
	  }, {
	    key: 'moveSectionToIndex',
	    value: function moveSectionToIndex(section, index) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('section must be Section');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._sections.indexOf(section) === -1) {
	        throw new Error('The specified section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._sections.length - 1) {
	        throw new Error('The specified index is outside the bounds of the sections array');
	      }
	      this._sections.splice(index, 0, this._sections.splice(this._sections.indexOf(section), 1)[0]);
	    }
	
	    /**
	     * Adds a Field object to the Step
	     *
	     * @param {Field} field The Field object to add to the step
	     * @throws {Error} A Field with the same name already exists
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'addField',
	    value: function addField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	      if (this.getField(field.name) !== undefined) {
	        throw new Error('A Field with name ' + field.name + ' already exists');
	      }
	
	      this._fields.push(field);
	    }
	
	    /**
	     * Returns the Field object with the specified name
	     *
	     * @param {string} fieldName The name of the Field to retrieve
	     * @return {Field} The Field with the specified name.
	     *                undefined if a field with the specified name doesn't exist.
	     */
	
	  }, {
	    key: 'getField',
	    value: function getField(fieldName) {
	      return this._fields.find(function (f) {
	        return f.name === fieldName;
	      });
	    }
	
	    /**
	     * Update a Field object from the Step
	     *
	     * @param {Field} field The Field object to update
	     * @throws {Error} A Field with the given name doesn't exist in the step
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'updateField',
	    value: function updateField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields[index] = field;
	    }
	
	    /**
	     * Remove a Field object from the Step
	     *
	     * @param {Field} field The Field object to remove from the step
	     * @throws {Error} A Field with the given name doesn't exist in the step
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'removeField',
	    value: function removeField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields.splice(index, 1);
	    }
	
	    /**
	     * Moves a Field object from the Step to a specific index
	     *
	     * @param {Field} field The Field object to move
	     * @param {Number} index The index to move the field to
	     * @throws {Error} the specified field doesn't exist in the step or the index is out of bounds
	     * @throws {TypeError} when specified field is not a Field or specified index is not a number
	     */
	
	  }, {
	    key: 'moveFieldToIndex',
	    value: function moveFieldToIndex(field, index) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('field must be Field');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._fields.indexOf(field) === -1) {
	        throw new Error('The specified field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._fields.length - 1) {
	        throw new Error('The specified index is outside the bounds of the fields array');
	      }
	      this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
	    }
	
	    /**
	     * Exports the Step as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        id: this._id,
	        title: this._title,
	        subtitle: this._subtitle,
	        body: this._body,
	        type: this._type,
	        state: this._state && this._state.toJSON(),
	        prerequisites: this._prerequisites && this._prerequisites.toJSON(),
	        navigationTexts: this._navigationTexts && this._navigationTexts.toJSON(),
	        sections: this._sections && this._sections.length > 0 ? this._sections.map(function (f) {
	          return f.toJSON();
	        }) : null,
	        fields: this._fields && this._fields.length > 0 ? this._fields.map(function (f) {
	          return f.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Convert the specified JSON to a Step instance.
	     *
	     * @param {Object|string} [data]
	     * @return {Step} A step instance containing the specified data
	     */
	
	  }, {
	    key: 'id',
	    get: function get() {
	      return this._id;
	    }
	
	    /**
	     * Gets the title content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'title',
	    get: function get() {
	      return this._title;
	    }
	
	    /**
	     * Sets the title content
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._title = value;
	    }
	
	    /**
	     * Gets the subtitle content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'subtitle',
	    get: function get() {
	      return this._subtitle;
	    }
	
	    /**
	     * Sets the subtitle content
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._subtitle = value;
	    }
	
	    /**
	     * Gets the body content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'body',
	    get: function get() {
	      return this._body;
	    }
	
	    /**
	     * Sets the body content
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._body = value;
	    }
	
	    /**
	     * Gets the type
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'type',
	    get: function get() {
	      return this._type;
	    }
	
	    /**
	     * Sets the type
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._type = value;
	    }
	
	    /**
	     * Gets the state
	     *
	     * @type {StepState}
	     */
	
	  }, {
	    key: 'state',
	    get: function get() {
	      return this._state;
	    }
	
	    /**
	     * Sets the state
	     *
	     * @type {StepState}
	     *
	     * @throws {TypeError} when specified value is not a StepState object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _stepState2.default)) {
	        throw new TypeError('Value must be a StepState object');
	      }
	      this._state = value;
	    }
	
	    /**
	     * Gets the prerequisites
	     *
	     * @type {Prerequisites}
	     */
	
	  }, {
	    key: 'prerequisites',
	    get: function get() {
	      return this._prerequisites;
	    }
	
	    /**
	     * Sets the prerequisites
	     *
	     * @type {Prerequisites}
	     *
	     * @throws {TypeError} when specified value is not a Prerequisites object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _prerequisites2.default)) {
	        throw new TypeError('Value must be a Prerequisites object');
	      }
	      this._prerequisites = value;
	    }
	
	    /**
	     * Retrieve the navigation texts
	     *
	     * @type {NavigationTexts}
	     */
	
	  }, {
	    key: 'navigationTexts',
	    get: function get() {
	      return this._navigationTexts;
	    }
	
	    /**
	     * Sets the navigation texts
	     *
	     * @type {NavigationTexts}
	     *
	     * @throws {TypeError} when specified value is not a NavigationTexts object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _navigationTexts2.default)) {
	        throw new TypeError('Value must be a NavigationTexts object');
	      }
	      this._navigationTexts = value;
	    }
	
	    /**
	     * Gets the entire collection of Section objects
	     *
	     * @type {Section[]}
	     */
	
	  }, {
	    key: 'sections',
	    get: function get() {
	      return this._sections;
	    }
	
	    /**
	     * Sets the entire collection of Section objects,
	     * overriding the existing collection.
	     *
	     * @type {Section[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Sections
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _section2.default;
	      })) {
	        throw new TypeError('Value must be an array of Sections');
	      }
	      this._sections = value;
	    }
	
	    /**
	     * Gets the entire collection of Field objects
	     *
	     * @type {Field[]}
	     */
	
	  }, {
	    key: 'fields',
	    get: function get() {
	      return this._fields;
	    }
	
	    /**
	     * Sets the entire collection of Field objects,
	     * overriding the existing collection.
	     *
	     * @type {Field[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Fields
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _field2.default;
	      })) {
	        throw new TypeError('Value must be an array of Fields');
	      }
	      this._fields = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      if (data == null) {
	        return data;
	      }
	
	      var obj = data;
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.state = _stepState2.default.fromJSON(obj.state);
	      obj.prerequisites = _prerequisites2.default.fromJSON(obj.prerequisites);
	      obj.navigationTexts = _navigationTexts2.default.fromJSON(obj.navigationTexts);
	
	      obj.sections = obj.sections || [];
	      obj.sections = obj.sections.map(function (f) {
	        return _section2.default.fromJSON(f);
	      });
	
	      obj.fields = obj.fields || [];
	      obj.fields = obj.fields.map(function (f) {
	        return _field2.default.fromJSON(f);
	      });
	
	      var instance = new Step(obj.id);
	
	      delete obj.id;
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Step;
	}(_jsonConvertable2.default);
	
	exports.default = Step;
	module.exports = exports['default'];

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	* An object containing the labels for navigation buttons
	*
	*/
	var NavigationTexts = function (_JsonConvertable) {
	  _inherits(NavigationTexts, _JsonConvertable);
	
	  /**
	   * Creates a new NavigationTexts instance
	   */
	  function NavigationTexts() {
	    _classCallCheck(this, NavigationTexts);
	
	    var _this = _possibleConstructorReturn(this, (NavigationTexts.__proto__ || Object.getPrototypeOf(NavigationTexts)).call(this));
	
	    _this._submit;
	    _this._next;
	    _this._previous;
	    _this._concept;
	    return _this;
	  }
	
	  /**
	   * Retrieve the submit label
	   *
	   * @type {string}
	   */
	
	
	  _createClass(NavigationTexts, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the NavigationTexts as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        submit: this._submit,
	        next: this._next,
	        previous: this._previous,
	        concept: this._concept
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'submit',
	    get: function get() {
	      return this._submit;
	    }
	
	    /**
	     * Set the submit label
	     *
	     * @type {?string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._submit = value;
	    }
	
	    /**
	     * Retrieve the next label
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'next',
	    get: function get() {
	      return this._next;
	    }
	
	    /**
	     * Set the next label
	     *
	     * @type {?string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._next = value;
	    }
	
	    /**
	     * Retrieve the previous label
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'previous',
	    get: function get() {
	      return this._previous;
	    }
	
	    /**
	     * Set the previous label
	     *
	     * @type {?string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._previous = value;
	    }
	
	    /**
	     * Retrieve the concept label
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'concept',
	    get: function get() {
	      return this._concept;
	    }
	
	    /**
	     * Set the concept label
	     *
	     * @type {?string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._concept = value;
	    }
	  }]);
	
	  return NavigationTexts;
	}(_jsonConvertable2.default);
	
	exports.default = NavigationTexts;
	module.exports = exports['default'];

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A state object containing status fields of a Step
	 */
	var StepState = function (_JsonConvertable) {
	  _inherits(StepState, _JsonConvertable);
	
	  /**
	   * Creates a new StepState instance
	   */
	  function StepState() {
	    _classCallCheck(this, StepState);
	
	    var _this = _possibleConstructorReturn(this, (StepState.__proto__ || Object.getPrototypeOf(StepState)).call(this));
	
	    _this._showTitle;
	    _this._editable;
	    _this._editMode;
	    _this._externalUrl;
	    return _this;
	  }
	
	  /**
	   * Retrieve the showTitle flag
	   *
	   * @type {boolean}
	   */
	
	
	  _createClass(StepState, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the StepState as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        showTitle: this._showTitle,
	        editable: this._editable,
	        editMode: this._editMode,
	        externalUrl: this._externalUrl
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'showTitle',
	    get: function get() {
	      return this._showTitle;
	    }
	
	    /**
	     * Set the showTitle flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._showTitle = value;
	    }
	
	    /**
	     * Retrieve the editable flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'editable',
	    get: function get() {
	      return this._editable;
	    }
	
	    /**
	     * Set the editable flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._editable = value;
	    }
	
	    /**
	     * Retrieve the editMode flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'editMode',
	    get: function get() {
	      return this._editMode;
	    }
	
	    /**
	     * Set the editMode flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._editMode = value;
	    }
	
	    /**
	     * Retrieve the externalUrl
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'externalUrl',
	    get: function get() {
	      return this._externalUrl;
	    }
	
	    /**
	     * Set the externalUrl
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._externalUrl = value;
	    }
	  }]);
	
	  return StepState;
	}(_jsonConvertable2.default);
	
	exports.default = StepState;
	module.exports = exports['default'];

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _field = __webpack_require__(7);
	
	var _field2 = _interopRequireDefault(_field);
	
	var _formInfo = __webpack_require__(28);
	
	var _formInfo2 = _interopRequireDefault(_formInfo);
	
	var _navigationTexts = __webpack_require__(25);
	
	var _navigationTexts2 = _interopRequireDefault(_navigationTexts);
	
	var _section = __webpack_require__(21);
	
	var _section2 = _interopRequireDefault(_section);
	
	var _step = __webpack_require__(24);
	
	var _step2 = _interopRequireDefault(_step);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	var _validator = __webpack_require__(6);
	
	var _validator2 = _interopRequireDefault(_validator);
	
	var _validatorFactory = __webpack_require__(15);
	
	var _validatorFactory2 = _interopRequireDefault(_validatorFactory);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * Represents a form based on a JSON template
	 */
	var Form = function () {
	
	  /**
	   * Creates a new instance of Form.
	   * Use {@link FormSdk.createForm} to create a new Form.
	   * @private
	   */
	  function Form() {
	    _classCallCheck(this, Form);
	
	    this._name;
	    this._formId;
	    this._rendererVersion;
	    this._info = new _formInfo2.default();
	    this._canSaveDraft;
	    this._saveOnNavigate;
	    this._navigationTexts = new _navigationTexts2.default();
	    this._steps = [];
	    this._sections = [];
	    this._fields = [];
	    this._validators = [];
	  }
	
	  /**
	   * Sets the specified title in the info object
	   * which can be accessed using form.info.title
	   *
	   * @param {string} title The value to use as title
	   */
	
	
	  _createClass(Form, [{
	    key: 'setTitle',
	    value: function setTitle(title) {
	      this.info.title = title;
	    }
	
	    /**
	     * Sets the specified body in the info object
	     * which can be accessed using form.info.body
	     *
	     * @param {string} body The value to use as body
	     */
	
	  }, {
	    key: 'setBody',
	    value: function setBody(body) {
	      this.info.body = body;
	    }
	
	    /**
	     * Gets the entire collection of Step objects
	     *
	     * @type {Step[]}
	     */
	
	  }, {
	    key: 'addStep',
	
	
	    /**
	     * Adds a Step object to the Form
	     *
	     * @param {Step} step The Step object to add to the form
	     * @throws {Error} A Step with the same ID already exists
	     * @throws {TypeError} when specified value is not a Step
	     */
	    value: function addStep(step) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('Value must be Step');
	      }
	      if (this.getStep(step.id) !== undefined) {
	        throw new Error('A Step with id ' + step.id + ' already exists');
	      }
	
	      this._steps.push(step);
	    }
	
	    /**
	     * Returns the Step object with the specified ID
	     *
	     * @param {string} stepId The ID of the Step to retrieve
	     * @return {Step} The Step with the specified ID.
	     *                undefined if a step with the specified ID doesn't exist.
	     */
	
	  }, {
	    key: 'getStep',
	    value: function getStep(stepId) {
	      return this._steps.find(function (s) {
	        return s.id === stepId;
	      });
	    }
	
	    /**
	     * Update a Step object from the Form
	     *
	     * @param {Step} step The Step object to update
	     * @throws {Error} A Step with the given ID doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Step
	     */
	
	  }, {
	    key: 'updateStep',
	    value: function updateStep(step) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('Value must be Step');
	      }
	
	      var index = this._steps.findIndex(function (s) {
	        return s.id === step.id;
	      });
	      if (index === -1) {
	        throw new Error('A Step with id ' + step.id + ' doesn\'t exist');
	      }
	
	      this._steps[index] = step;
	    }
	
	    /**
	     * Remove a Step object from the Form
	     *
	     * @param {Step} step The Step object to remove from the form
	     * @throws {Error} A Step with the given ID doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Step
	     */
	
	  }, {
	    key: 'removeStep',
	    value: function removeStep(step) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('Value must be Step');
	      }
	
	      var index = this._steps.findIndex(function (s) {
	        return s.id === step.id;
	      });
	      if (index === -1) {
	        throw new Error('A Step with id ' + step.id + ' doesn\'t exist');
	      }
	
	      this._steps.splice(index, 1);
	    }
	
	    /**
	     * Moves a Step object from the Form to a specific index
	     *
	     * @param {Step} step The Step object to move
	     * @param {Number} index The index to move the step to
	     * @throws {Error} the specified step doesn't exist in the form or the index is out of bounds
	     * @throws {TypeError} when specified step is not a Step or specified index is not a number
	     */
	
	  }, {
	    key: 'moveStepToIndex',
	    value: function moveStepToIndex(step, index) {
	      if (step && !(step instanceof _step2.default)) {
	        throw new TypeError('step must be Step');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._steps.indexOf(step) === -1) {
	        throw new Error('The specified step with id ' + step.id + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._steps.length - 1) {
	        throw new Error('The specified index is outside the bounds of the steps array');
	      }
	      this._steps.splice(index, 0, this._steps.splice(this._steps.indexOf(step), 1)[0]);
	    }
	
	    /**
	     * Adds a Section object to the Form
	     *
	     * @param {Section} section The Section object to add to the form
	     * @throws {Error} A Section with the same ID already exists
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'addSection',
	    value: function addSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	      if (this.getSection(section.id) !== undefined) {
	        throw new Error('A Section with id ' + section.id + ' already exists');
	      }
	
	      this._sections.push(section);
	    }
	
	    /**
	     * Returns the Section object with the specified ID
	     *
	     * @param {string} sectionId The ID of the Section to retrieve
	     * @return {Section} The Section with the specified ID.
	     *                undefined if a section with the specified ID doesn't exist.
	     */
	
	  }, {
	    key: 'getSection',
	    value: function getSection(sectionId) {
	      return this._sections.find(function (s) {
	        return s.id === sectionId;
	      });
	    }
	
	    /**
	     * Update a Section object from the Form
	     *
	     * @param {Section} section The Section object to update
	     * @throws {Error} A Section with the given ID doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'updateSection',
	    value: function updateSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	
	      var index = this._sections.findIndex(function (s) {
	        return s.id === section.id;
	      });
	      if (index === -1) {
	        throw new Error('A Section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      this._sections[index] = section;
	    }
	
	    /**
	     * Remove a Section object from the Form
	     *
	     * @param {Section} section The Section object to remove from the form
	     * @throws {Error} A Section with the given ID doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Section
	     */
	
	  }, {
	    key: 'removeSection',
	    value: function removeSection(section) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('Value must be Section');
	      }
	
	      var index = this._sections.findIndex(function (s) {
	        return s.id === section.id;
	      });
	      if (index === -1) {
	        throw new Error('A Section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      this._sections.splice(index, 1);
	    }
	
	    /**
	     * Moves a Section object from the Form to a specific index
	     *
	     * @param {Section} section The Section object to move
	     * @param {Number} index The index to move the section to
	     * @throws {Error} the specified section doesn't exist in the form or the index is out of bounds
	     * @throws {TypeError} when specified section is not a Section or specified index is not a number
	     */
	
	  }, {
	    key: 'moveSectionToIndex',
	    value: function moveSectionToIndex(section, index) {
	      if (section && !(section instanceof _section2.default)) {
	        throw new TypeError('section must be Section');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._sections.indexOf(section) === -1) {
	        throw new Error('The specified section with id ' + section.id + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._sections.length - 1) {
	        throw new Error('The specified index is outside the bounds of the sections array');
	      }
	      this._sections.splice(index, 0, this._sections.splice(this._sections.indexOf(section), 1)[0]);
	    }
	
	    /**
	     * Adds a Field object to the Form
	     *
	     * @param {Field} field The Field object to add to the form
	     * @throws {Error} A Field with the same name already exists
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'addField',
	    value: function addField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	      if (this.getField(field.name) !== undefined) {
	        throw new Error('A Field with name ' + field.name + ' already exists');
	      }
	
	      this._fields.push(field);
	    }
	
	    /**
	     * Returns the Field object with the specified name
	     *
	     * @param {string} fieldName The name of the Field to retrieve
	     * @return {Field} The Field with the specified name.
	     *                undefined if a field with the specified name doesn't exist.
	     */
	
	  }, {
	    key: 'getField',
	    value: function getField(fieldName) {
	      return this._fields.find(function (f) {
	        return f.name === fieldName;
	      });
	    }
	
	    /**
	     * Update a Field object from the Form
	     *
	     * @param {Field} field The Field object to update
	     * @throws {Error} A Field with the given name doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'updateField',
	    value: function updateField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('Value must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields[index] = field;
	    }
	
	    /**
	     * Remove a Field object from the Form
	     *
	     * @param {Field} field The Field object to remove from the form
	     * @throws {Error} A Field with the given name doesn't exist in the form
	     * @throws {TypeError} when specified value is not a Field
	     */
	
	  }, {
	    key: 'removeField',
	    value: function removeField(field) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('field must be Field');
	      }
	
	      var index = this._fields.findIndex(function (f) {
	        return f.name === field.name;
	      });
	      if (index === -1) {
	        throw new Error('A Field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      this._fields.splice(index, 1);
	    }
	
	    /**
	     * Moves a Field object from the Form to a specific index
	     *
	     * @param {Field} field The Field object to move
	     * @param {Number} index The index to move the field to
	     * @throws {Error} the specified field doesn't exist in the form or the index is out of bounds
	     * @throws {TypeError} when specified field is not a Field or specified index is not a number
	     */
	
	  }, {
	    key: 'moveFieldToIndex',
	    value: function moveFieldToIndex(field, index) {
	      if (field && !(field instanceof _field2.default)) {
	        throw new TypeError('field must be Field');
	      }
	
	      if (index && !_typecheck2.default.isNumber(index)) {
	        throw new TypeError('index must be a number');
	      }
	
	      if (this._fields.indexOf(field) === -1) {
	        throw new Error('The specified field with name ' + field.name + ' doesn\'t exist');
	      }
	
	      if (index === undefined || index < 0 || index >= this._fields.length - 1) {
	        throw new Error('The specified index is outside the bounds of the fields array');
	      }
	      this._fields.splice(index, 0, this._fields.splice(this._fields.indexOf(field), 1)[0]);
	    }
	
	    /**
	     * Adds a Validator object to the Form
	     *
	     * @param {Validator} validator The Validator object to add to the Form
	     * @throws {Error} A Validator with the same name already exists
	     * @throws {TypeError} when specified value is not a Validator
	     */
	
	  }, {
	    key: 'addValidator',
	    value: function addValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	      if (this.getValidator(validator.name) !== undefined) {
	        throw new Error('A Validator with name ' + validator.name + ' already exists');
	      }
	
	      this._validators.push(validator);
	    }
	
	    /**
	     * Returns the Validator object with the specified name
	     *
	     * @param {string} validatorName The name of the Validator to retrieve
	     * @return {Validator} The Validator with the specified name.
	     *                undefined if a validator with the specified name doesn't exist.
	     */
	
	  }, {
	    key: 'getValidator',
	    value: function getValidator(validatorName) {
	      return this._validators.find(function (v) {
	        return v.name === validatorName;
	      });
	    }
	
	    /**
	     * Update a Validator object from the Form
	     *
	     * @param {Validator} validator The Validator object to update
	     * @throws {Error} A Validator with the given name doesn't exist in the Form
	     * @throws {TypeError} when specified value is not a Validator
	     */
	
	  }, {
	    key: 'updateValidator',
	    value: function updateValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	
	      var index = this._validators.findIndex(function (v) {
	        return v.name === validator.name;
	      });
	      if (index === -1) {
	        throw new Error('A Validator with name ' + validator.name + ' doesn\'t exist');
	      }
	
	      this._validators[index] = validator;
	    }
	
	    /**
	     * Remove a Validator object from the Form
	     *
	     * @param {Validator} validator The Validator object to remove from the Form
	     * @throws {Error} A Validator with the given name doesn't exist in the Form
	     * @throws {TypeError} when specified value is not a Validator
	     */
	
	  }, {
	    key: 'removeValidator',
	    value: function removeValidator(validator) {
	      if (validator && !(validator instanceof _validator2.default)) {
	        throw new TypeError('Value must be Validator');
	      }
	
	      var index = this._validators.findIndex(function (v) {
	        return v.name === validator.name;
	      });
	      if (index === -1) {
	        throw new Error('A Validator with name ' + validator.name + ' doesn\'t exist');
	      }
	
	      this._validators.splice(index, 1);
	    }
	
	    /**
	     * Exports the Form as a plain Javascript Object
	     *
	     * @return {Object} An Object describing the Form template
	     *
	     */
	
	  }, {
	    key: 'toJSON',
	    value: function toJSON() {
	      var _this = this;
	
	      var obj = {
	        name: this._name,
	        formId: this._formId,
	        rendererVersion: this._rendererVersion,
	        info: this._info && this._info.toJSON(),
	        canSaveDraft: this._canSaveDraft,
	        saveOnNavigate: this._saveOnNavigate,
	        navigationTexts: this._navigationTexts && this._navigationTexts.toJSON(),
	        steps: this._steps && this._steps.length > 0 ? this._steps.map(function (s) {
	          return s.toJSON();
	        }) : null,
	        sections: this._sections && this._sections.length > 0 ? this._sections.map(function (s) {
	          return s.toJSON();
	        }) : null,
	        fields: this._fields && this._fields.length > 0 ? this._fields.map(function (f) {
	          return f.toJSON();
	        }) : null,
	        validators: this._validators && this._validators.length > 0 ? this._validators.map(function (v) {
	          return v.toJSON();
	        }) : null
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this[key];
	      });
	
	      return obj;
	    }
	
	    /**
	     * Create a Form instance from a JSON string
	     *
	     * @param {String|Object} [data] A JSON string or Object literal describing the Form template
	     * @return {Form} A Form instance containing the data from the JSON string
	     *
	     */
	
	  }, {
	    key: 'steps',
	    get: function get() {
	      return this._steps;
	    }
	
	    /**
	     * Sets the entire collection of Step objects,
	     * overriding the existing collection.
	     *
	     * @type {Step[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Steps
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _step2.default;
	      })) {
	        throw new TypeError('Value must be an array of Steps');
	      }
	      this._steps = value;
	    }
	
	    /**
	     * Gets the entire collection of Section objects
	     *
	     * @type {Section[]}
	     */
	
	  }, {
	    key: 'sections',
	    get: function get() {
	      return this._sections;
	    }
	
	    /**
	     * Sets the entire collection of Section objects,
	     * overriding the existing collection.
	     *
	     * @type {Section[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Sections
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _section2.default;
	      })) {
	        throw new TypeError('Value must be an array of Sections');
	      }
	      this._sections = value;
	    }
	
	    /**
	     * Gets the entire collection of Field objects
	     *
	     * @type {Field[]}
	     */
	
	  }, {
	    key: 'fields',
	    get: function get() {
	      return this._fields;
	    }
	
	    /**
	     * Sets the entire collection of Field objects,
	     * overriding the existing collection.
	     *
	     * @type {Field[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Fields
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _field2.default;
	      })) {
	        throw new TypeError('Value must be an array of Fields');
	      }
	      this._fields = value;
	    }
	
	    /**
	     * Gets the entire collection of Validator objects
	     *
	     * @type {Validator[]}
	     */
	
	  }, {
	    key: 'validators',
	    get: function get() {
	      return this._validators;
	    }
	
	    /**
	     * Sets the entire collection of Validator objects,
	     * overriding the existing collection.
	     *
	     * @type {Validator[]}
	     *
	     * @throws {TypeError} when specified value is not an array of Validators
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isArrayOf(value, function (i) {
	        return i instanceof _validator2.default;
	      })) {
	        throw new TypeError('Value must be an array of Validators');
	      }
	      this._validators = value;
	    }
	
	    /**
	     * Retrieve the name of the form
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'name',
	    get: function get() {
	      return this._name;
	    }
	
	    /**
	     * Set the form name
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._name = value;
	    }
	
	    /**
	     * Retrieve the ID of the form
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'formId',
	    get: function get() {
	      return this._formId;
	    }
	
	    /**
	     * Set the form ID
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._formId = value;
	    }
	
	    /**
	     * Retrieve the version of the renderer this form should be rendered with
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'rendererVersion',
	    get: function get() {
	      return this._rendererVersion;
	    }
	
	    /**
	     * Set the form renderer version
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._rendererVersion = value;
	    }
	
	    /**
	     * Retrieve the form info
	     *
	     * @type {FormInfo}
	     */
	
	  }, {
	    key: 'info',
	    get: function get() {
	      return this._info;
	    }
	
	    /**
	     * Set the form info
	     *
	     * @type {FormInfo}
	     *
	     * @throws {TypeError} when specified value is not a FormInfo instance
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _formInfo2.default)) {
	        throw new TypeError('Value must be a FormInfo object');
	      }
	      this._info = value;
	    }
	
	    /**
	     * Retrieve the canSaveDraft flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'canSaveDraft',
	    get: function get() {
	      return this._canSaveDraft;
	    }
	
	    /**
	     * Sets the specified canSaveDraft flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._canSaveDraft = value;
	    }
	
	    /**
	     * Retrieve the saveOnNavigate flag
	     *
	     * @type {boolean}
	     */
	
	  }, {
	    key: 'saveOnNavigate',
	    get: function get() {
	      return this._saveOnNavigate;
	    }
	
	    /**
	     * Sets the specified saveOnNavigate flag
	     *
	     * @type {boolean}
	     *
	     * @throws {TypeError} when specified value is not a Boolean
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isBoolean(value)) {
	        throw new TypeError('Value must be a boolean');
	      }
	      this._saveOnNavigate = value;
	    }
	
	    /**
	     * Retrieve the navigation texts
	     *
	     * @type {NavigationTexts}
	     */
	
	  }, {
	    key: 'navigationTexts',
	    get: function get() {
	      return this._navigationTexts;
	    }
	
	    /**
	     * Sets the navigation texts
	     *
	     * @type {NavigationTexts}
	     *
	     * @throws {TypeError} when specified value is not a NavigationTexts object
	     */
	    ,
	    set: function set(value) {
	      if (value && !(value instanceof _navigationTexts2.default)) {
	        throw new TypeError('Value must be a NavigationTexts object');
	      }
	      this._navigationTexts = value;
	    }
	  }], [{
	    key: 'fromJSON',
	    value: function fromJSON(data) {
	      var obj = data || {};
	
	      if (_typecheck2.default.isString(data)) {
	        obj = JSON.parse(data);
	      }
	
	      obj.info = _formInfo2.default.fromJSON(obj.info);
	      obj.navigationTexts = _navigationTexts2.default.fromJSON(obj.navigationTexts);
	
	      obj.steps = obj.steps || [];
	      obj.steps = obj.steps && obj.steps.map(function (s) {
	        return _step2.default.fromJSON(s);
	      });
	
	      obj.sections = obj.sections || [];
	      obj.sections = obj.sections.map(function (s) {
	        return _section2.default.fromJSON(s);
	      });
	
	      obj.fields = obj.fields || [];
	      obj.fields = obj.fields.map(function (f) {
	        return _field2.default.fromJSON(f);
	      });
	
	      obj.validators = obj.validators || [];
	      obj.validators = obj.validators.map(function (v) {
	        return _validatorFactory2.default.createValidatorFromJSON(v);
	      });
	
	      var instance = new Form();
	
	      Object.assign(instance, obj);
	
	      return instance;
	    }
	  }]);
	
	  return Form;
	}();
	
	exports.default = Form;
	module.exports = exports['default'];

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _jsonConvertable = __webpack_require__(4);
	
	var _jsonConvertable2 = _interopRequireDefault(_jsonConvertable);
	
	var _typecheck = __webpack_require__(5);
	
	var _typecheck2 = _interopRequireDefault(_typecheck);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	/**
	 * A small info object containing some meta fields
	 * for informational purposes.
	 */
	var FormInfo = function (_JsonConvertable) {
	  _inherits(FormInfo, _JsonConvertable);
	
	  /**
	   * Creates a new FormInfo instance
	   */
	  function FormInfo() {
	    _classCallCheck(this, FormInfo);
	
	    var _this = _possibleConstructorReturn(this, (FormInfo.__proto__ || Object.getPrototypeOf(FormInfo)).call(this));
	
	    _this._title;
	    _this._body;
	    return _this;
	  }
	
	  /**
	   * Retrieve the body content
	   *
	   * @type {string}
	   */
	
	
	  _createClass(FormInfo, [{
	    key: 'toJSON',
	
	
	    /**
	     * Exports the FormInfo as a plain Javascript object.
	     *
	     * @return {Object} A plain Javascript Object
	     */
	    value: function toJSON() {
	      var _this2 = this;
	
	      var obj = {
	        title: this._title,
	        body: this._body
	      };
	
	      Object.keys(obj).forEach(function (key) {
	        return obj[key] == null && delete obj[key];
	      });
	
	      Object.keys(this).filter(function (key) {
	        return key[0] !== '_';
	      }).forEach(function (key) {
	        obj[key] = _this2[key];
	      });
	
	      return obj;
	    }
	  }, {
	    key: 'body',
	    get: function get() {
	      return this._body;
	    }
	
	    /**
	     * Set the body content
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._body = value;
	    }
	
	    /**
	     * Retrieve the title content
	     *
	     * @type {string}
	     */
	
	  }, {
	    key: 'title',
	    get: function get() {
	      return this._title;
	    }
	
	    /**
	     * Set the title content
	     *
	     * @type {string}
	     *
	     * @throws {TypeError} when specified value is not a string
	     */
	    ,
	    set: function set(value) {
	      if (value && !_typecheck2.default.isString(value)) {
	        throw new TypeError('Value must be a string');
	      }
	      this._title = value;
	    }
	  }]);
	
	  return FormInfo;
	}(_jsonConvertable2.default);
	
	exports.default = FormInfo;
	module.exports = exports['default'];

/***/ }
/******/ ])
});
;
//# sourceMappingURL=acpaas-composer-core-sdk.js.map