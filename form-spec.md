# Form Renderer


## Concept
The Form Renderer (or short FR) is a generic module that can (given the correct json input) present the user with a complete form to fill in or update as he sees fit.

## Functionalities
- Rendering of form fields
- Ability to render forms in multiple steps with next and previous navigation
- Ability to render forms divided in sections
- Rendering of a form that contains prefilled data and can be considered as a previously saved form
- Validation through validators both default included or custom validation
- Flexibility to include custom field types for app specific fields
...

## Architecture & Technical documentation


### Introduction
The FR is an AngularJS module, that can render a form on a html page. In order to do that we defined a JSON structure that outlines all properties required by the FR to render all requested features in forms. Not every property might be necessary for your implementation but the FR will be able to present a form on the screen as soon as all required properties are available. Read on to see what fields and properties you can provide to make rich and complexe forms in your app.

### Input (JSON Schema)

#### The Form Object
##### Json example

```javascript
{
   "info": {
      "title": "form title",
      "body": "Form body text"
   },
   "name": "formName",
   "formId": "uniqueFormId",
   "canSaveDraft": true,
   "saveOnNavigate": false,
   "rendererVersion": 1,
   "steps": [],
   "sections": [],
   "fields": [],
   "dataStore": {
	   "someStore": [
		   {
			   "key": "someKey",
			   "value": "someValue"
		   }
	   ]
   },
   "validators": [
      {
         "name": "myOwnReusableValidator",
         "type": "regexp",
         "options": {
            "pattern": "/^[1-9]|[1-9][0-9]+$/"
         },
         "errorMessage": "Please enter a number"
      }
   ]
}
```

##### Properties


| Name       | Type          | Required     | Description |
------------ | ------------- | ------------ | ----------- |
| name | string | yes | A name for the form, this name is never printed on the page but rather used as a working title. |
| formId | string | yes | A unique Identifier for this form. can be a guid or md5 or just a number or string representation. As long as it’s unique |
| rendererVersion | string | yes | Version number of the renderer this form should be rendered with see changelog for available options |
| info | object | yes | |
| info.title | string | yes | Form title printed on top of the form |
| info.body | string |  | Intro text printed right under the title above the form |
| canSaveDraft | boolean |  | Defines if you can save the form in a draft state (default: false) ... this will add `draft: true` to the model passed into the save method. |
| saveOnNavigate | boolean |  | Defines if the aloket should save the form inbetween navigating between steps |
| navigationTexts | object |  | object holding translations for navigation buttons on this form |
| navigationTexts.submit | string |  | set the text for the submit button (f.e. on the last step of a payment form you want 'Betalen' instead of 'Verzenden') / defaults to the formrenderer's submit button text |
| navigationTexts.next | string |  | set the text of the next button (f.e. 'Volgende') / defaults to the formrenderer's next button text |
| navigationTexts.previous | string |  | set the text of the previous button (f.e. 'Vorige') / defaults to the formrenderer's previous button text |
| navigationTexts.concept | string | | set the text of the save draft button (f.e. 'Tijdelijk opslaan' of 'Opslaan als concept') / defaults to the formrenderer's concept button text |
| steps | Array | |This array can contain any number of steps. see Step below |
| sections | Array | | This array can contain any number of sections. see Sections below |
| fields | Array | | This array can contain any number of fields. see Fields below |
| dataStore | Object | | This object can contain reusable dataStores. Custom fields like (select, radio list, checkbox list) can fetch this data to fill up the value options.
| validators | Array | | This array can contain reusable validators to be referenced later see Reusing validators |

#### A Step
##### Json example

```javascript
{
   "id": "step0",
   "type": "regular",
   "title": "Step title",
   "subtitle": "Step subtitle, this can not contain html",
   "body": "Any text, as step body text, this text can contain html",
   "state": {
      "editable": true,
      "editMode": true,
      "externalUrl": "http://www.google.be"
   },
   "navigationTexts": {
      "previous": "previous",
      "next": "next",
      "concept": "Save concept",
      "submit": "Submit"
   },
   "prerequisites": {},
   "sections": []
}
```



##### Properties

| Name         | Type          | Required    | Description  |
------------ | ------------- | ----------- | ------------ |
| id | string | yes | Unique string to identify this step |
| title | string | yes | Title to be displayed at the top of this step |
| subtitle | string |  | If needed a subtitle could be given, displayed below the title field |
| body | string |  | Body text for this step. This text can contain html for formatting purposes. |
| type | string |  | default 'regular' if not given, but can be 'intro' or 'outro' or 'recap' or 'regular', this helps the formrenderer decide where to show the actual submit button and other navigation elements. |
| state | object | yes | |
| state.showTitle | boolean |  | Tells us to show the title (default: true) |
| state.editable | boolean | yes | Tells us whether this step can be edited |
| state.editMode | boolean | yes | Tells us whether this step is currently in edit mode |
| state.externalUrl | string |  | If given, the edit link will not toggle the editable state but will direct the user to this link. |
| prerequisites | object |  | [see Prerequisites](#prerequisites) |
| prerequisites.sectionsCompleted | object |  | Tells us which sections need to be completed before this step is shown |
| prerequisites.sectionsCompleted.logical | string |  | AND or OR, if not given, it will default to AND |
| prerequisites.sectionsCompleted.sections | array | yes | an array of section ids to check for completion |
| prerequisites.stepsCompleted | object |  | Tells us which steps need to be completed before this step is shown |
| prerequisites.stepsCompleted.logical | string | | AND or OR, if not given, it will default to AND |
| prerequisites.stepsCompleted.steps | array | yes | an array of step ids to check for completion |
| prerequisites.fieldsCompleted | object | | Tells us which fields need to be completed before this step is shown |
| prerequisites.fieldsCompleted.logical | string | AND or OR, if not given, it will default to AND |
| prerequisites.fieldsCompleted.fields | array | yes | an array of section ids to check for completion |
| prerequisites.fieldValues | object | | |
| prerequisites.fieldValues.logical | string | | AND or OR, if not given, it will default to AND |
| prerequisites.fieldValues.operands | array | yes | an array of fields to do a valuecheck against |
| prerequisites.fieldValues.operands[].name | string | yes | name of a field to check |
| prerequisites.fieldValues.operands[].value | string | yes | value that should be checked for |
| prerequisites.fieldValues.operands[].operator | string | yes | what equality should we check for |
| navigationTexts | object | | object holding translations for navigation buttons on this step |
| navigationTexts.submit | string | set the text for the submit button (f.e. on the last step of a payment form you want 'Betalen' instead of 'Verzenden') / defaults to the form's submit button text |
| navigationTexts.next | string | |  set the text of the next button (f.e. 'Volgende') / defaults to the form's next button text |
| navigationTexts.previous | string | | set the text of the previous button (f.e. 'Vorige') / defaults to the form's previous button text |
| navigationTexts.concept | string | | set the text of the save draft button (f.e. 'Tijdelijk opslaan' of 'Opslaan als concept') / defaults to the form's concept button text |
| sections | Array | | This array can contain any number of sections. see Sections below |
| fields | Array | | This array can contain any number of fields. see Fields below |

#### A Section

##### Json example

```javascript
{
   "id": "section0",
   "title": "Title Section",
   "subtitle": "Subtitle section",
   "state": {
      "editable": true,
      "editMode": true,
      "externalUrl": "http://www.google.be",
      "collapsible": true,
      "collapsed": true
   },
   "prerequisites": {},
   "fields": []
}
```

##### Properties

| Name       | Type          | Required     | Description  |
------------ | ------------- | ------------ | ------------ |
| id | string | yes | Unique string to identify this section |
| title | string | yes | Title to be displayed at the top of this section |
| subtitle | string | | If needed a subtitle could be given, displayed below the title field |
| state | object | yes | |
| state.editable | boolean | yes | Tells us whether this section can be edited |
| state.editMode | boolean | yes | Tells us whether this section is currently in edit mode |
| state.externalUrl | string | |  If given, the edit link will not toggle the editable state but will direct the user to this link. |
| state.collapsible | boolean | |  Tells us whether this section can be collapsed, default false |
| state.collapsed | boolean | | Tells us whether this section is currently collapsed. default false |
| prerequisites |  object | | [see Prerequisites](#prerequisites) |
| prerequisites.sectionsCompleted | object | | Tells us which sections need to be completed before this section is shown |
| prerequisites.sectionsCompleted.logical | string | |AND or OR, if not given, it will default to AND |
| prerequisites.sectionsCompleted.sections | array | yes | an array of section ids to check for completion |
| prerequisites.stepsCompleted | object | | Tells us which steps need to be completed before this section is shown |
| prerequisites.stepsCompleted.logical | string | |  AND or OR, if not given, it will default to AND |
| prerequisites.stepsCompleted.steps | array | yes | an array of step ids to check for completion |
| prerequisites.fieldsCompleted | object | | Tells us which fields need to be completed before this section is shown |
| prerequisites.fieldsCompleted.logical | string | | AND or OR, if not given, it will default to AND |
| prerequisites.fieldsCompleted.fields | array | yes | an array of section ids to check for completion |
| prerequisites.fieldValues | object | | |
| prerequisites.fieldValues.logical | string | AND or OR, if not given, it will default to AND |
| prerequisites.fieldValues.operands | array | yes | an array of fields to do a valuecheck against |
| prerequisites.fieldValues.operands[].name | string | yes | name of a field to check |
| prerequisites.fieldValues.operands[].value | string | yes | value that should be checked for |
| prerequisites.fieldValues.operands[].operator | string | yes | what equality should we check for |
| fields | Array | | This array can contain any number of fields. see Fields below |

#### A field

##### Json example

```javascript
{
   "name": "userName",
   "spec": {
      "attributes": {
         "type": "text",
         "value": null
      },
      "options": {
         "layout": {
            "fieldLayout": "full",
            "fieldClass": "span-full tablet--span-6-1 desktop--span-6-1"
         },
         "valueOptions": []
      }
   },
   "state": {
      "editable": true,
      "editMode": true,
      "externalUrl": "http://www.google.be",
      "extraInfoCollapsed": true
   },
   "prerequisites": {},
   "inputFilter": {
      "name": "otherFirstName",
      "required": true,
      "errorMessage": "Default error message",
      "validators": [
         {
            "name": "myRequiredValidator",
            "type": "required",
            "errorMessage": "Please enter a user name"
         }
      ]
   }
}
```



##### Properties


Name         | Type          | Required     | Description    |
------------ | ------------- | ------------ | -------------- |
| name | string | yes | Name for this field. must be unique. |
| spec | object | yes | |
| spec.attributes | object | yes | |
| spec.attributes.type | string | yes | Type of the field, see available fieldTypes for more info |
| spec.attributes.value | string | yes | Value of the input. when this is filled the field will be prefilled with this value. |
| spec.options | object | yes | |
| spec.options.layout | object | | |
| spec.options.layout.fieldLayout | string | | String representation of the field’s layout. fullor half are suported. the future can possibly allow more options. |
| spec.options.layout.fieldClass | string | | String that is added to the field’s class attribute. Use this if you want to add a custom CSS class to the field. |
| spec.options.valueOptions | array | yes (if types elect,radio or checkboxlist) | Array of key value objects defining the available options in this field. |
| state | object | | |
| state.editable | boolean | | Tells us whether this field can be edited. default true |
| state.editMode | boolean | | Tells us whether this field is currently in edit mode. default: true |
| state.externalUrl | string | | If present, the edit button next to the field will point towards this url instead of toggle the editmode. |
| state.extraInfoCollapsed | boolean | | Defaults to true if not given. Tells us the form should be rendered with extra Info block open by default. |
| inputFilter | object | yes | |
| inputFilter.required | boolean | | Tells us if this field is required |
| inputFilter.validators | array | | see Validators for more info |
| inputFilter.validators[].name | string | | A working name for your input filter |
| inputFilter.validators[].type | string | | A type for your filter (regex, required, ... see Validators for more info) |
| inputFilter.validators[].errorMessage | string | | Error message to display when a required field was submitted empty. |
| inputFilter.validators[].options | object | yes (if the type requires options) | extra options for your input filter (pattern, min, max, ... see Validators for more info) |
| prerequisites | object | | [see Prerequisites](#prerequisites) |
| prerequisites.sectionsCompleted | object | | Tells us which sections need to be completed before this field is shown |
| prerequisites.sectionsCompleted.logical | string | | AND or OR, if not given it will default to AND |
| prerequisites.sectionsCompleted.sections | array | yes | an array of sectionIds to check for completion |
| prerequisites.stepsCompleted | object | Tells us which steps need to be completed before this field is shown |
| prerequisites.stepsCompleted.logical | string | AND or OR, if not given it will default to AND |
| prerequisites.stepsCompleted.steps | array | yes | an array of stepIds to check for completion |
| prerequisites.fieldsCompleted | object | | Tells us which fields need to be completed before this field is shown |
| prerequisites.fieldsCompleted.logical | object | | AND or OR, if not given it will default to AND |
| prerequisites.fieldsCompleted.fields | array | yes | an array of fieldIds to check for completion |
| prerequisites.fieldValues | object | | |
| prerequisites.fieldValues.logical | object | |  AND or OR, if not given it will default to AND |
| prerequisites.fieldValues.operands | array | yes | an array of fields with their operator and value to check |
| prerequisites.fieldValues.operands[].name | string | yes | name of the field to check |
| prerequisites.fieldValues.operands[].value | string | yes | value that should be checked for |
| prerequisites.fieldValues.operands[].operator | string | yes | what equiality should we check for |

#### Value Options
Values for a multiple selectable type like select, radio and/or checkboxlist should be passed in a key value object collection.

##### example:

```javascript
{
   "valueOptions": [
      {
         "key": "yes",
         "value": "Yes"
      },
      {
         "key": "no",
         "value": "No"
      }
   ]
}
```

Setting a ValueOption selected
When you want to prefill one of your selectbox's values in your field, be sure to add the `valueoption.key`, in the 'spec.attributes.value' property of the field.

#### Field Types


| Type              | Description                                                                                                                                                         |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| text              | regular text input field                                                                                                                                            |
| textarea          | big text imput field, allows enter key                                                                                                                              |
| select            | dropdown box with options (requires valueOptions to be filled)                                                                                                      |
| radio             | radiobutton list with only 1 result, (requires valueOptions to be filled)                                                                                           |
| checkbox          | one checkbox.                                                                                                                                                       |
| checkboxlist      | multiple checkboxes. (requires valueOptions to be filled)                                                                                                           |
| date              | datepicker                                                                                                                                                          |
| street            | custom field for antwerpen.be e-loket. Requires both the districtCodespec field and the service property to be field.                                               |
| country           | custom field for antwerpen.be e-loket.                                                                                                                              |
| hidden            | a hidden field to the user, where you can store certain data that the user does not need to care about but needs to be resend on submit, for example: a session id. |
| astad-message     | astad message block, expects a status title and content to render as field options.                                                                                 |
| astad-eloket-file | file field, allowing the user to upload a file, requires the fileInfo spec.                                                                                         |
| astad-bias-file   | file field, allowing the user to upload a file required for use in bias (enterprise) eloket. requires the fileinfo spec.                                            |
| astad-html        | html block, where certain html tags are allowed...                                                                                                                  |
| repeater          | repeat any given number of fields                                                                                                                                   |
| fieldgroup        | group any number of fields together as 1 field, (f.e. address, ...)

#### Validators

example:
```javascript
{
   "name": "myCustomNumericValidator",
   "type": "regexp",
   "options": {
      "pattern": "/^[1-9]|[1-9][0-9]+$/"
   },
   "errorMessage": "Please enter a valid number"
}
```

A validator is nothing more than a type and an error message. The type defines how it will validate and corresponds with logic that is provided by the form-renderer's validation module.

Using Default Validators




#### <a name="prerequisites"></a>Prerequisites

Showing and hiding elements based on other elements (Using prerequisites)

You can hide all 3 major elements (steps, fields and sections) depending on certain prerequisites. This can be done on 4 different triggers

##### When one or more fields are complete (valid)
When you pass a 'fieldsCompleted' object to the prerequisites of a field, step or section, it will test the given fields for completion. Only once the test matches this element will be made visible.

- logical: The logical property tells us if you want 'firstname' and 'lastname' completed, or just one of the two. The logical property can thus be 'AND' or 'OR'.
- fields: The fields property should be an array of fields to be tested.

```javascript
{
   "prerequisites": {
      "fieldsCompleted": {
         "logical": "AND",
         "fields": [
            "firstname",
            "lastname"
         ]
      }
   }
}
```

##### When one or more sections are complete (valid)
When you pass a 'sectionsCompleted' object to the prerequisites of a field, step or section, it will test the given sections for completion. Only once the test matches this element will be made visible.

- logical: The logical property tells us if you want 'section0' and 'section2' completed, or just one of the two, The logical property can thus be 'AND' or 'OR'.
- sections: The sections property should be an array of sections to be tested.


```javascript
{
   "prerequisites": {
      "sectionsCompleted": {
         "logical": "AND",
         "sections": [
            "section0",
            "section2"
         ]
      }
   }
}
```


##### When one or more steps are complete (valid)
When you pass a 'stepsCompleted' object to the prerequisites of a field, step or section, it will test the given steps for completion. Only once the test matches this element will be made visible.

- logical: The logical property tells us if you want 'step0' and 'step2' completed or just one of the two. The logical property can thus be 'AND' or 'OR'.
- steps: The steps property should be an array of steps to be tested.

```javascript
{
   "prerequisites": {
      "stepsCompleted": {
         "logical": "AND",
         "steps": [
            "step0",
            "step2"
         ]
      }
   }
}
```


##### When the value of a certain field matches a specific operation.
When you pass a 'fieldValues' object to the prerequisites of a field, step or section, that element will only be visible if all given tests match.

- logical: The logical property tells us if both given tests should succeed or if just one is enough
- operands: The operands property should be an array of tests ... these tests should contain 3 properties of their own:

	- name: The name of the field to test.
	- value: The value that should be tested for.
	- operator: The operator to use for the test, do we test equality, or if a value is lower than some other value, or do we check if a checkbox is checked, thus testing if a value is in an array of values.

	- the available operators are:
		- `==` [alias `eq`]: test for equality, is the given value equal to the value the user filled in
		- `!=` [alias `neq`]: test for inequality, is the given value different from the value the user filled in
		- `<` [alias `lt`]: test for lower than, is the user's value lower than the given value
		- `>` [alias `gt`]: test for greater than, is the user's value greater than the given value
		- `<=` [alias `lte`]: test for lower than or equals, is the user's value lower than or equal to the given value
		- `>=` [alias `gte`]: test for greater than or equals, is the user's value greater than or equal to the given value
		- `in`: test if the field filled in by the user contains a value like the given value (to be used with checkbox lists)
		- `!in`: test if the field filled in by the user doesn't contain a value like the given value (to be used with checkbox lists)


This setup will test if the user's nationality IS NOT `Belgian` OR if his firstname IS `Jan`.

```javascript
{
   "prerequisites": {
      "fieldValues": {
         "logical": "OR",
         "operands": [
            {
               "name": "nationality",
               "value": "Belgian",
               "operator": "!="
            },
            {
               "name": "firstname",
               "value": "Jan",
               "operator": "=="
            }
         ]
      }
   }
}
```

The following setup will test if the user checked the `other` checkbox in a list of preferred contact methods, based on this, we could show an extra textfield asking him to clarify which other method he then prefers.

```javascript
{
   "prerequisites": {
      "fieldValues": {
         "logical": "AND",
         "operands": [
            {
               "name": "preferedContactMethod",
               "value": "other",
               "operator": "in"
            }
         ]
      }
   }
}
```


### Known Problems and FAQ

#### Where can I get more info if I have more questions about the form-renderer or it's JSON schema?
You can ask questions in the comments section of this page, or you can email me directly at sander.houttekier@digipolis.be

#### How can I see what updates were made to this page
To view changes to this document, you can easily use the history functionality of this wikispaces platform. In the top right corner, next to the page title, you can find the history button, next to the comments button. This will show you all revisions to this document. Select the revision you want and then select the version you want to compare to. It will highlight all changes for you.
