# ACPaaS Composer Core SDK

## Intro
The Composer Core SDK is a Javascript library for producing JSON files used in the ACPaaS Form & Survey Engine. The JSON spec is [described here](https://stash.antwerpen.be/projects/ASTAD/repos/formrenderer_tools_angularjs/browse/wiki/frSchema.md).

Instead of learning this spec, developers can use this SDK and develop in javascript. This way they can have a clean developer experience with a fully functional javascript library, including documentation and support for code editors.

Check out the man pages [here](manual/index.html) to get started.


## Development

Before starting the development, install the dependencies.

```
npm install
```

During development you can use the following NPM command to automatically build the sources
on any change that happens in the source tree.

```
npm run dev
```

To have your tests executed and re-run on every file change execute the following NPM command.

```
npm run test
```

To get a test coverage report execute the following NPM command. The coverage report can be found in `./coverage`.

```
npm run cover-es6
```

The documentation is generated from source code using ESDoc. Code comments are used
to build the API reference.
The manual is built from markdown files in the manual folder. To add new manual pages, create a new markdown file and update the .esdoc.json file.
To re-generate the documentation you can use the following command.

```
npm run docs
```


## Deploying

To generate the full output for deployment you first need to run the build command.
This command uses webpack to transpile the sources from src to es5 compatible files in the lib folder.

```
npm run build
```

Also make sure to update the documentation.

```
npm run docs
```


### Releasing

When publishing a new version, add the release notes in `CHANGELOG.md`. Decide on the next version (`major`, `minor`, `patch`) and run the `npm version` command.

```
npm version <major|minor|patch>
```

The command updates the version in `package.json`, adds a release commit and creates a version tag. Next, push the tag in Git and publish the package in the NPM registry.

E.g.
```
npm version major
git push origin master --tags
npm publish
```
